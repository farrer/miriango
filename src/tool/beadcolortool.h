/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_color_tool_h_
#define _miriango_bead_color_tool_h_

#include "tool.h"

namespace Miriango {

   class BeadColorTool : public Tool {
      public:
         virtual ~BeadColorTool();

      protected:
         BeadColorTool();
         
         virtual void onMouseMovedImpl(double mouseX, double mouseY) override; 
         void onKeyReleasedImpl(guint key) override;


      private:
         Bead* highBead; /**< Current hightlighted bead, if any */


   };


}


#endif

