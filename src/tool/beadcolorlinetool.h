/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_color_line_tool_h_
#define _miriango_bead_color_line_tool_h_

#include "beadcolortool.h"
#include "../common/color.h"
#include <set>

namespace Miriango {

   /*! Tool to draw colored lines to the pattern */
   class BeadColorLineTool : public BeadColorTool {
      public:
         /*! Init tool to use */
         static void init();
         /*! Finish tool use, before closing application */
         static void finish();

         /*! \return the single instance of this class */
         static BeadColorLineTool* getSingleton();

         /*! Destructor */
         ~BeadColorLineTool();

      protected:
         void onMouseMovedImpl(double mouseX, double mouseY) override; 
         void onMouseClickedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMousePressedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMouseDraggedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMouseReleasedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onKeyPressedImpl(guint key) override;

      private:
         /*! Constructor */
         BeadColorLineTool();
         
         /*! \return set with beads affected for creating the current defined
          * line from firstAffectedBead to lastAffectedBead */
         void getAffectedBeadsForLine(std::set<Bead*>& affected);

         Bead* firstAffectedBead;
         Bead* lastAffectedBead;
         Color usedColor;

         static BeadColorLineTool* singleton;

   };

}

#endif
