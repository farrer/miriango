/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcolorlinetool.h"
#include "../common/pattern.h"
#include "../common/controller.h"

#include <stdlib.h>
#include <math.h>

namespace Miriango {

   void BeadColorLineTool::init() {
      singleton = new BeadColorLineTool();
   }

   void BeadColorLineTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadColorLineTool* BeadColorLineTool::getSingleton() {
      return singleton;
   }

   BeadColorLineTool::BeadColorLineTool() {
      firstAffectedBead = NULL;
      lastAffectedBead = NULL;
   }

   BeadColorLineTool::~BeadColorLineTool() {
   }

   void BeadColorLineTool::onMouseMovedImpl(double mouseX, double mouseY) {
      /* Only highligh below bead if not started the line */
      if(firstAffectedBead == NULL) {
			BeadColorTool::onMouseMovedImpl(mouseX, mouseY);
		}
   }

   void BeadColorLineTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }

   void BeadColorLineTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(firstAffectedBead == NULL) { 
         Pattern* pattern = Controller::getCurrentPattern();

         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
         Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
         if(bead != NULL) {
            pattern->clearDrawList();
            firstAffectedBead = bead;
            lastAffectedBead = bead;

            if(button == MouseEvent::BUTTON_LEFT) {
               usedColor = Controller::getLeftColor();
            } else {
               usedColor = Controller::getRightColor();
            }
            
            pattern->drawBeadStroke(bead->getX(), bead->getY(), 
                  Color::YELLOW);
            pattern->drawBeadFill(bead->getX(), bead->getY(), usedColor);

            pattern->setDirtyArea(bead, pattern->getZoom());
         }
         pattern->notifyDirty();
      }

   }
   
   void BeadColorLineTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(firstAffectedBead != NULL) {

         Pattern* pattern = Controller::getCurrentPattern();
         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
			Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
			
         if((bead != NULL) && (lastAffectedBead != bead)) {
				lastAffectedBead = bead;

				pattern->clearDrawList();
				std::set<Bead*> affected;
            getAffectedBeadsForLine(affected);

            for(std::set<Bead*>::iterator it = affected.begin(); 
                it != affected.end(); ++it) {
               Bead* curBead = *it;
               pattern->drawBeadStroke(curBead->getX(), curBead->getY(),
                     Color::YELLOW);
               pattern->drawBeadFill(curBead->getX(), curBead->getY(), 
                     usedColor);
            }

				int x1 = (firstAffectedBead->getX() <= lastAffectedBead->getX()) ? 
						firstAffectedBead->getX() : lastAffectedBead->getX();
				int y1 = (firstAffectedBead->getY() <= lastAffectedBead->getY()) ? 
						firstAffectedBead->getY() : lastAffectedBead->getY();
				int x2 = (firstAffectedBead->getX() > lastAffectedBead->getX()) ? 
						firstAffectedBead->getX() : lastAffectedBead->getX();
				int y2 = (firstAffectedBead->getY() > lastAffectedBead->getY()) ? 
						firstAffectedBead->getY() : lastAffectedBead->getY();
            
            pattern->setDirtyArea(
                  1 + Bead::getHalfWidth(pattern->getZoom()) * x1, 
                  1 + Bead::getHalfHeight(pattern->getZoom()) * y1, 
                  Bead::getWidth(pattern->getZoom()) * (x2 - x1 + 1), 
                  Bead::getHeight(pattern->getZoom()) * (y2 - y1 + 1));
            pattern->notifyDirty();
			}
		}

   }

   void BeadColorLineTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(firstAffectedBead != NULL) {
         Pattern* pattern = Controller::getCurrentPattern();
		
         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
			Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
			if((bead != NULL) && (lastAffectedBead != bead)) {
				lastAffectedBead = bead;
			}

			if(lastAffectedBead != NULL) {
				pattern->clearDrawList();
				std::set<Bead*> affected;
            getAffectedBeadsForLine(affected);

            for(std::set<Bead*>::iterator it = affected.begin(); 
                  it != affected.end(); ++it) {
               Bead* curBead = *it;
					curBead->setColor(usedColor, false);
				}
				
				if(affected.size() > 0) {
					pattern->getUsedColors().updateDisplay();
               pattern->doneAction();
				}
			}
			firstAffectedBead = NULL;
			lastAffectedBead = NULL;
		}
   }
   
   void BeadColorLineTool::onKeyPressedImpl(guint key) {
      if((key == GDK_KEY_Escape) && (firstAffectedBead != NULL)) {
         Pattern* pattern = Controller::getCurrentPattern();
         pattern->clearDrawList();
			firstAffectedBead = NULL;
		}
   }

   void BeadColorLineTool::getAffectedBeadsForLine(std::set<Bead*>& affected) {
		affected.insert(firstAffectedBead);
		
		int diffX = lastAffectedBead->getX() - firstAffectedBead->getX();
		int diffY = lastAffectedBead->getY() - firstAffectedBead->getY();
		
		int div = 0;
		if(abs(diffX) > abs(diffY)) {
			div = abs(diffX);
		} else {
			div = abs(diffY);
		}
		
		if(div == 0) {
			/* Only a single bead is affected */
			return;
		}
		
		float incX = diffX / (float)div;
		float incY = diffY / (float)div;
		
		float curX = firstAffectedBead->getX();
		float curY = firstAffectedBead->getY();
      
      Pattern* pattern = Controller::getCurrentPattern();
		
		while((curX != lastAffectedBead->getX()) || 
            (curY != lastAffectedBead->getY())) {
			Bead* bead = pattern->getBead(round(curX), round(curY));
			if((bead != NULL) && (bead->isUsed())) {
				affected.insert(bead);
			} else if(bead != NULL) {
				/* Check if will affect side ones, as the directed affected 
             * isn't used */
				if(curX != lastAffectedBead->getX()) {
					if(incX > 0) {
						bead = pattern->getBead(round(curX) + 1, round(curY));
					} else {
						bead = pattern->getBead(round(curX) - 1, round(curY));
					}
					if((bead != NULL) && (bead->isUsed())) {
						affected.insert(bead);
					}
				}
				if(curY != lastAffectedBead->getY()) {
					if(incY > 0) {
						bead = pattern->getBead(round(curX), round(curY) + 1);
					} else {
						bead = pattern->getBead(round(curX), round(curY) - 1);
					}
					if((bead != NULL) && (bead->isUsed())) {
						affected.insert(bead);
					}
				}
			}
			
			curX += incX;
			if((incX > 0 && curX > lastAffectedBead->getX()) ||
			   (incX < 0 && curX < lastAffectedBead->getX())) {
				curX = lastAffectedBead->getX();
			}
			curY += incY;
			if((incY > 0 && curY > lastAffectedBead->getY()) ||
			   (incY < 0 && curY < lastAffectedBead->getY())) {
				curY = lastAffectedBead->getY();
			}
		}
		affected.insert(lastAffectedBead);
	}


   BeadColorLineTool* BeadColorLineTool::singleton = NULL;

}
