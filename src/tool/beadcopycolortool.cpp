/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcopycolortool.h"
#include "beadpastecolortool.h"

namespace Miriango {

   void BeadCopyColorTool::init() {
      singleton = new BeadCopyColorTool();
   }

   void BeadCopyColorTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadCopyColorTool* BeadCopyColorTool::getSingleton() {
      return singleton;
   }

   BeadCopyColorTool::BeadCopyColorTool() 
                     :BeadCopyTool(Tool::BEAD_EDITOR, Color::YELLOW) {
   }

   BeadCopyColorTool::~BeadCopyColorTool() {
   }

   void BeadCopyColorTool::onApplySelection(std::set<Bead*>& beads) {
      BeadPasteColorTool::getSingleton()->setSelection(beads);
   }

   BeadCopyColorTool* BeadCopyColorTool::singleton = NULL;
}

