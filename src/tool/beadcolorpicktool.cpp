/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcolorpicktool.h"
#include "../common/pattern.h"
#include "../common/controller.h"

namespace Miriango {
   
   void BeadColorPickTool::init() {
      singleton = new BeadColorPickTool();
   }
   
   void BeadColorPickTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadColorPickTool* BeadColorPickTool::getSingleton() {
      return singleton;
   }

   BeadColorPickTool::BeadColorPickTool() {
   }

   BeadColorPickTool::~BeadColorPickTool() {
   }

   void BeadColorPickTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      
      Pattern* pattern = Controller::getCurrentPattern();
      pattern->clearDrawList();
      
      int beadX=-1, beadY=-1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);
      Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
      if(bead != NULL) {
         if(button == MouseEvent::BUTTON_LEFT) {
            Controller::setLeftColor(bead->getColor());
         } else {
            Controller::setRightColor(bead->getColor());
         }
      }
   }
   
   void BeadColorPickTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }

   void BeadColorPickTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }
   
   void BeadColorPickTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }
   
   void BeadColorPickTool::onKeyPressedImpl(guint key) {
   }

   BeadColorPickTool* BeadColorPickTool::singleton = NULL;

}

