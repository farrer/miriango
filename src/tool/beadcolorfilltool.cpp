/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcolorfilltool.h"
#include "../common/controller.h"
#include "../common/pattern.h"

namespace Miriango {

   void BeadColorFillTool::init() {
      singleton = new BeadColorFillTool();
   }

   void BeadColorFillTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadColorFillTool::BeadColorFillTool() {
   }

   BeadColorFillTool::~BeadColorFillTool() {
   }

   BeadColorFillTool* BeadColorFillTool::getSingleton() {
      return singleton;
   }

   void BeadColorFillTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
     
      Pattern* pattern = Controller::getCurrentPattern();
      pattern->clearDrawList();

      int beadX=-1, beadY=-1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);
      Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
      if(bead != NULL) {
         if(button == MouseEvent::BUTTON_LEFT) {
            fillNeighborSameColor(Controller::getLeftColor(), 
                  bead->getColor(), bead);
         } else {
            fillNeighborSameColor(Controller::getRightColor(), 
                  bead->getColor(), bead);
         }

      }
      pattern->doneAction();
   }

   void BeadColorFillTool::fillNeighborSameColor(const Color& newColor, 
         const Color& oldColor, Bead* bead) {
      
      if(newColor == oldColor) {
         /* Nothing to do, already filled with the desired color. */
         return;
      }

      Pattern* pattern = Controller::getCurrentPattern();
      std::stack<Bead*> activeBeads;
      activeBeads.push(bead);

      while(activeBeads.size() > 0) {
         Bead* currentBead = activeBeads.top();
         activeBeads.pop();

         /* Fill current bead to new color */
         currentBead->setColor(newColor, false);
         pattern->setDirtyArea(currentBead, pattern->getZoom());

         /* Add neighbors to the stack */
         int x = currentBead->getX();
         int y = currentBead->getY();

         /* Half neigh at left or right */
         addIfCompatible(activeBeads, pattern->getBead(x-2, y-1), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x-2, y+1), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x+2, y-1), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x+2, y+1), 
               oldColor, newColor);

         /* half neigh at top or bottom */
         addIfCompatible(activeBeads, pattern->getBead(x-1, y-2), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x-1, y+2), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x+1, y-2), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x+1, y+2), 
               oldColor, newColor);

         /* full neigh above */
         addIfCompatible(activeBeads, pattern->getBead(x-2, y-2), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x, y-2), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x+2, y-2), 
               oldColor, newColor);

         /* full neigh at same line */
         addIfCompatible(activeBeads, pattern->getBead(x-2, y), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x+2, y), 
               oldColor, newColor);

         /* full neigh bellow */
         addIfCompatible(activeBeads, pattern->getBead(x-2, y+2), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x, y+2), 
               oldColor, newColor);
         addIfCompatible(activeBeads, pattern->getBead(x+2, y+2), 
               oldColor, newColor);
      }
      pattern->getUsedColors().updateDisplay();
      pattern->notifyDirty();
   }
   
   void BeadColorFillTool::addIfCompatible(std::stack<Bead*>& activeBeads, 
         Bead* bead, const Color& oldColor, const Color& newColor) {

      /* Must be defined, used, with current color equals to the old color
       * and different from new color */
      if((bead != NULL) && (bead->isUsed()) && 
         (bead->getColor() == oldColor) && (bead->getColor() != newColor)) {
         activeBeads.push(bead);
      } 
   }

   void BeadColorFillTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }
   void BeadColorFillTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }
   void BeadColorFillTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }
   void BeadColorFillTool::onKeyPressedImpl(guint key) {
   }

   BeadColorFillTool* BeadColorFillTool::singleton = NULL;
}

