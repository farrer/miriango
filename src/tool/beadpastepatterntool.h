/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_paste_pattern_tool_h_
#define _miriango_bead_paste_pattern_tool_h_

#include "beadpastetool.h"

namespace Miriango {

   /*! Paste selected pattern to the area. This will change any already existed
    * beads that overlaps selected position to paste. */
   class BeadPastePatternTool : public BeadPasteTool {
      public:
         /*! Init the tool to use */
         static void init();
         /*! Finish the use of the tool (end of application) */
         static void finish();
         /*! Get the single instance of this tool */
         static BeadPastePatternTool* getSingleton();
         /*! Destructor */
         ~BeadPastePatternTool();

      protected:
         BeadPastePatternTool();

         void onMouseMovedImpl() override; 
         void onMouseClickedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMousePressedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMouseDraggedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMouseReleasedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;

      private:
         static BeadPastePatternTool* singleton;
   };

}

#endif

