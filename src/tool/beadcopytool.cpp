/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcopytool.h"
#include "../common/controller.h"
#include "../common/pattern.h"
#include <math.h>

namespace Miriango {

   BeadCopyTool::BeadCopyTool(const Tool::EditorType& editorType, 
         const Color& selectionColor)
                :Tool(editorType),
                 middleColor(1.0f, 1.0f, 1.0f, 0.1f) {
      this->curMode = NONE;
      this->selectionColor = selectionColor;
      this->highBead = NULL;
      this->highCoor[0] = -1;
      this->highCoor[1] = -1;
   }

   BeadCopyTool::~BeadCopyTool() {
   }
   
   void BeadCopyTool::onMouseMovedImpl(double mouseX, double mouseY) {
      if(curMode == MANUAL) {
         Pattern* pattern = Controller::getCurrentPattern();
         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
         Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
         if(bead != highBead) {
            pattern->clearDrawList();
            highBead = bead;
            if(bead != NULL) {
               /* Highlight the bead */
               pattern->drawBeadStroke(bead->getX(), bead->getY(), 
                     Color::YELLOW);
               pattern->setDirtyArea(bead, pattern->getZoom());
            }
            drawSelectedBeads();
            pattern->notifyDirty();
         }
      }
   }

   void BeadCopyTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(curMode == MANUAL) {
         Pattern* pattern = Controller::getCurrentPattern();

         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
         Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
         if(bead != NULL) {
            if(selectedBeads.find(bead) != selectedBeads.end()) {
               /* Remove it from selection */
               selectedBeads.erase(bead);
            } else {
               /* Insert it on the selection */
               selectedBeads.insert(bead);
            }

            /* Redraw selected beads */
            pattern->clearDrawList();
            drawSelectedBeads();
            pattern->notifyDirty();
         }
      }
   }

   void BeadCopyTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(curMode == NONE) {
         curMode = RECTANGULAR;
         firstPos.set_x(mouseX);
         firstPos.set_y(mouseY);
         lastPos.set_x(mouseX);
         lastPos.set_y(mouseY);

         defineBeadsUnder();
      }
   }

   void BeadCopyTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(curMode == RECTANGULAR) {
         lastPos.set_x(mouseX);
         lastPos.set_y(mouseY);
         defineBeadsUnder();
      }
   }

   void BeadCopyTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(curMode == RECTANGULAR) {
         lastPos.set_x(mouseX);
         lastPos.set_y(mouseY);
         defineBeadsUnder();
         onApplySelection(selectedBeads);

         curMode = NONE;
         Controller::getCurrentPattern()->clearDrawList();
         Controller::getCurrentPattern()->notifyDirty();
         selectedBeads.clear();
      }
   }

   void BeadCopyTool::onKeyPressedImpl(guint key) {
      if(key == GDK_KEY_Escape) {
         if(curMode != NONE) {
            /* Cancel active selection */
            selectedBeads.clear();
            curMode = NONE;
            Controller::getCurrentPattern()->clearDrawList();
            Controller::getCurrentPattern()->notifyDirty();
         }
      } else if((key == GDK_KEY_Shift_L) || (key == GDK_KEY_Shift_R)) {
         if(curMode == NONE) {
            /* Enter manual bead selection mode */
            curMode = MANUAL;
            selectedBeads.clear();
         }
      }
   }
   
   void BeadCopyTool::onKeyReleasedImpl(guint key) {
      if(curMode == MANUAL) {
         if((key == GDK_KEY_Shift_L) || (key == GDK_KEY_Shift_R)) {
            /* Done selection */
            onApplySelection(selectedBeads);
            curMode = NONE;
            Controller::getCurrentPattern()->clearDrawList();
            Controller::getCurrentPattern()->notifyDirty();
            selectedBeads.clear();
         }
      }
   }

   void BeadCopyTool::drawSelectedBeads() {
      Pattern* pattern = Controller::getCurrentPattern();
      /* Highlight each of the selected beads */
      for(std::set<Bead*>::iterator it = selectedBeads.begin();
            it != selectedBeads.end(); ++it) {
         Bead* bead = *it;
         pattern->drawBeadStroke(bead->getX(), bead->getY(), selectionColor);
         pattern->setDirtyArea(bead, pattern->getZoom());
      }
   }

   void BeadCopyTool::defineBeadsUnder() {
      Pattern* pattern = Controller::getCurrentPattern();

      int firstCoorX=-1, firstCoorY=-1;
      getBeadCoordinate(firstPos.get_x(), firstPos.get_y(), 
            firstCoorX, firstCoorY);
      int lastCoorX=-1, lastCoorY=-1;
      getBeadCoordinate(lastPos.get_x(), lastPos.get_y(), 
            lastCoorX, lastCoorY);

      if((lastCoorX != highCoor[0]) || (lastCoorY != highCoor[1])) {
         selectedBeads.clear();

         highCoor[0] = lastCoorX;
         highCoor[1] = lastCoorY;

         pattern->clearDrawList();
         pattern->drawRectangle(firstPos, lastPos, selectionColor);


         int x1 = (firstCoorX <= lastCoorX) ? firstCoorX : lastCoorX;
         int y1 = (firstCoorY <= lastCoorY) ? firstCoorY : lastCoorY;
         int x2 = (firstCoorX > lastCoorX) ? firstCoorX : lastCoorX;
         int y2 = (firstCoorY > lastCoorY) ? firstCoorY : lastCoorY;

         for(int x = x1; x <= x2; x++) {
            for(int y = y1; y <= y2; y++) {
               Bead* bead = pattern->getBead(x, y);
               if((bead != NULL) && (bead->isUsed())) {
                  selectedBeads.insert(bead);
                  pattern->drawBeadStroke(bead->getX(), bead->getY(),
                        selectionColor);
               }
            }
         }

         pattern->setDirtyArea(
               1 + floor(Bead::getHalfWidth(pattern->getZoom()) * x1),
               1 + floor(Bead::getHalfHeight(pattern->getZoom()) * y1),
               ceil(Bead::getWidth(pattern->getZoom()) * (x2 - x1 + 1)),
               ceil(Bead::getHeight(pattern->getZoom()) * (y2 - y1 + 1)));
         pattern->notifyDirty();
      }
   }
}

