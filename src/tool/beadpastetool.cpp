/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadpastetool.h"
#include "../common/bead.h"

namespace Miriango {

   BeadPasteTool::BeadPasteTool(const Tool::EditorType& editorType)
                 :Tool(editorType) {
      this->angle = 0;
      this->mouseX = 0.0;
      this->mouseY = 0.0;
      this->lastBead = NULL;
   }

   BeadPasteTool::~BeadPasteTool() {
      clearSelection();
   }
   
   void BeadPasteTool::clearSelection() {
      while(beads.size() > 0) {
         Bead* bead = beads.back();
         beads.pop_back();
         delete bead;
      }
      this->angle = 0;
   }

   void BeadPasteTool::setSelection(std::set<Bead*>& selection) {
      mutex.lock();
      clearSelection();

      if(selection.size() == 0) {
         /* no selection, just exit as already cleared it */
         mutex.unlock();
         return;
      }

      int x1 = 0, y1 = 0, x2 = 0, y2 = 0;

      Bead* firstBead = *selection.begin();
      for(std::set<Bead*>::iterator it = selection.begin();
            it != selection.end(); ++it) {
         Bead* selBead = *it;
         Bead* bead = new Bead(NULL);
         if(beads.size() == 0) {
            /* First bead at 0,0 */
            bead->setCoordinate(0, 0);
         } else {
            /* All next beads with coordinate relative to the first one */
            bead->setCoordinate(selBead->getX() - firstBead->getX(), 
                  selBead->getY() - firstBead->getY());
         }
         if(bead->getX() > x2) {
            x2 = bead->getX();
         }
         if(bead->getX() < x1) {
            x1 = bead->getX();
         }
         if(bead->getY() > y2) {
            y2 = bead->getY();
         }
         if(bead->getY() < y1) {
            y1 = bead->getY();
         }
         (*bead) = (*selBead);
         beads.push_back(bead);
      }

      beadsArea.set_x(x1);
      beadsArea.set_y(y1);
      beadsArea.set_width(x2 - x1 + 1);
      beadsArea.set_height(y2 - y1 + 1);

      mutex.unlock();
   }

   void BeadPasteTool::onMouseMovedImpl(double mouseX, double mouseY) {
      this->mouseX = mouseX;
      this->mouseY = mouseY;
      onMouseMovedImpl();
   }

   void BeadPasteTool::onKeyPressedImpl(guint key) {
   }

   void BeadPasteTool::onKeyReleasedImpl(guint key) {
      bool angleChanged = false;
      if(key == GDK_KEY_Left) {
         angle = (angle + 90);
         angleChanged = true;
      } else if(key == GDK_KEY_Right) {
         angle = (angle - 90);
         angleChanged = true;
      }

      if(angleChanged) {
         /* Keep angle at [0, 360) range, without affecting 
          * the rotation it describes */
         if(angle < 0) {
            angle += 360;
         } else if(angle >= 360) {
            angle -= 360;
         }
         /* Update the potential paste by calling onMouseMove with
          * previous mouse position and without lastBead defined. */
         lastBead = NULL;
         onMouseMovedImpl();
      }
   }


   void BeadPasteTool::rotateRelativeCoordinate(int& x, int& y) {
      /* Do the rotation. angle = 0, no rotation at all */
      int prevX = x;
      int prevY = y;
      if(angle == 90) {
         x = -prevY;
         y = -prevX;
      } else if(angle == 180) {
         x = -prevX;
         y = -prevY;
      } else if(angle == 270) {
         x = prevY;
         y = prevX;
      }
   }
}

