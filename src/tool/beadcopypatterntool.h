/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_copy_pattern_tool_h_
#define _miriango_bead_copy_pattern_tool_h_

#include "beadcopytool.h"

namespace Miriango {

   class BeadCopyPatternTool : public BeadCopyTool {
      public:
         static void init();
         static void finish();
         static BeadCopyPatternTool* getSingleton();

         virtual ~BeadCopyPatternTool();

      protected:
         BeadCopyPatternTool();
         void onApplySelection(std::set<Bead*>& beads) override;

      private:
         static BeadCopyPatternTool* singleton;
         
   };

}

#endif

