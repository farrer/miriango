/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadpastepatterntool.h"
#include "../common/controller.h"
#include "../common/pattern.h"

namespace Miriango {

   void BeadPastePatternTool::init() {
      singleton = new BeadPastePatternTool();
   }

   void BeadPastePatternTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadPastePatternTool* BeadPastePatternTool::getSingleton() {
      return singleton;
   }

   BeadPastePatternTool::BeadPastePatternTool()
                        :BeadPasteTool(Tool::PATTERN_EDITOR) {
   }

   BeadPastePatternTool::~BeadPastePatternTool() {
   }

   void BeadPastePatternTool::onMouseMovedImpl() {
      if(beads.size() > 0) {
         Pattern* pattern = Controller::getCurrentPattern();

         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
         Bead* selBead = pattern->getBead(beadX, beadY);
         if(selBead != lastBead) {
            lastBead = selBead;
            pattern->clearDrawList();
            if(selBead != NULL) {
               for(std::list<Bead*>::iterator it = beads.begin(); 
                     it != beads.end(); ++it) {
                  Bead* curBead = *it;
                  /* Define bead to affect, based on orientation angle */
                  int curX = curBead->getX();
                  int curY = curBead->getY();
                  rotateRelativeCoordinate(curX, curY);
                  Bead* bead = pattern->getBead(curX + selBead->getX(),
                        curY + selBead->getY());
                  if(bead != NULL) {
                     pattern->drawBeadStroke(bead->getX(), bead->getY(),
                           Color::BLACK);
                     pattern->drawBeadFill(bead->getX(), bead->getY(),
                           curBead->getColor());
                     pattern->setDirtyArea(bead, pattern->getZoom());
                  }
               }
            }
            pattern->notifyDirty();
         }
      }
   }
   
   void BeadPastePatternTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(beads.size() > 0) {
         Pattern* pattern = Controller::getCurrentPattern();
         pattern->clearDrawList();

         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
         Bead* selBead = pattern->getBead(beadX, beadY);
         if(selBead != NULL) {

            for(std::list<Bead*>::iterator it = beads.begin(); 
                  it != beads.end(); ++it) {
               Bead* curBead = *it;
               /* Define bead to affect, based on orientation angle */
               int curX = curBead->getX();
               int curY = curBead->getY();
               rotateRelativeCoordinate(curX, curY);
               Bead* bead = pattern->getBead(curX + selBead->getX(),
                     curY + selBead->getY());
               if(bead != NULL) {
                  bead->setColor(curBead->getColor(), false);
                  if(!bead->isUsed()) {
                     /* Must check immediate neighbors, that should be unused */
                     for(int x = -1; x <= 1; x++) {
                        for(int y = -1; y <= 1; y++) {
                           Bead* neigh = pattern->getBead(bead->getX() + x, 
                                 bead->getY() + y);
                           if((neigh != NULL) && (neigh->isUsed())) {
                              neigh->unuse(false);
                              pattern->setDirtyArea(neigh, pattern->getZoom());
                           }
                        }
                     }

                     /* Finally, use this one */
                     bead->use(false);
                     pattern->setDirtyArea(bead, pattern->getZoom());
                  }
               }
            }

            if(beads.size() > 0) {
               pattern->getUsedColors().updateDisplay();
               pattern->doneAction();
            }
            pattern->notifyDirty();
         }
      }
   }

   void BeadPastePatternTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }

   void BeadPastePatternTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }

   void BeadPastePatternTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }

   BeadPastePatternTool* BeadPastePatternTool::singleton = NULL;

}

