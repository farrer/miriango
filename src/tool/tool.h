/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_tool_h_
#define _miriango_tool_h_

#include "miriangoconfig.h"
#include "../common/mouseevent.h"
#include "../common/bead.h"
#include "../common/pattern.h"
#include <gtkmm.h>

namespace Miriango {

   /*! Tool abstract class, with common tool functions */
   class Tool {

      public:

         enum EditorType {
            PATTERN_EDITOR,
            BEAD_EDITOR
         };

         /*! Destructor */
         virtual ~Tool();

         const EditorType& getEditorType() { return editorType; };

         void getBeadCoordinate(double mouseX, double mouseY, 
               int& beadX, int& beadY);

         void onMouseMoved(double mouseX, double mouseY); 
         void onMouseClicked(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button);
         void onMousePressed(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button);
         void onMouseDragged(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button);
         void onMouseReleased(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button);
 
         void onKeyPressed(guint key);
         void onKeyReleased(guint key);

      protected:
         /*! Constructor 
          * \param type type of the editor where the tool act */
         Tool(const EditorType& type);


         virtual void onMouseMovedImpl(double mouseX, double mouseY) = 0; 
         virtual void onMouseClickedImpl(double mouseX, double mouseY, 
                        const MouseEvent::MouseButton& button) = 0;
         virtual void onMousePressedImpl(double mouseX, double mouseY, 
                        const MouseEvent::MouseButton& button) = 0;
         virtual void onMouseDraggedImpl(double mouseX, double mouseY, 
                        const MouseEvent::MouseButton& button) = 0;
         virtual void onMouseReleasedImpl(double mouseX, double mouseY, 
                        const MouseEvent::MouseButton& button) = 0;
         virtual void onKeyPressedImpl(guint key) = 0;
         virtual void onKeyReleasedImpl(guint key) = 0;

         /*! Mutex to avoid mutual access on critical parts */
         Glib::Mutex mutex;

      private:
         /*! Type of the editor that owns the Tool */
         EditorType editorType;
         

   };

}

#endif


