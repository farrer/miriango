/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcutpatterntool.h"
#include "beadpastepatterntool.h"
#include "../common/controller.h"
#include "../common/pattern.h"

namespace Miriango {

   void BeadCutPatternTool::init() {
      singleton = new BeadCutPatternTool();
   }

   void BeadCutPatternTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadCutPatternTool* BeadCutPatternTool::getSingleton() {
      return singleton;
   }

   BeadCutPatternTool::BeadCutPatternTool() 
                     :BeadCopyTool(Tool::PATTERN_EDITOR, Color::RED) {
   }

   BeadCutPatternTool::~BeadCutPatternTool() {
   }

   void BeadCutPatternTool::onApplySelection(std::set<Bead*>& beads) {
      BeadPastePatternTool::getSingleton()->setSelection(beads);
      Pattern* pattern = Controller::getCurrentPattern();

      for(std::set<Bead*>::iterator it = beads.begin(); 
          it != beads.end(); ++it) {
         Bead* bead = *it;
         bead->unuse(false);
         pattern->setDirtyArea(bead, pattern->getZoom());
      }
      if(beads.size() > 0) {
         pattern->getUsedColors().updateDisplay();
         pattern->doneAction();
      }
   }

   BeadCutPatternTool* BeadCutPatternTool::singleton = NULL;
}

