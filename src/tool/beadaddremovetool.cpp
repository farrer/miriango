/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadaddremovetool.h"
#include "../common/pattern.h"
#include "../common/controller.h"

namespace Miriango {

   void BeadAddRemoveTool::init() {
      singleton = new BeadAddRemoveTool();
   }

   void BeadAddRemoveTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadAddRemoveTool* BeadAddRemoveTool::getSingleton() {
      return singleton;
   }

   BeadAddRemoveTool::BeadAddRemoveTool()
                     :Tool(Tool::PATTERN_EDITOR) {
      dragIsAdding = false;
      highBeadPos[0] = -1;
      highBeadPos[1] = -1;
   }
   
   BeadAddRemoveTool::~BeadAddRemoveTool() {
   }

   void BeadAddRemoveTool::onMouseMovedImpl(double mouseX, double mouseY) {
      Pattern* pattern = Controller::getCurrentPattern();

      int beadX=-1, beadY=-1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);
      if((beadX != highBeadPos[0]) || (beadY != highBeadPos[1])) {
         pattern->clearDrawList();
         highBeadPos[0] = beadX;
         highBeadPos[1] = beadY;
         Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
         if(bead != NULL) {
            /* Potentially removing a bead */
            pattern->drawBeadStroke(bead->getX(), bead->getY(), Color::RED);
            pattern->setDirtyArea(bead, pattern->getZoom());
         } else if((beadX < pattern->getWidth()) && 
               (beadY < pattern->getHeight())) {
            /* Potentially adding a bead */
            pattern->drawBeadFill(beadX, beadY, Color::GREEN);
            pattern->setDirtyArea(beadX, beadY, pattern->getZoom());
         }
         pattern->notifyDirty();
      }
   }

   void BeadAddRemoveTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      Pattern* pattern = Controller::getCurrentPattern();
      pattern->clearDrawList();

      int beadX=-1, beadY=-1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);
      Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
      if(bead != NULL) {
         /* unuse it */
         bead->unuse(true);
         pattern->setDirtyArea(bead, pattern->getZoom());
         pattern->doneAction();
      } else if((beadX < pattern->getWidth()) && 
                (beadY < pattern->getHeight())) {
         bead = pattern->getBead(beadX, beadY);
         if((bead != NULL) && (!bead->isUsed())) {
            /* Use it */
            bead->use(true);
            pattern->setDirtyArea(bead, pattern->getZoom());
            pattern->doneAction();
         }
      }
   }

   void BeadAddRemoveTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      Pattern* pattern = Controller::getCurrentPattern();
      pattern->clearDrawList();

      int beadX=-1, beadY=-1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);
      Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
      if(bead != NULL) {
         /* unuse it */
         bead->unuse(false);
         pattern->setDirtyArea(bead, pattern->getZoom());
         pattern->notifyDirty();
         dragIsAdding = false;
      } else if((beadX < pattern->getWidth()) && 
                (beadY < pattern->getHeight())) {
         bead = pattern->getBead(beadX, beadY);
         if((bead != NULL) && (!bead->isUsed())) {
            /* Use it */
            bead->use(false);
            pattern->setDirtyArea(bead, pattern->getZoom());
            pattern->notifyDirty();
         }
         dragIsAdding = true;
      }
   }
   
   void BeadAddRemoveTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      Pattern* pattern = Controller::getCurrentPattern();
      pattern->clearDrawList();

      int beadX=-1, beadY=-1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);
      Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
      if(bead != NULL) {
         if(!dragIsAdding) {
            /* unuse it */
            bead->unuse(false);
            pattern->setDirtyArea(bead, pattern->getZoom());
            pattern->notifyDirty();
         }
      } else if((dragIsAdding) &&
                (beadX < pattern->getWidth()) && 
                (beadY < pattern->getHeight())) {
         bead = pattern->getBead(beadX, beadY);
         if((bead != NULL) && (!bead->isUsed())) {
            /* Use it */
            bead->use(false);
            pattern->setDirtyArea(bead, pattern->getZoom());
            pattern->notifyDirty();
         }
      }
   }
   
   void BeadAddRemoveTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      Pattern* pattern = Controller::getCurrentPattern();
      pattern->clearDrawList();
      pattern->doneAction();
      pattern->getUsedColors().updateDisplay();
   }
   
   void BeadAddRemoveTool::onKeyPressedImpl(guint key) {
   }

   void BeadAddRemoveTool::onKeyReleasedImpl(guint key) {
   }

   BeadAddRemoveTool* BeadAddRemoveTool::singleton = NULL;

}

