/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_copy_tool_h_
#define _miriango_bead_copy_tool_h_

#include "tool.h"
#include "../common/color.h"
#include <set>

namespace Miriango {

   class BeadCopyTool : public Tool {
      public:
         enum SelectionMode {
            /** When not started the selection yet */
            NONE,
            /** Rectangular area selection */
            RECTANGULAR,
            /** Manual beads selection */
            MANUAL
         };
         virtual ~BeadCopyTool();

      protected:
         /*! Constructor
          * \param editorType type of the editor who implements it
          * \param selectionColor color to highlight selections */
         BeadCopyTool(const Tool::EditorType& editorType, 
               const Color& selectionColor);

         /*! Called whe mouse is released and selection should be applyed */
         virtual void onApplySelection(std::set<Bead*>& beads) = 0;
         
         void onMouseMovedImpl(double mouseX, double mouseY) override; 
         void onMouseClickedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMousePressedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMouseDraggedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onMouseReleasedImpl(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) override;
         void onKeyPressedImpl(guint key) override;
         void onKeyReleasedImpl(guint key) override;

         /*! Current under selection beads */
         std::set<Bead*> selectedBeads;
         /*! Point on canvas where the mouse first pressed before drag */
         Gdk::Point firstPos;
         /*! Current point on canvas while dragging */
         Gdk::Point lastPos;
         /*! Color do draw the selection border */
         Color selectionColor;
         /*! Color to draw the fill of selection */
         Color middleColor;
         /*! Current selection mode */
         SelectionMode curMode;

      private:
         /*! Draw all selected beads */
         void drawSelectedBeads();
         /*! Define selectedBeads to be the beads under firstPos to lastPos
          * rectangular area */
         void defineBeadsUnder();

         Bead* highBead; /**< Current under mouse single highlighted bead */
         int highCoor[2]; /**< Current highlighted rectangle final coordinate */

   };

}

#endif

