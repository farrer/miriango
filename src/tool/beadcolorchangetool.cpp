/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcolorchangetool.h"
#include "../common/pattern.h"
#include "../common/controller.h"

namespace Miriango {

   void BeadColorChangeTool::init() {
      singleton = new BeadColorChangeTool();
   }

   void BeadColorChangeTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }

   BeadColorChangeTool* BeadColorChangeTool::getSingleton() {
      return singleton;
   }

   BeadColorChangeTool::BeadColorChangeTool() {
      this->changed = false;
   }

   BeadColorChangeTool::~BeadColorChangeTool() {
   }

   bool BeadColorChangeTool::changeBeadColorOnClickOrPress(double mouseX, 
         double mouseY, const MouseEvent::MouseButton& button) {
      Pattern* pattern = Controller::getCurrentPattern();
      pattern->clearDrawList();

      int beadX=-1, beadY=-1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);

      Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);
      bool colorChanged = false;
      if(bead != NULL) {
         if(button == MouseEvent::BUTTON_LEFT) {
            colorChanged = bead->setColor(Controller::getLeftColor(), true);
         } else {
            colorChanged = bead->setColor(Controller::getRightColor(), true);
         }
         if(colorChanged) {
            pattern->setDirtyArea(bead, pattern->getZoom());
            changed = true;
         }
      }
      return colorChanged;
   }

   void BeadColorChangeTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      changeBeadColorOnClickOrPress(mouseX, mouseY, button);
      if(changed) {
         Controller::getCurrentPattern()->doneAction();
         changed = false;
      }
   }

   void BeadColorChangeTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(changeBeadColorOnClickOrPress(mouseX, mouseY, button)) {
         Controller::getCurrentPattern()->notifyDirty();
      }
   }

   void BeadColorChangeTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(changeBeadColorOnClickOrPress(mouseX, mouseY, button)) {
         Controller::getCurrentPattern()->notifyDirty();
      }
   }

   void BeadColorChangeTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(changed) {
         Controller::getCurrentPattern()->doneAction();
         changed = false;
      }
   }

   void BeadColorChangeTool::onKeyPressedImpl(guint key) {
   }


   BeadColorChangeTool* BeadColorChangeTool::singleton = NULL;

}

