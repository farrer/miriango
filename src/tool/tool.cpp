/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tool.h"
#include "../common/pattern.h"
#include "../common/controller.h"

namespace Miriango {

   Tool::Tool(const EditorType& type) {
     this->editorType = type;
   }
   
   Tool::~Tool() {
   }

   void Tool::onMouseMoved(double mouseX, double mouseY) {
      mutex.lock();
         onMouseMovedImpl(mouseX, mouseY);
      mutex.unlock();
   }

   void Tool::onMouseClicked(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      mutex.lock();
         onMouseClickedImpl(mouseX, mouseY, button);
      mutex.unlock();
   }

   void Tool::onMousePressed(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      mutex.lock();
         onMousePressedImpl(mouseX, mouseY, button);
      mutex.unlock();
   }

   void Tool::onMouseDragged(double mouseX, double mouseY, 
               const MouseEvent::MouseButton& button) {
      mutex.lock();
         onMouseDraggedImpl(mouseX, mouseY, button);
      mutex.unlock();
   }

   void Tool::onMouseReleased(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      mutex.lock();
         onMouseReleasedImpl(mouseX, mouseY, button);
      mutex.unlock();     
   }

   void Tool::onKeyPressed(guint key) {
      mutex.lock();
         onKeyPressedImpl(key);
      mutex.unlock();     
   }

   void Tool::onKeyReleased(guint key) {
      mutex.lock();
         onKeyReleasedImpl(key);
      mutex.unlock(); 
   }


   void Tool::getBeadCoordinate(double mouseX, double mouseY, 
         int& beadX, int& beadY) {
      
      Pattern* pattern = Controller::getCurrentPattern();

      beadX = (int) ((mouseX-1) / Bead::getHalfWidth(pattern->getZoom()));
      beadY = (int) ((mouseY-1) / Bead::getHalfHeight(pattern->getZoom()));
   }


}


