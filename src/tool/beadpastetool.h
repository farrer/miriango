/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_paste_tool_h_
#define _miriango_bead_paste_tool_h_

#include "tool.h"
#include <list>
#include <set>

namespace Miriango {

   /*! Base class for all Paste Tools. */
   class BeadPasteTool : public Tool {
      public:
         /*! Destructor */
         virtual ~BeadPasteTool();

         /*! Define the selection to be pasted
          * \param list of beads to be pasted */
         void setSelection(std::set<Bead*>& selection);
         /*! Clear current selection */
         void clearSelection();

      protected:
         /*! Constructor */
         BeadPasteTool(const Tool::EditorType& editorType);

         /*! Keeps trace of mouse position for recall when angle changes */
         void onMouseMovedImpl(double mouseX, double mouseY) override;
         /*! Implementation of onMouseMoved. Use mouseX, mouseY values
          * saved at the instance */
         virtual void onMouseMovedImpl()=0;
         /*! Unused */
         void onKeyPressedImpl(guint key) override;
         /*! Arrow left and right to subtract or add 90 degrees to 
          * rotation angle */
         void onKeyReleasedImpl(guint key) override;
         /*! Rotate the relative selection coordinate by current rotation
          * angle */
         void rotateRelativeCoordinate(int& x, int& y);

         /*! List of beads to be pasted */
         std::list<Bead*> beads;
         /*! Area needed to paste current beads */
         Gdk::Rectangle beadsArea;
         /*! Rotation angle to apply to the paste */
         int angle;
         double mouseX;
         double mouseY;
         Bead* lastBead; /**< Last initial bead for paste */
   };

}

#endif

