/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadpastecolortool.h"
#include "../common/controller.h"
#include "../common/pattern.h"

namespace Miriango {

   void BeadPasteColorTool::init() {
      singleton = new BeadPasteColorTool();
   }

   void BeadPasteColorTool::finish() {
      if(singleton) {
         delete singleton;
      }
   }
   
   BeadPasteColorTool* BeadPasteColorTool::getSingleton() {
      return singleton;
   }
   
   BeadPasteColorTool::BeadPasteColorTool() 
                      :BeadPasteTool(Tool::BEAD_EDITOR) {
   }

   BeadPasteColorTool::~BeadPasteColorTool() {
   }

   void BeadPasteColorTool::onMouseMovedImpl() {
      if(beads.size() > 0) {
         Pattern* pattern = Controller::getCurrentPattern();

         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
         Bead* selBead = pattern->getOverlapedBeadInUse(beadX, beadY);
         if(selBead != lastBead) {
            lastBead = selBead;
            pattern->clearDrawList();
            if(selBead != NULL) {
               for(std::list<Bead*>::iterator it = beads.begin();
                     it != beads.end(); ++it) {
                  Bead* curBead = *it;
                  /* Define bead to affect, based on orientation angle */
                  int curX = curBead->getX();
                  int curY = curBead->getY();
                  rotateRelativeCoordinate(curX, curY);
                  Bead* bead = pattern->getBead(curX + selBead->getX(),
                        curY + selBead->getY());
                  if((bead != NULL) && (bead->isUsed())) {
                     pattern->drawBeadStroke(bead->getX(), bead->getY(),
                           Color::YELLOW);
                     pattern->drawBeadFill(bead->getX(), bead->getY(),
                           curBead->getColor());
                     pattern->setDirtyArea(bead, pattern->getZoom());
                  }
               }
            }
            pattern->notifyDirty();
         }
      }

   }

   void BeadPasteColorTool::onMouseClickedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
      if(beads.size() > 0) {
         Pattern* pattern = Controller::getCurrentPattern();
         pattern->clearDrawList();

         int beadX=-1, beadY=-1;
         getBeadCoordinate(mouseX, mouseY, beadX, beadY);
         Bead* selBead = pattern->getOverlapedBeadInUse(beadX, beadY);
         if(selBead != NULL) {
            for(std::list<Bead*>::iterator it = beads.begin();
                it != beads.end(); ++it) {
               Bead* curBead = *it;
               /* Define bead to affect, based on orientation angle */
               int curX = curBead->getX();
               int curY = curBead->getY();
               rotateRelativeCoordinate(curX, curY);
               Bead* bead = pattern->getBead(curX + selBead->getX(),
                     curY + selBead->getY());
               if((bead != NULL) && (bead->isUsed()) && 
                  (bead->getColor() != curBead->getColor())) {
                  bead->setColor(curBead->getColor(), false);
                  pattern->setDirtyArea(bead, pattern->getZoom());
               }
            }

            if(beads.size() > 0) {
               pattern->getUsedColors().updateDisplay();
            }
            pattern->doneAction();
         }
      }
   }

   void BeadPasteColorTool::onMousePressedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }
   
   void BeadPasteColorTool::onMouseDraggedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }

   void BeadPasteColorTool::onMouseReleasedImpl(double mouseX, double mouseY, 
         const MouseEvent::MouseButton& button) {
   }

   BeadPasteColorTool* BeadPasteColorTool::singleton = NULL;

}


