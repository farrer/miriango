/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadcolortool.h"
#include "../common/controller.h"
#include "../common/pattern.h"

namespace Miriango {

   BeadColorTool::BeadColorTool()
                 :Tool(Tool::BEAD_EDITOR) {
      highBead = NULL;
   }

   BeadColorTool::~BeadColorTool() {
   }

   void BeadColorTool::onKeyReleasedImpl(guint key) {
   }
   
   void BeadColorTool::onMouseMovedImpl(double mouseX, double mouseY) {
      Pattern* pattern = Controller::getCurrentPattern();

      int beadX = -1, beadY = -1;
      getBeadCoordinate(mouseX, mouseY, beadX, beadY);
      
      Bead* bead = pattern->getOverlapedBeadInUse(beadX, beadY);

      if(bead != highBead) {
         /* Bead under mouse changed, let's highlight it */
         pattern->clearDrawList();
         highBead = bead;

         if(bead != NULL) {
            pattern->drawBeadStroke(bead->getX(), bead->getY(), 
                  Color::YELLOW);
            pattern->setDirtyArea(bead, pattern->getZoom());
         }
         pattern->notifyDirty();
      }
   }

}

