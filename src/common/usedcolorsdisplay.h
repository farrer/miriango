/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_used_colors_display_h_
#define _miriango_used_colors_display_h_

#include <gtkmm.h>
#include <map>
#include <vector>
#include "color.h"

namespace Miriango {

   /*! A widget to display current pattern used colors */
   class UsedColorsDisplay : public Gtk::DrawingArea {
      public:
         /*! Constructor */
         UsedColorsDisplay(Gtk::Container* container);
         /*! Destructor */
         ~UsedColorsDisplay();

         /*! Update current colors displayed to the ones defined by
          * the map whose use is greater than 0. */
         void update(std::map<int, int>& map);
         /*! Clear current colors displayed (usually when no more
          * patterns opened on screen) */
         void clear();

      protected:
         bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
         bool onButtonReleaseEvent(GdkEventButton* event);

      private:
         /*! Set the whole display dirty, to redraw.  */
         void setDirty();
         /*! current colors displayed */
         std::vector<int> colors;
         Gtk::ScrolledWindow scroller;
         static const int COLOR_HEIGHT;

   };

}


#endif


