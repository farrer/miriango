/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "drawingarea.h"
#include "bead.h"

namespace Miriango {

   DrawingArea::DrawingArea() {
      definedDirtyArea = false;
   }
   
   DrawingArea::~DrawingArea() {
   }

   void DrawingArea::setDirtyArea(Bead* bead, float zoom) {
      setDirtyArea(bead->getX(), bead->getY(), zoom);
   }
   
   void DrawingArea::setDirtyArea(int posX, int posY, float zoom) {
      setDirtyArea(1 + Bead::getHalfWidth(zoom) * posX,
            1 + Bead::getHalfHeight(zoom) * posY,
            Bead::getWidth(zoom), Bead::getHeight(zoom));
   }

   void DrawingArea::setDirtyArea(float x, float y, float w, float h) {
      if(!definedDirtyArea) {
         /* use the new area */
         definedDirtyArea = true;
         dirtyArea.set(x, y, w, h);
      } else {
         /* grow the area to envolve both old and new dirty areas */
         float x2 = x + w - 1;
         float y2 = y + h - 1;
         float curX2 = dirtyArea.getX() + dirtyArea.getWidth() - 1;
         float curY2 = dirtyArea.getY() + dirtyArea.getHeight() - 1;

         if(x < dirtyArea.getX()) {
            dirtyArea.setX(x);
         }
         if(y < dirtyArea.getY()) {
            dirtyArea.setY(y);
         }
         if(x2 > curX2) {
            curX2 = x2;
         }
         if(y2 > curY2) {
            curY2 = y2;
         }
         dirtyArea.setWidth(curX2 - dirtyArea.getX() + 1);
         dirtyArea.setHeight(curY2 - dirtyArea.getY() + 1);
      }
   }

   void DrawingArea::setAllDirty() {
      definedDirtyArea = true;
      dirtyArea.set(0, 0, get_allocation().get_width(), 
            get_allocation().get_height());
   }

   void DrawingArea::notifyDirty() {
      if(definedDirtyArea) {
         Glib::RefPtr<Gdk::Window> win = get_window();
         if(win) {
            /* Grow area to avoid artifacts */
            Gdk::Rectangle dirty;
            if(dirtyArea.getX() >= 1) {
               dirty.set_x(floor(dirtyArea.getX() - 1));
            } else {
               dirty.set_x(floor(dirtyArea.getX()));
            }
            if(dirtyArea.getY() >= 1) {
               dirty.set_y(floor(dirtyArea.getY() - 1));
            } else {
               dirty.set_y(floor(dirtyArea.getY()));
            }
            dirty.set_width(ceil(dirtyArea.getWidth() + 2));
            dirty.set_height(ceil(dirtyArea.getHeight() + 2));
            /* invalidade area */
            win->invalidate_rect(dirty, false);
         }
      }
   }

   void DrawingArea::clear() {
      definedDirtyArea = false;
   }

   void DrawingArea::clearAll() {
      if(!definedDirtyArea) {
         setAllDirty();
      }
      definedDirtyArea = false;
   }

}
