/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bead.h"
#include "pattern.h"

namespace Miriango {

   const float Bead::DIST =  0.5f;
   const float Bead::WIDTH =  8.0f;
   const float Bead::HEIGHT = 6.0f;
   const float Bead::HALF_WIDTH  = (Bead::WIDTH / 2.0f) + Bead::DIST;
   const float Bead::HALF_HEIGHT = (Bead::HEIGHT / 2.0f) + Bead::DIST;

   Bead::Bead(UsedColors* usedColors) {
      this->usedColors = usedColors;
      this->used = false;
   }

   Bead& Bead::operator=(const Bead& b) {
      setColor(b.color, false);
      if(b.used) {
         use(false);
      } else {
         unuse(false);
      }
      
      return *this;
   }

   void Bead::use(bool updateColorsView) {
      if(!this->used) {
         if(this->usedColors != NULL) {
         this->usedColors->useColor(color, updateColorsView);
         }
         this->used = true;
      }
   }

   void Bead::unuse(bool updateColorsView) {
      if(this->used) { 
         if(this->usedColors != NULL) {
            this->usedColors->unuseColor(color, updateColorsView);
         }
         this->used = false;
      }
   }

   void Bead::setCoordinate(int x, int y) {
      this->x = x;
      this->y = y;
   }

   bool Bead::setColor(float red, float green, float blue,
         bool updateColorsView) {
      Color newColor(red, green, blue);
      return setColor(newColor, updateColorsView);
   }

   bool Bead::setColor(const Color& newColor, bool updateColorsView) {
      if(newColor != this->color) {
         if(this->usedColors != NULL) {
            usedColors->useColor(newColor, false);
            usedColors->unuseColor(this->color, false);
            if(updateColorsView) {
               usedColors->updateDisplay();
            }
         }
         this->color = newColor;
         return true;
      }
      return false;
   }

   void Bead::draw(const Cairo::RefPtr<Cairo::Context>& cr, float zoom) {
      Bead::draw(cr, color, Color::BLACK, x, y, zoom);
   }

   void Bead::draw(const Cairo::RefPtr<Cairo::Context>& cr, 
         const Color& fillColor, const Color& strokeColor, 
         int x, int y, float zoom) {
         drawFill(cr, fillColor, x, y, zoom);
         drawStroke(cr, strokeColor, x, y, zoom);
   }

   /* Note for bead draw: a circle with diameter 1.0f, centered at
    * (0.5f, 0.5f), scaled to the oval size and translated to
    * the desired position. */
   void Bead::drawStroke(const Cairo::RefPtr<Cairo::Context>& cr, 
         const Color& strokeColor, int x, int y, float zoom) {
      cr->save();
      cr->set_antialias(Cairo::ANTIALIAS_SUBPIXEL);
      cr->set_line_width(zoom / Pattern::MAX_ZOOM_LEVEL * 0.125);
      cr->translate(1 + HALF_WIDTH * x * zoom, 1 + HALF_HEIGHT * y * zoom);
      cr->scale(WIDTH * zoom, HEIGHT * zoom);
      cr->arc(0.5f, 0.5f, 0.5f, 0.0f, 2.0f * M_PI);
      cr->set_source_rgba(strokeColor.getRed(), strokeColor.getGreen(), 
            strokeColor.getBlue(), strokeColor.getAlpha());
      cr->stroke();
      cr->restore();
   }

   void Bead::drawFill(const Cairo::RefPtr<Cairo::Context>& cr, 
         const Color& fillColor, int x, int y, float zoom) {
      cr->save();
      
      Cairo::RefPtr<Cairo::RadialGradient> grad = 
         Cairo::RadialGradient::create(0.5f, 0.5f, 0.25f, 0.5f, 0.5f, 0.5f);
      grad->add_color_stop_rgba(0, fillColor.getRed(), fillColor.getGreen(), 
            fillColor.getBlue(), fillColor.getAlpha());
      grad->add_color_stop_rgba(1, 0.0f, 0.0f, 0.0f, 1.0f);

      cr->set_antialias(Cairo::ANTIALIAS_SUBPIXEL);
      cr->translate(1 + HALF_WIDTH * x * zoom, 
            1 + HALF_HEIGHT * y * zoom);
      cr->scale(WIDTH * zoom, HEIGHT * zoom);
      cr->arc(0.5f, 0.5f, 0.5f, 0.0f, 2.0f * M_PI);
      cr->set_source(grad);
      cr->fill();
      cr->restore();
   }

   float Bead::getHalfWidth(float zoom) {
      return HALF_WIDTH * zoom;
   }

   float Bead::getWidth(float zoom) {
      return WIDTH * zoom;
   }

   float Bead::getHalfHeight(float zoom) {
      return HALF_HEIGHT * zoom;
   }

   float Bead::getHeight(float zoom) {
      return HEIGHT * zoom;
   }


}


