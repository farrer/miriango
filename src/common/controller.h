/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_controller_h
#define _miriango_controller_h

#include "miriangoconfig.h"
#include <list>
#include <gtkmm.h>
#include "color.h"
#include "pattern.h"
#include "usedcolorsdisplay.h"
#include "../tool/tool.h"


namespace Miriango {

   class Controller {
      public:

         enum ToolType {
            TOOL_NONE,
            TOOL_PATTERN_EDIT,
            TOOL_BEAD_COLOR_CHANGE,
            TOOL_BEAD_COLOR_LINE,
            TOOL_BEAD_COLOR_FILL,
            TOOL_BEAD_COLOR_PICK,
            TOOL_CUT_PATTERN,
            TOOL_COPY_PATTERN,
            TOOL_PASTE_PATTERN,
            TOOL_COPY_COLOR,
            TOOL_PASTE_COLOR,
         };

         /*! Constructor */
         static void init();
         /*! Destructor */
         static void finish();

         /*! \return current editing Pattern */
         static Pattern* getCurrentPattern();
         /*! Set Pattern currently editing */
         static void setCurrentPattern(Pattern* pattern);
         /*! Set Pattern currently editing to the one that owns the
          * defined widget (page) */
         static void setCurrentPattern(Gtk::Widget* widget);
         /*! Add a pattern to the controller's list */
         static void addPattern(Pattern* pattern);
         /*! Remove Pattern from list and free its memmory */
         static void removePattern(Pattern* pattern);
         /*! Get Pattern related to the widget, if any */
         static Pattern* getPattern(Gtk::Widget* widget);

         /*! Change current active tool */
         static void changeTool(const ToolType& tool);
         /*! \return pointer to the active tool */
         static Tool* getCurrentTool();

         static void setColorButtons(Gtk::ColorButton* left, 
               Gtk::ColorButton* right);
         static const Color getLeftColor();
         static const Color getRightColor();
         static void setLeftColor(const Color& color);
         static void setRightColor(const Color& color);

         static void setUsedColorsDisplay(UsedColorsDisplay* display);
         static UsedColorsDisplay* getUsedColorsDisplay();

         /*! Set if patterns should display grid or not */
         static void setShowGrid(bool value);
         /*! \return if patterns should display grid or not */
         static bool getShowGrid();

      private:
         static Pattern* currentPattern;
         static std::list<Pattern*> patterns;
         static ToolType currentTool;
         static Gtk::ColorButton* leftColorButton;
         static Gtk::ColorButton* rightColorButton;
         static UsedColorsDisplay* usedColorsDisplay;
         static bool showGrid;

   };

}


#endif


