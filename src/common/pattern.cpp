/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "pattern.h"
#include "controller.h"
#include "../tool/tool.h"
#include <math.h>

namespace Miriango {

    const float Pattern::MAX_ZOOM_LEVEL = 3.0f;
    const float Pattern::MIN_ZOOM_LEVEL = 1.0f;
    const float Pattern::DEFAULT_ZOOM_LEVEL = 1.5f;

   Pattern::Pattern(int w, int h, Gtk::Notebook* notebook)
           :MouseDrawingArea(this),
            MouseEvent(this) {
      char buf[128];
      snprintf(buf, 128, "(%dx%d)", w / 2 + 1, h / 2 + 1);
      this->sizeString = buf;
      this->width = w;
      this->height = h;
      this->zoomLevel = DEFAULT_ZOOM_LEVEL;
      this->filename = "";
      this->notebook = notebook;

      this->undoRedo = new UndoRedoController(this);
      this->beads = new BeadGrid(width, height, &usedColors);

      this->scrolledWindow.set_policy(Gtk::POLICY_ALWAYS, Gtk::POLICY_ALWAYS);

      int canvasW = (w + 1) * (int) ceilf(Bead::getHalfWidth(MAX_ZOOM_LEVEL));
      int canvasH = (h + 1) * (int) ceilf(Bead::getHalfHeight(MAX_ZOOM_LEVEL));

      set_size_request(canvasW, canvasH);
      this->show();
       
      this->scrolledWindow.add(*this);
      this->scrolledWindow.show();
   
      /* Notebook page label and close button */
      Gtk::HBox* pageHeader = Gtk::manage(new Gtk::HBox());
      pageTextLabel.set_text("Unknow " + sizeString);
      Gtk::Image* closeImage = Gtk::manage(new Gtk::Image(Gtk::Stock::CLOSE,
               Gtk::ICON_SIZE_MENU));
      Gtk::Button* closeButton = Gtk::manage(new Gtk::Button()); 
      closeButton->set_image(*closeImage);
      closeButton->set_relief(Gtk::RELIEF_NONE);
      closeButton->signal_clicked().connect(sigc::mem_fun(*this,
               &Pattern::onCloseButtonClicked));
      pageHeader->pack_start(pageTextLabel, Gtk::PACK_SHRINK);
      pageHeader->pack_end(*closeButton, Gtk::PACK_SHRINK);
      pageHeader->show_all();

      /* Apeend the notebook page with our new pattern */
      int page = notebook->append_page(this->scrolledWindow, *pageHeader);
      notebook->set_tab_reorderable(this->scrolledWindow, true);
      notebook->set_current_page(page);
      Controller::addPattern(this);

      /* Define our events and connect our signals */
      this->add_events(Gdk::POINTER_MOTION_MASK | 
            Gdk::BUTTON_MOTION_MASK | Gdk::BUTTON_PRESS_MASK |
            Gdk::BUTTON_RELEASE_MASK | Gdk::LEAVE_NOTIFY_MASK | 
            Gdk::ENTER_NOTIFY_MASK);
      this->scrolledWindow.add_events(Gdk::KEY_PRESS_MASK |
            Gdk::KEY_RELEASE_MASK);
      this->scrolledWindow.set_can_focus(true);

      this->signal_button_press_event().connect(
            sigc::mem_fun(*this, &Pattern::onButtonPressEvent));
      this->signal_button_release_event().connect(
            sigc::mem_fun(*this, &Pattern::onButtonReleaseEvent));
      this->signal_motion_notify_event().connect(
            sigc::mem_fun(*this, &Pattern::onMouseMoveEvent));
      this->signal_enter_notify_event().connect(
            sigc::mem_fun(*this, &Pattern::onMouseEnter));
      this->scrolledWindow.signal_leave_notify_event().connect(
            sigc::mem_fun(*this, &Pattern::onMouseLeave));
      this->scrolledWindow.signal_key_press_event().connect(sigc::mem_fun(
               *this, &Pattern::onKeyPressed));
      this->scrolledWindow.signal_key_release_event().connect(sigc::mem_fun(
               *this, &Pattern::onKeyReleased));
   }

   Pattern::~Pattern() {
      delete undoRedo;
   }

	void Pattern::setFilename(std::string filename) {
		this->filename = filename;
      setName(Glib::path_get_basename(filename));
	}

   void Pattern::setName(std::string name) {
      pageTextLabel.set_text(name + " " + sizeString);
   }
	
	bool Pattern::definedFile() {
		return !filename.empty();
	}

   bool Pattern::isValidCoordinate(int x, int y) {
      return (x >= 0 && x < width) && (y >= 0 && y < height);
   }

   Bead* Pattern::getBead(int x, int y) {
      return (*beads)(x, y);
   }
   
   void Pattern::setZoom(float zoom) {
      this->zoomLevel = zoom;
   }

	Bead* Pattern::getOverlapedBeadInUse(int x, int y) {
		Bead* res = NULL; 
		
		/* Check upper line */
		if((y - 1 >= 0) && (y - 1 < height)) {
			res = getOverlapedBeadInUseAtLine(x, y - 1);
		}
		/* Check same line */
		if((res == NULL) && (y < height) && (y >= 0)) {
			res = getOverlapedBeadInUseAtLine(x, y);
		}
		/* Check bottom line */
		if((y + 1 >= 0) && (y + 1 < height) && (res == NULL)) {
			res = getOverlapedBeadInUseAtLine(x, y + 1);
		}
		
		return res;
	}
	
	Bead* Pattern::getOverlapedBeadInUseAtLine(int x, int y) {
		if((x - 1 >= 0) && (x - 1 < width) && ((*beads)(x - 1, y)->isUsed())) {
			return (*beads)(x - 1, y);
		}
		
		if((x >= 0 && x < width) && ((*beads)(x, y)->isUsed())) {
			return (*beads)(x, y);
		}
		
		if((x + 1 >= 0) && (x + 1 < width) && ((*beads)(x + 1, y)->isUsed())) {
			return (*beads)(x + 1, y);
		}
		
		return NULL;
	}

   void Pattern::populate(const FillType& fillType) {
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
            switch(fillType) {
               case FILL_TYPE_LOOM:
                  if((x % 2 == 0) && (y % 2 == 0)) {
                     (*beads)(x, y)->use(false);
                  } else {
                     (*beads)(x, y)->unuse(false);
                  }
               break;
               case FILL_TYPE_PEYOTE:
                  if((x % 4 == 0) && (y % 2 == 0)) {
                     (*beads)(x, y)->use(false);
                  } else if((x % 4 == 2) && (y % 2 == 1)) {
                     (*beads)(x, y)->use(false);
                  } else {
                     (*beads)(x, y)->unuse(false);
                  }
               break;
               case FILL_TYPE_TWO_DROP:
                  if(((x % 8 == 0) || (x % 8 == 2)) && (y % 2 == 0)) {
                     (*beads)(x, y)->use(false);
                  } else if(((x % 8 == 4) || (x % 8 == 6)) && (y % 2 == 1)) {
                     (*beads)(x, y)->use(false);
                  } else {
                     (*beads)(x, y)->unuse(false);
                  }
               break;
               case FILL_TYPE_BRICK:
                  if((y % 4 == 0) && (x % 2 == 0)) {
                     (*beads)(x, y)->use(false);
                  } else if((y % 4 == 2) && (x % 2 == 1)) {
                     (*beads)(x, y)->use(false);
                  } else {
                     (*beads)(x, y)->unuse(false);
                  }
               break;
				}
			}
		}
		usedColors.updateDisplay();
	}

   void Pattern::draw(const Cairo::RefPtr<Cairo::Context>& cr) {
      on_draw(cr);
   }

   bool Pattern::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {

      /* Precalculate bead half sizes */
      float beadHalfWidth = Bead::getHalfWidth(zoomLevel);
      float beadHalfHeight = Bead::getHalfHeight(zoomLevel);

      /* Get the area to draw */
      double clipX1=0.0, clipY1=0.0, clipX2=0.0, clipY2=0.0;
      cr->get_clip_extents(clipX1, clipY1, clipX2, clipY2);

      /* Calculate bead range on the area to draw */
      int initBeadX = floor((clipX1 - 1) / beadHalfWidth) - 1;
      int initBeadY = floor((clipY1 - 1) / beadHalfHeight) - 1;
      int finalBeadX = ceil((clipX2 + 1) / beadHalfWidth) + 1;
      int finalBeadY = ceil((clipY2 + 1) / beadHalfHeight) + 1;

      /* Keep it in [0,0,width,height] range */
      if(initBeadX < 0) {
         initBeadX = 0;
      }
      if(initBeadY < 0) {
         initBeadY = 0;
      }
      if(finalBeadX > width) {
         finalBeadX = width;
      }
      if(finalBeadY > height) {
         finalBeadY = height;
      }
      clear();

      /* Draw current limits for actual zoom */
      cr->save();
      cr->set_line_width(1.0f);
      cr->set_source_rgba(0.8f, 0.8f, 0.8f, 1.0f);
      cr->rectangle(0, 0, (width + 1) * beadHalfWidth,
                    (height + 1) * beadHalfHeight);
      cr->stroke();
      cr->restore();

      /* Draw grid, if desired */
      if(Controller::getShowGrid()) {
         int initGridX = (initBeadX % 2 == 0) ? initBeadX : initBeadX - 1;
         int initGridY = (initBeadY % 2 == 0) ? initBeadY : initBeadY - 1;
         cr->save();
         cr->set_line_width(1.0f);
         cr->set_source_rgba(0.8f, 0.8f, 0.8f, 1.0f);
         /* Vertical lines */
         for(int x = initGridX; x < finalBeadX; x += 2) {
            cr->move_to(x * beadHalfWidth, initGridY * beadHalfHeight);
            cr->line_to(x * beadHalfWidth, (finalBeadY + 1) * beadHalfHeight);
            cr->stroke();
         }
         /* Horizontal lines */
         for(int y = initGridY; y < finalBeadY; y += 2) {
            cr->move_to(initGridX * beadHalfWidth, y * beadHalfHeight);
            cr->line_to((finalBeadX + 1) * beadHalfWidth, y * beadHalfHeight);
            cr->stroke();
         }
         cr->restore();
      }

      /* Current line width */
      cr->set_line_width(zoomLevel / Pattern::MAX_ZOOM_LEVEL * 0.125);

      for(int x = initBeadX; x < finalBeadX; x++) {
         for(int y = initBeadY; y < finalBeadY; y++) {
            Bead* bead = (*beads)(x, y);
            if(bead->isUsed()) {
               bead->draw(cr, zoomLevel);
            }
         }
      }

      /* Draw mouse under inner list */
      drawInnerList(cr);

      return true;
   }

   void Pattern::onCloseButtonClicked() {
      this->notebook->remove_page(scrolledWindow);
   }

   void Pattern::onMouseMoved(float x, float y) {
      Tool* curTool = Controller::getCurrentTool();
      if(curTool != NULL) {
         curTool->onMouseMoved(x, y);
      }
   }

   void Pattern::onMouseClicked(const MouseButton& button, float x, float y) {
      Tool* curTool = Controller::getCurrentTool();
      if(curTool != NULL) {
         curTool->onMouseClicked(x, y, button);
      }
   }

   void Pattern::onMousePressed(const MouseButton& button, float x, float y) {
      Tool* curTool = Controller::getCurrentTool();
      if(curTool != NULL) {
         curTool->onMousePressed(x, y, button);
      }
   }

   void Pattern::onMouseDragged(const MouseButton& button, float x, float y) {
      Tool* curTool = Controller::getCurrentTool();
      if(curTool != NULL) {
         curTool->onMouseDragged(x, y, button);
      }
   }

   void Pattern::onMouseReleased(const MouseButton& button, float x, float y) {
      Tool* curTool = Controller::getCurrentTool();
      if(curTool != NULL) {
         curTool->onMouseReleased(x, y, button);
      }
   }

   bool Pattern::onKeyPressed(GdkEventKey* event) {
      Tool* curTool = Controller::getCurrentTool();
      if(curTool != NULL) {
         curTool->onKeyPressed(event->keyval);
      }
      return true;
   }

   bool Pattern::onMouseEnter(GdkEventCrossing* cross) {
      this->scrolledWindow.grab_focus();
      return true;
   }

   bool Pattern::onMouseLeave(GdkEventCrossing* cross) {
      if(getDrawListSize() > 0) {
         /* Clear temporary draw */
         clearDrawList();
         notifyDirty();
      }
      return true;
   }
   
   bool Pattern::onKeyReleased(GdkEventKey* event) {
      Tool* curTool = Controller::getCurrentTool();
      if(curTool != NULL) {
         curTool->onKeyReleased(event->keyval);
      }
      return true;
   }

	void Pattern::doneAction() {
      notifyDirty();
		undoRedo->saveCurrentState();
		//Miriango::updateMenus();
	}
	
}

