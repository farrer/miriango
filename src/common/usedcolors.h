/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_used_colors_h_
#define _miriango_used_colors_h_

#include <map>
#include "color.h"

namespace Miriango {

   /*! Controller for current colors used on a pattern */
   class UsedColors {
      public:
         UsedColors();

         /*! Tell the map that a bead now use the color
          * \param color Color used.
          * \param updateDraw true to update ui control draw if needed. */
         void useColor(const Color& color, bool updateDraw);

         /*! Tell the map that a bead no longer use the color 
          * @param color Color no longer used.
          * @param updateDraw true to update ui control draw if needed. */
         void unuseColor(const Color& color, bool updateDraw);

         /*! Update the gui element with the current colors display. */
         void updateDisplay();

      private:
         std::map<int, int> colors;
   };


}

#endif
