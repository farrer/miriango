/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "controller.h"
#include "pattern.h" 
#include "../tool/beadaddremovetool.h"
#include "../tool/beadcolorchangetool.h"
#include "../tool/beadcolorfilltool.h"
#include "../tool/beadcolorlinetool.h"
#include "../tool/beadcolorpicktool.h"
#include "../tool/beadcopycolortool.h"
#include "../tool/beadcopypatterntool.h"
#include "../tool/beadcutpatterntool.h"
#include "../tool/beadpastecolortool.h"
#include "../tool/beadpastepatterntool.h"
#include "../ebp/ebpcolormap.h"
#include "../editor/effects.h"

namespace Miriango {

   void Controller::init() {
      currentPattern = NULL;
      currentTool = TOOL_NONE;
      
      /* Initialize our tools */
      BeadAddRemoveTool::init();
      BeadColorChangeTool::init();
      BeadColorFillTool::init();
      BeadColorLineTool::init();
      BeadColorPickTool::init();
      BeadCopyColorTool::init();
      BeadCopyPatternTool::init();
      BeadCutPatternTool::init();
      BeadPasteColorTool::init();
      BeadPastePatternTool::init();

      EBPColorMap::init();
      Effects::init();
   }
   
   void Controller::finish() {
      while(patterns.size() > 0) {
         removePattern(patterns.back());
      }
      currentPattern = NULL;

      /* Done with all tools */
      BeadAddRemoveTool::finish();
      BeadColorChangeTool::finish();
      BeadColorFillTool::finish();
      BeadColorLineTool::finish();
      BeadColorPickTool::finish();
      BeadCopyColorTool::finish();
      BeadCopyPatternTool::finish();
      BeadCutPatternTool::finish();
      BeadPasteColorTool::finish();
      BeadPastePatternTool::finish();
      
      EBPColorMap::finish();
      Effects::finish();
   }

   Pattern* Controller::getCurrentPattern() {
      return currentPattern;
   }
         
   void Controller::setCurrentPattern(Pattern* pattern) {
      currentPattern = pattern;
   }
   
   void Controller::setCurrentPattern(Gtk::Widget* widget) {
      Pattern* pattern = Controller::getPattern(widget);
      if(pattern != NULL) {
         setCurrentPattern(pattern);
      }
   }

   Pattern* Controller::getPattern(Gtk::Widget* widget) {
      std::list<Pattern*>::iterator it;
      for(it = patterns.begin(); it != patterns.end(); ++it) {
         Pattern* pattern = *it;
         if(pattern->getWidget() == widget) {
            return pattern;
         }
      }

      return NULL;
   }
   
   void Controller::addPattern(Pattern* pattern) {
      patterns.push_back(pattern);
      setCurrentPattern(pattern);
   }

   void Controller::removePattern(Pattern* pattern) {
      /* Note: current pattern will change by onSwitchPage
       * call if it was the current displayed pattern. */
      patterns.remove(pattern);
      if(patterns.size() == 0) {
         currentPattern = NULL;
         usedColorsDisplay->clear();
      }
      delete pattern;
   }

   void Controller::changeTool(const ToolType& tool) {
      currentTool = tool;
   }

   Tool* Controller::getCurrentTool() {
      switch(currentTool) {
         case TOOL_BEAD_COLOR_CHANGE:
            return BeadColorChangeTool::getSingleton();
         case TOOL_BEAD_COLOR_FILL:
            return BeadColorFillTool::getSingleton();
         case TOOL_BEAD_COLOR_LINE:
            return BeadColorLineTool::getSingleton();
         case TOOL_BEAD_COLOR_PICK:
            return BeadColorPickTool::getSingleton();
         case TOOL_COPY_COLOR:
            return BeadCopyColorTool::getSingleton();
         case TOOL_PASTE_COLOR:
            return BeadPasteColorTool::getSingleton();
         case TOOL_CUT_PATTERN:
            return BeadCutPatternTool::getSingleton();
         case TOOL_COPY_PATTERN:
            return BeadCopyPatternTool::getSingleton();
         case TOOL_PASTE_PATTERN:
            return BeadPastePatternTool::getSingleton();
         case TOOL_PATTERN_EDIT:
            return BeadAddRemoveTool::getSingleton();
         case TOOL_NONE:
         default:
            return NULL;
      }
   }

   void Controller::setColorButtons(Gtk::ColorButton* left, 
         Gtk::ColorButton* right) {
      Controller::leftColorButton = left;
      Controller::rightColorButton = right;
   }

   const Color Controller::getLeftColor() {
      Color res;
      Gdk::RGBA cur = leftColorButton->get_rgba();
      res.set(cur.get_red(), cur.get_green(), cur.get_blue());

      return res;
   }

   const Color Controller::getRightColor() {
      Color res;
      Gdk::RGBA cur = rightColorButton->get_rgba();
      res.set(cur.get_red(), cur.get_green(), cur.get_blue());

      return res;
   }

   void Controller::setLeftColor(const Color& color) {
      Gdk::RGBA val;
      val.set_rgba(color.getRed(), color.getGreen(), color.getBlue(), 1.0f);
      leftColorButton->set_rgba(val); 
   }
   
   void Controller::setRightColor(const Color& color) {
      Gdk::RGBA val;
      val.set_rgba(color.getRed(), color.getGreen(), color.getBlue(), 1.0f);
      rightColorButton->set_rgba(val);
   }


   void Controller::setUsedColorsDisplay(UsedColorsDisplay* display) {
       usedColorsDisplay = display;
   }
   
   UsedColorsDisplay* Controller::getUsedColorsDisplay() {
      return usedColorsDisplay;
   }
   
   void Controller::setShowGrid(bool value) {
      if(value != showGrid) {
         showGrid = value;
         if(currentPattern != NULL) {
            /* Must redraw current pattern with ot without the grid */
            currentPattern->setAllDirty();
            currentPattern->notifyDirty();
         }
      }
   }

   bool Controller::getShowGrid() {
      return showGrid;
   }

   Pattern* Controller::currentPattern = NULL;
   std::list<Pattern*> Controller::patterns;
   Controller::ToolType Controller::currentTool;
   Gtk::ColorButton* Controller::leftColorButton = NULL;
   Gtk::ColorButton* Controller::rightColorButton = NULL;
   UsedColorsDisplay* Controller::usedColorsDisplay = NULL;
   bool Controller::showGrid = false;

}

