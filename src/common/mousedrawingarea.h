/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_mouse_drawing_area_h_
#define _miriango_mouse_drawing_area_h_

#include <gtkmm/drawingarea.h>
#include "miriangoconfig.h"
#include "color.h"
#include "drawingarea.h"

namespace Miriango {

   class MouseDrawingArea : public Miriango::DrawingArea {
      public:
         MouseDrawingArea(Pattern* pattern);
         virtual ~MouseDrawingArea();

         void drawRectangle(const Gdk::Point& c1, const Gdk::Point& c2,
               const Color& color);
         void drawRectangle(float x, float y, float w, float h, 
               const Color& color);
         void drawBeadStroke(int x, int y, const Color& color);
         void drawBeadFill(int x, int y, const Color& color);

         /*! Clear all the drawing area */
         void clearAll() override;
         
         /*! Clear all inner draws from list, marking its area as dirty */
         void clearDrawList();

         /*! \return current inner draw list size */
         size_t getDrawListSize() { return drawList.size(); };
         
         /*! Draw current drawing list to a cairo context */
         void drawInnerList(const Cairo::RefPtr<Cairo::Context>& cr);
      
      private:
         enum DrawType {
            RECTANGLE,
            BEAD_STROKE,
            BEAD_FILL
         };
         class InnerDraw {
            public:
               InnerDraw(const DrawType& type, const Color& color);

               void setPosition(float x, float y);
               void setSize(float width, float height);

               DrawType type;
               float x; 
               float y;
               float width;
               float height;
               Color color;
         };         
        
         /*! Set the area occupied by an InnerDraw as dirty */
         void setInnerDrawDirtyArea(InnerDraw* draw);

         Pattern* owner;
         std::list<InnerDraw*> drawList;
   };

}

#endif


