/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beadgrid.h"

namespace Miriango {

   BeadGrid::BeadGrid(int width, int height, UsedColors* usedColors)
            :beads(width * height, Bead(usedColors)) {
      this->width = width;
      this->height = height;

      for(int x = 0; x < width; x++) {
         for(int y = 0; y < height; y++) {
            this->beads[y * width + x].setCoordinate(x, y);
         }
      }
   }

   BeadGrid::~BeadGrid() {
   }

   Bead* BeadGrid::operator()(int x, int y) {
      if((x >= 0) && (x < width) && (y >=0 ) && (y < height)) {
         return &(this->beads[y * width + x]);
      }
      return NULL;
   }

}
