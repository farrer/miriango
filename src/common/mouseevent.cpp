/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mouseevent.h"

namespace Miriango {

   MouseEvent::MouseEvent(Gtk::Widget* owner) {
      this->lastX = -1.0;
      this->lastY = -1.0;
      this->lastButton = BUTTON_NONE;
      this->pressStarted = false;
      this->dragging = false;
      this->owner = owner;
   }

   MouseEvent::~MouseEvent() {
   }

   bool MouseEvent::onButtonPressEvent(GdkEventButton* event) {
      if(!pressStarted) {
         lastX = event->x;
         lastY = event->y;
         lastButton = (MouseButton) event->button;
         pressStarted = true;
         dragging = false;
      }

      return true;
   }
   
   bool MouseEvent::onButtonReleaseEvent(GdkEventButton* event) {
      if(pressStarted) {
         if(dragging) {
            /* Done with dragging */
            onMouseReleased((MouseButton) event->button, event->x, event->y);
            dragging = false;
         } else {
            /* Simple mouse click event */
            onMouseClicked((MouseButton) event->button, event->x, event->y);
         }
         pressStarted = false;
      }

      return true;
   }

   bool MouseEvent::onMouseMoveEvent(GdkEventMotion* event) {
      if(pressStarted) {
         if(!dragging) {
            /* Start drag */
            dragging = true;
            onMousePressed(lastButton, lastX, lastY);
            onMouseDragged(lastButton, event->x, event->y);
         } else {
            /* Dragging */
            onMouseDragged(lastButton, event->x, event->y);
         }
      } else {
         /* Simple move */
         onMouseMoved(event->x, event->y);
      }
      return true;
   }

}


