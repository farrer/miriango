/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_pattern_h_
#define _miriango_pattern_h_

#include <gtkmm.h>
#include <string>

#include "mousedrawingarea.h"
#include "bead.h"
#include "beadgrid.h"
#include "drawingarea.h"
#include "mouseevent.h"

#include "../editor/undoredocontroller.h"

namespace Miriango {

   class Pattern : public MouseDrawingArea, public Miriango::MouseEvent {
      public:

         enum FillType {
            FILL_TYPE_LOOM,
            FILL_TYPE_PEYOTE,
            FILL_TYPE_TWO_DROP,
            FILL_TYPE_BRICK
         };

         /*! Constructor 
          * \note The pattern is added to Controller.
          * \note The pattern is made the current visualized one
          * \param w pattern grid width
          * \param h pattern grid height
          * \param notebook GTK notebook to insert the pattern to display */
         Pattern(int w, int h, Gtk::Notebook* notebook);
         /*! Destructor */
         virtual ~Pattern();

         /*! \return the path to the pattern file, if defined */
         std::string getFilename() { return filename; };
         /*! Set path to the pattern file */
         void setFilename(std::string filename);
         /*! \return if path to the pattern file is defined or not */
         bool definedFile();
         /*! Set pattern's name (one that appears on tab title) */
         void setName(std::string name);
         /*! \return widget which is related to the Pattern's 
          * page on notebook */
         Gtk::Widget* getWidget() { return &scrolledWindow; };

         const int getWidth() const { return width; };
         const int getHeight() const { return height; };
         const float getZoom() const { return zoomLevel; };
         void setZoom(float zoom);

         bool isValidCoordinate(int x, int y);
         Bead* getBead(int x, int y);

         /** Get an used bead that overlaps the position (x, y).
          * \return pointer to used bead or NULL if none overlaps 
          *         or if all that do are unused. */
         Bead* getOverlapedBeadInUse(int x, int y);

         UsedColors& getUsedColors() { return usedColors; };
         UndoRedoController& getUndoRedo() { return *undoRedo; };

         /** Function that should be called every time an action was 
          * made to the pattern, to keep its undo-redo states up-to-date. */
         void doneAction(); 

         /*! Populate a pattern with used beads, according to a type
          * \param fillType filling type */
         void populate(const FillType& fillType);

         /*! Draw current pattern to a cairo context, usually for
          * exporting to an image */
         void draw(const Cairo::RefPtr<Cairo::Context>& cr);

         const static float MAX_ZOOM_LEVEL;
         const static float MIN_ZOOM_LEVEL;
         const static float DEFAULT_ZOOM_LEVEL;

      protected:
         
         /*! Gtkmm drawing function */
         bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

         void onCloseButtonClicked();

         void onMouseMoved(float x, float y) override;
         void onMouseClicked(const MouseButton& button, 
               float x, float y) override;
         void onMousePressed(const MouseButton& button, 
               float x, float y) override;
         void onMouseDragged(const MouseButton& button, 
               float x, float y) override;
         void onMouseReleased(const MouseButton& button, 
               float x, float y) override;
         bool onMouseEnter(GdkEventCrossing* cross);
         bool onMouseLeave(GdkEventCrossing* cross);
         bool onKeyPressed(GdkEventKey* event);
         bool onKeyReleased(GdkEventKey* event);

      private:
         Bead* getOverlapedBeadInUseAtLine(int x, int y);

         /*! Pattern grid width */
         int width;
         /*! Pattern grid height */
         int height;
         /** Beads possible for the pattern. Used and unused ones. */
         BeadGrid* beads;
         /*! Current pattern zoom level */
         float zoomLevel;

         std::string filename;
         std::string sizeString;

         /*! Overlay used do display pattern */
         //Gtk::Overlay overlay;
         Gtk::ScrolledWindow scrolledWindow;
         Gtk::Notebook* notebook;
         Gtk::Label pageTextLabel;

         /*! Controller for current colors used by the pattern */
         UsedColors usedColors;
         UndoRedoController* undoRedo;
   };

}

#endif


