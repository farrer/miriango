/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usedcolorsdisplay.h"
#include "controller.h"

namespace Miriango {

   const int UsedColorsDisplay::COLOR_HEIGHT = 16;

   UsedColorsDisplay::UsedColorsDisplay(Gtk::Container* container) {
      this->set_hexpand(true);
      this->set_vexpand(true);
      this->add_events(Gdk::POINTER_MOTION_MASK |
            Gdk::BUTTON_MOTION_MASK | Gdk::BUTTON_PRESS_MASK |
            Gdk::BUTTON_RELEASE_MASK);
      this->signal_button_release_event().connect(sigc::mem_fun(*this,
               &UsedColorsDisplay::onButtonReleaseEvent));
      this->scroller.add(*this);

      container->add(scroller);
      
      this->scroller.set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_ALWAYS);
   }
   
   UsedColorsDisplay::~UsedColorsDisplay() {
   }

   void UsedColorsDisplay::clear() {
      colors.clear();
      setDirty();
      set_size_request(get_allocation().get_width(), COLOR_HEIGHT);
   }

   void UsedColorsDisplay::update(std::map<int, int>& map) {
      std::map<int, int>::iterator it;
      /* Add all used colors to the list */
      colors.clear(); 
      for(it = map.begin(); it != map.end(); ++it) {
        if(it->second > 0) {
           colors.push_back(it->first);
        }
      }
 
      int neededHeight = colors.size() * COLOR_HEIGHT;
      if(neededHeight > get_allocation().get_height()) {
         set_size_request(get_allocation().get_width(), neededHeight);
      }

      setDirty();
   }

   void UsedColorsDisplay::setDirty() {
      /* Mark to redraw the whole display */
      Glib::RefPtr<Gdk::Window> win = get_window();
      if(win) {
         Gdk::Rectangle dirtyArea;
         dirtyArea.set_x(0);
         dirtyArea.set_y(0);
         dirtyArea.set_width(get_allocation().get_width());
         dirtyArea.set_height(get_allocation().get_height());
         win->invalidate_rect(dirtyArea, false);
      }
   }

   bool UsedColorsDisplay::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
      std::vector<int>::iterator it;
      int curY = 0;
      int width = get_allocation().get_width();
      for(it = colors.begin(); it != colors.end(); ++it) {
         Color color(*it);
         cr->save();
         cr->rectangle(0, curY, width, COLOR_HEIGHT);
         cr->set_source_rgba(color.getRed(), color.getGreen(), color.getBlue(),
               color.getAlpha());
         cr->fill();
         cr->restore();

         curY += COLOR_HEIGHT;
      }
      return true;
   }

   bool UsedColorsDisplay::onButtonReleaseEvent(GdkEventButton* event) {
      size_t index = event->y / COLOR_HEIGHT;
      if(index >= 0 && index < colors.size()) {
         Color color(colors[index]);
         if(event->button == 1) {
            Controller::setLeftColor(color);
         } else {
            Controller::setRightColor(color);
         }
      }

      return true;
   }

}

