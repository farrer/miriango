/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_grid_h_
#define _miriango_bead_grid_h_

#include "bead.h"
#include <vector>

namespace Miriango {

   /*! The grid array of Beads */
   class BeadGrid {
      public:
         /*! Constructor 
          * \param width grid width
          * \param height grid height
          * \param usedColors pointer to the UsedColors, if any. */
         BeadGrid(int width, int height, UsedColors* usedColors);
         /*! Destructor */
         ~BeadGrid();

         /*! \return pointer of Bead at position x, y or NULL */
         Bead* operator()(int x, int y);

      private:
         int width;    /** Grid Width */
         int height;   /** Grid Height */ 
         std::vector<Bead> beads; /** Beads composing the grid */
   };

}


#endif

