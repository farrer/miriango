/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_color_h_
#define _miriango_color_h_

namespace Miriango {

   /*! Color representation in RGB space */
   class Color {
      public:
         /*! Default contructor. Set color to black */
         Color();
         /*! Constructor, from RGB */
         Color(float r, float g, float b);
         /*! Constructor, from RGBA*/
         Color(float r, float g, float b, float a);
         /*! Constructor
          * \param color hexadecimal color representation */
         Color(int color);
      
         /*! Copy color from another one */
         Color& operator=(const Color& c);
         bool operator<(const Color& c) const;
         bool operator!=(const Color& c) const;
         bool operator==(const Color& c) const;
         
         /*! Set color component values */
         void set(float r, float g, float b, float a=1.0f);
         /*! Interpolate color components from another color */
         void interpolate(const Color& color);

         const float getRed() const { return r;};
         const float getGreen() const { return g;};
         const float getBlue() const { return b;}
         const float getAlpha() const { return a;}
         
         const int getHexValue() const { return hexValue; };

         static Color BLACK;
         static Color BLUE;
         static Color GREEN;
         static Color RED;
         static Color YELLOW;

      private:
         void calculateHexValue();

         float r;
         float g;
         float b;
         float a;

         int hexValue;
   };



}

#endif

