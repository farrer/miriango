/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usedcolors.h"
#include "controller.h"

namespace Miriango {

   UsedColors::UsedColors() {
   }

   void UsedColors::useColor(const Color& color, bool updateDraw) {
      bool shouldUpdate = updateDraw;
      int value = 0;
      std::map<int, int>::iterator it;

      it = colors.find(color.getHexValue());
      if(it != colors.end()) {
         value = it->second;
         /* No need to update, as already on map */
         shouldUpdate = false;
      } 
      colors[color.getHexValue()] = value + 1;

      if(shouldUpdate) {
         updateDisplay();
      }
   }

   void UsedColors::unuseColor(const Color& color, bool updateDraw) {
      int value = 0;
      std::map<int, int>::iterator it;

      it = colors.find(color.getHexValue());
      if(it != colors.end()) {
         value = it->second;
         value--;
         if(value == 0) {
            colors.erase(it);
            if(updateDraw) {
               updateDisplay();
            }
         } else {
            colors[color.getHexValue()] = value;
         }
      }

   }

   void UsedColors::updateDisplay() {
      Controller::getUsedColorsDisplay()->update(colors);
   }

}

