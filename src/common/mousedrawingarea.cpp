/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mousedrawingarea.h"
#include "pattern.h"

namespace Miriango {

   MouseDrawingArea::MouseDrawingArea(Pattern* pattern) {
      this->owner = pattern;
   }

   MouseDrawingArea::~MouseDrawingArea() {
      clearDrawList();
   }

   void MouseDrawingArea::clearAll() {
      DrawingArea::clearAll();
      clearDrawList();
   }

   void MouseDrawingArea::clearDrawList() {
      while(drawList.size() > 0) {
         /* Remove draw from the list */
         InnerDraw* draw = drawList.back();
         drawList.pop_back();
         /* Must set its area as dirty to redraw things under it */
         setInnerDrawDirtyArea(draw);
         /* Finally, delete the no more used draw */
         delete draw;
      }
   }

   void MouseDrawingArea::setInnerDrawDirtyArea(InnerDraw* draw) {
      if(draw->type == RECTANGLE) {
         DrawingArea::setDirtyArea(draw->x, draw->y, draw->width, draw->height);
      } else {
         DrawingArea::setDirtyArea(draw->x, draw->y, owner->getZoom());
      }
   }

   void MouseDrawingArea::drawRectangle(const Gdk::Point& c1, 
         const Gdk::Point& c2, const Color& color) {
      float x1 = (c1.get_x() <= c2.get_x()) ? c1.get_x() : c2.get_x();
      float y1 = (c1.get_y() <= c2.get_y()) ? c1.get_y() : c2.get_y();
      float x2 = (c1.get_x() > c2.get_x()) ? c1.get_x() : c2.get_x();
      float y2 = (c1.get_y() > c2.get_y()) ? c1.get_y() : c2.get_y();

      drawRectangle(x1, y1, x2 - x1 + 1, y2 - y1 + 1, color);
   }

   void MouseDrawingArea::drawRectangle(float x, float y, float w, float h, 
         const Color& color) {
      InnerDraw* draw = new InnerDraw(RECTANGLE, color);
      draw->setPosition(x, y);
      draw->setSize(w, h);
      drawList.push_back(draw);
      setInnerDrawDirtyArea(draw);
   }

   void MouseDrawingArea::drawBeadStroke(int x, int y, const Color& color) {
      InnerDraw* draw = new InnerDraw(BEAD_STROKE, color);
      draw->setPosition(x, y);
      drawList.push_back(draw);
      setInnerDrawDirtyArea(draw);
   }
   
   void MouseDrawingArea::drawBeadFill(int x, int y, const Color& color) {
      InnerDraw* draw = new InnerDraw(BEAD_FILL, color);
      draw->setPosition(x, y);
      drawList.push_back(draw);
      setInnerDrawDirtyArea(draw);
   }

   void MouseDrawingArea::drawInnerList(
         const Cairo::RefPtr<Cairo::Context>& cr) {
      for(std::list<InnerDraw*>::iterator it = drawList.begin();
          it != drawList.end(); ++it) {
         InnerDraw* draw = *it;
         if(draw->type == RECTANGLE) {
            cr->save();
            cr->rectangle(draw->x, draw->y, draw->width, draw->height);
            cr->set_source_rgba(draw->color.getRed(), draw->color.getGreen(),
                  draw->color.getBlue(), draw->color.getAlpha());
            cr->stroke();
            cr->restore();
         } else {
            int posX = (int)draw->x;
            int posY = (int)draw->y;
            if(draw->type == BEAD_STROKE) {
               Bead::drawStroke(cr, draw->color, posX, posY, owner->getZoom());
            } else {
               Bead::drawFill(cr, draw->color, posX, posY, owner->getZoom());
            }
         }
      }
   }


   MouseDrawingArea::InnerDraw::InnerDraw(const DrawType& type, 
         const Color& color) {
      this->type = type;
      this->color = color;
      this->x = 0.0f;
      this->y = 0.0f;
      this->width = 0.0f;
      this->height = 0.0f;
   }
   
   void MouseDrawingArea::InnerDraw::setPosition(float x, float y) {
      this->x = x;
      this->y = y;
   }

   void MouseDrawingArea::InnerDraw::setSize(float width, float height) {
      this->width = width;
      this->height = height;
   }

}
