/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "color.h"
#include <math.h>

namespace Miriango {

   Color::Color() {
      this->r = 0.0f;
      this->g = 0.0f;
      this->b = 0.0f;
      this->a = 1.0f;
      this->hexValue = 0;
   }

   Color::Color(float r, float g, float b) {
      this->r = r;
      this->g = g;
      this->b = b;
      this->a = 1.0f;
      calculateHexValue();
   }

   Color::Color(float r, float g, float b, float a) {
      this->r = r;
      this->g = g;
      this->b = b;
      this->a = a;
      calculateHexValue();
   }

   Color::Color(int color) {
      this->r = ((color >> 16) & 0xFF) / 255.0f;
      this->g = ((color >> 8) & 0xFF) / 255.0f;
      this->b = ((color) & 0xFF) / 255.0f;
      this->a = 1.0f;
      this->hexValue = color;
   }
      
   Color& Color::operator=(const Color& c) {
      this->r = c.r;
      this->g = c.g;
      this->b = c.b;
      this->a = 1.0f;
      this->hexValue = c.hexValue;
      return *this;
   }
   
   bool Color::operator<(const Color& c) const {
      return hexValue < c.hexValue;
   }


   bool Color::operator!=(const Color& c) const {
      return !operator==(c);
   }

   bool Color::operator==(const Color& c) const {
      return hexValue == c.hexValue;
   }

   void Color::calculateHexValue() {
      int intR = (int)(floor(r * 255)); 
      int intG = (int)(floor(g * 255));
      int intB = (int)(floor(b * 255));

      hexValue = ((intR & 0xff) << 16) + ((intG & 0xff) << 8) + (intB & 0xff);
   }
   
   void Color::set(float r, float g, float b, float a) {
      this->r = r;
      this->g = g;
      this->b = b;
      this->a = a;
      calculateHexValue();
   }

   void Color::interpolate(const Color& color) {
      this->r = this->r +  (color.r - this->r) / 2.0f;
      this->g = this->g +  (color.g - this->g) / 2.0f;
      this->b = this->b +  (color.b - this->b) / 2.0f;
      this->a = 1.0f;
      calculateHexValue();
   }

   Color Color::BLACK = Color(0.0f, 0.0f, 0.0f);
   Color Color::BLUE = Color(0.0f, 0.0f, 1.0f);
   Color Color::GREEN = Color(0.0f, 1.0f, 0.0f);
   Color Color::RED = Color(1.0f, 0.0f, 0.0f);
   Color Color::YELLOW = Color(1.0f, 1.0f, 0.0f);

}

