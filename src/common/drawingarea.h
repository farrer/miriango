/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_drawing_area_h_
#define _miriango_drawing_area_h_

#include <gtkmm.h>
#include "miriangoconfig.h"
#include "bead.h"

namespace Miriango {
 
   class DirtyArea {
      public:
         DirtyArea() {
            set(-1.0f, -1.0f, 0.0f, 0.0f);
         }
         DirtyArea(float x, float y, float width, float height) {
            set(x, y, width, height);
         }

         DirtyArea& operator=(const DirtyArea& area) {
            set(area.x, area.y, area.width, area.height);
            return *this;
         }

         void set(float x, float y, float width, float height) {
            this->x = x; this->y = y; 
            this->width = width; this->height = height;
         }

         void setX(float x) { this->x = x; };
         float getX() { return x; };
         void setY(float y) { this->y = y; };
         float getY() { return y; };
         void setWidth(float width) { this->width = width; };
         float getWidth() { return width; };
         void setHeight(float height) { this->height = height; };
         float getHeight() { return height; };

      private:
         float x;
         float y;
         float width;
         float height;
   };

   class DrawingArea : public Gtk::DrawingArea {
      public:
         DrawingArea();
         virtual ~DrawingArea();

         /*! Set the dirty area as the one occupied by a Bead with specific
          * zoom level. */
         void setDirtyArea(Bead* bead, float zoom);
         /*! Set the dirty area as the one occupied by a Bead with specific
          * zoom level. */
         void setDirtyArea(int posX, int posY, float zoom);
         /*! Set the dirty area to a rectangle */
         void setDirtyArea(float x, float y, float w, float h);
         /*! Set the dirty area to whole drawing area */
         void setAllDirty();

         /*! Clear the current defined dirty area */
         virtual void clear();
         /*! Clear all the drawing area */
         virtual void clearAll();

         /*! Notify the window of current dirty area */
         void notifyDirty();

      private:
         DirtyArea dirtyArea;
         bool definedDirtyArea;
   };

}

#endif

