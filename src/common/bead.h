/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_bead_h_
#define _miriango_bead_h_

#include "usedcolors.h"
#include <gtkmm/drawingarea.h>

namespace Miriango {
   
    class Pattern;

    /*! A single bead representation */
    class Bead {
       public:
          /*! Constructor
           * \param usedColors pointer to the Pattern's UsedColors. */
          Bead(UsedColors* usedColors);

          /*! Copy color and used flag from another bead.
           * \note: keep untouched its coordinate and UsedColors pointer. */
          Bead& operator=(const Bead& b);

          /*! \return if is used or not */
          bool isUsed() { return used; };

          /*! Set bead as used.
           * \param updateColorsView if should update GUI with 
           *        current colors */
          void use(bool updateColorsView);

          /*! Set bead as unused.
           * \param updateColorsView if should update GUI with 
           *        current colors */
          void unuse(bool updateColorsView);

          /*! Set current bead color
           * \return true if color changed, false if it was the same color. */
          bool setColor(float red, float green, float blue, 
                bool updateColorsView);
          bool setColor(const Color& newColor, bool updateColorsView);

          /*! Get current bead color */
          const Color getColor() const { return color; };

          /*! \return X coordinate on pattern grid */
          int getX() { return x; };
          /*! \return Y coordinate on pattern grid */
          int getY() { return y; };

          /*! Set coordinate on pattern grid */
          void setCoordinate(int x, int y);

          /*! Draw the bead to context with defined zoom */
          void draw(const Cairo::RefPtr<Cairo::Context>& cr, float zoom);

          /*! Draw a bead representation using a cairo context */
          static void draw(const Cairo::RefPtr<Cairo::Context>& cr, 
                const Color& fillColor, const Color& strokeColor, 
                int x, int y, float zoom);

          /*! Draw the bead stroke (contorn) using a cairo context */
          static void drawStroke(const Cairo::RefPtr<Cairo::Context>& cr, 
                const Color& strokeColor, int x, int y, float zoom);

          /*! Draw the bead inner fill using a cairo context */
          static void drawFill(const Cairo::RefPtr<Cairo::Context>& cr, 
                const Color& fillColor, int x, int y, float zoom);

          /*! \return bead half width at zoom level */
          static float getHalfWidth(float zoom);
          /*! \return bead width at zoom level */
          static float getWidth(float zoom);
          /*! \return bead half height at zoom level */
          static float getHalfHeight(float zoom);
          /*! \return bead height at zoom level */
          static float getHeight(float zoom);


          const static float DIST;
          const static float WIDTH;
          const static float HEIGHT;
          const static float HALF_WIDTH;
          const static float HALF_HEIGHT;
 

       private:
          bool used;      /**< if is currently used */
          Color color;    /**< its color */
          int x;          /**< x coordinate on pattern */
          int y;          /**< y coordinate on pattern */

          UsedColors* usedColors; /**< current colors used on pattern */
    };

}

#endif

