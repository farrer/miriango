/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_ebp_color_map_h
#define _miriango_ebp_color_map_h

#include "../common/color.h"
#include <map>
#include <string>

namespace Miriango {
   
   /*! Color map used by Easy Bead Patterns application. Created here to
    * allow import of EBP files on Miriango. */
   class EBPColorMap {
      public:
         /*! Init ColorMap to use */
         static void init();
         /*! Finish the use of ColorMap */
         static void finish();

         /*! Destructor */
         ~EBPColorMap();

         /*! \return the single instance of this class */
         static EBPColorMap* getSingleton();

         /*! Get {\link Color} based on EBP bead name 
          * \param beadName string with name in format "DB0000"
          * \return Color found. Fallback to {\link Color}.BLACK */
         const Color& getColor(std::string beadName);

         /*! Get the nearest EBP color name to a {\link Color}.
          * \param color Color to retrieve nearest EBP color name.
          * \return string with name of the nearest color */
         const std::string getNearestColorName(const Color& color);
      
      private:
         /*! Constructor */
         EBPColorMap();

         /*! Convert color from r,g,b integers [0, 255] to a new Color */
         Color* getColor(int r, int g, int b);
 
         /*! The map used */
         std::map<std::string, Color*> colorMap;
         /*! The single instance of this class */
         static EBPColorMap* singleton;
   };
}

#endif


