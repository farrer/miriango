/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ebpcolormap.h"
#include <math.h>

namespace Miriango {

   void EBPColorMap::init() {
      singleton = new EBPColorMap();
   }

   void EBPColorMap::finish() {
      if(singleton != NULL) {
         delete singleton;
      }
   }

   EBPColorMap::EBPColorMap() {
      colorMap["DB0001"] = getColor(15, 15, 15);
      colorMap["DB0002"] = getColor(7, 31, 59);
      colorMap["DB0002"] = getColor(7, 31, 59);
      colorMap["DB0003"] = getColor(5, 39, 27);
      colorMap["DB0004"] = getColor(46, 19, 73);
      colorMap["DB0005"] = getColor(7, 31, 59);
      colorMap["DB0006"] = getColor(7, 31, 59);
      colorMap["DB0007"] = getColor(41, 15, 25);
      colorMap["DB0010"] = getColor(0, 0, 0);
      colorMap["DB0011"] = getColor(39, 45, 25);
      colorMap["DB0012"] = getColor(115, 27, 33);
      colorMap["DB0021"] = getColor(83, 87, 67);
      colorMap["DB0022"] = getColor(63, 35, 0);
      colorMap["DB0022L"] = getColor(123, 83, 27);
      colorMap["DB0023"] = getColor(63, 35, 0);
      colorMap["DB0024"] = getColor(55, 71, 23);
      colorMap["DB0025"] = getColor(7, 31, 59);
      colorMap["DB0026"] = getColor(31, 31, 35);
      colorMap["DB0027"] = getColor(15, 59, 47);
      colorMap["DB0029"] = getColor(63, 35, 0);
      colorMap["DB0031"] = getColor(247, 179, 15);
      colorMap["DB0032"] = getColor(165, 165, 145);
      colorMap["DB0033"] = getColor(231, 179, 75);
      colorMap["DB0034"] = getColor(180, 145, 60);
      colorMap["DB0035"] = getColor(216, 216, 191);
      colorMap["DB0037"] = getColor(219, 155, 139);
      colorMap["DB0038"] = getColor(165, 165, 145);
      colorMap["DB0040"] = getColor(179, 95, 43);
      colorMap["DB0041"] = getColor(205, 205, 205);
      colorMap["DB0042"] = getColor(231, 179, 75);
      colorMap["DB0043"] = getColor(183, 31, 51);
      colorMap["DB0044"] = getColor(139, 179, 179);
      colorMap["DB0045"] = getColor(255, 145, 0);
      colorMap["DB0046"] = getColor(0, 255, 127);
      colorMap["DB0047"] = getColor(51, 0, 204);
      colorMap["DB0048"] = getColor(91, 91, 91);
      colorMap["DB0050"] = getColor(255, 255, 255);
      colorMap["DB0051"] = getColor(255, 255, 255);
      colorMap["DB0052"] = getColor(255, 255, 245);
      colorMap["DB0053"] = getColor(255, 230, 105);
      colorMap["DB0054"] = getColor(200, 129, 90);
      colorMap["DB0055"] = getColor(255, 205, 255);
      colorMap["DB0056"] = getColor(204, 0, 255);
      colorMap["DB0057"] = getColor(75, 159, 167);
      colorMap["DB0058"] = getColor(59, 135, 151);
      colorMap["DB0059"] = getColor(87, 71, 131);
      colorMap["DB0060"] = getColor(127, 179, 139);
      colorMap["DB0061"] = getColor(102, 0, 51);
      colorMap["DB0062"] = getColor(223, 39, 115);
      colorMap["DB0063"] = getColor(84, 57, 133);
      colorMap["DB0064"] = getColor(187, 163, 111);
      colorMap["DB0065"] = getColor(167, 115, 31);
      colorMap["DB0066"] = getColor(255, 255, 253);
      colorMap["DB0067"] = getColor(247, 187, 119);
      colorMap["DB0068"] = getColor(247, 167, 119);
      colorMap["DB0069"] = getColor(203, 163, 111);
      colorMap["DB0070"] = getColor(231, 139, 147);
      colorMap["DB0071"] = getColor(255, 205, 255);
      colorMap["DB0072"] = getColor(234, 173, 234);
      colorMap["DB0073"] = getColor(175, 70, 185);
      colorMap["DB0074"] = getColor(204, 50, 204);
      colorMap["DB0075"] = getColor(243, 51, 87);
      colorMap["DB0076"] = getColor(56, 176, 222);
      colorMap["DB0077"] = getColor(0, 145, 199);
      colorMap["DB0078"] = getColor(187, 243, 227);
      colorMap["DB0079"] = getColor(75, 165, 165);
      colorMap["DB0080"] = getColor(215, 203, 235);
      colorMap["DB0081"] = getColor(125, 125, 115);
      colorMap["DB0082"] = getColor(255, 231, 231);
      colorMap["DB0083"] = getColor(230, 255, 230);
      colorMap["DB0084"] = getColor(127, 179, 139);
      colorMap["DB0085"] = getColor(47, 91, 127);
      colorMap["DB0086"] = getColor(47, 91, 127);
      colorMap["DB0087"] = getColor(51, 0, 0);
      colorMap["DB0088"] = getColor(155, 63, 43);
      colorMap["DB0089"] = getColor(75, 55, 55);
      colorMap["DB0098"] = getColor(199, 43, 59);
      colorMap["DB0099"] = getColor(199, 160, 75);
      colorMap["DB0100"] = getColor(255, 205, 100);
      colorMap["DB0101"] = getColor(196, 157, 72);
      colorMap["DB0102"] = getColor(140, 78, 25);
      colorMap["DB0103"] = getColor(142, 35, 35);
      colorMap["DB0104"] = getColor(95, 0, 95);
      colorMap["DB0105"] = getColor(147, 23, 63);
      colorMap["DB0106"] = getColor(247, 163, 155);
      colorMap["DB0107"] = getColor(127, 135, 140);
      colorMap["DB0108"] = getColor(151, 99, 131);
      colorMap["DB0109"] = getColor(255, 255, 255);
      colorMap["DB0110"] = getColor(199, 215, 215);
      colorMap["DB0111"] = getColor(127, 135, 140);
      colorMap["DB0112"] = getColor(127, 179, 139);
      colorMap["DB0113"] = getColor(115, 191, 195);
      colorMap["DB0114"] = getColor(127, 135, 140);
      colorMap["DB0115"] = getColor(140, 78, 25);
      colorMap["DB0116"] = getColor(123, 0, 27);
      colorMap["DB0117"] = getColor(147, 112, 219);
      colorMap["DB0118"] = getColor(121, 64, 47);
      colorMap["DB0119"] = getColor(155, 112, 77);
      colorMap["DB0121"] = getColor(145, 78, 36);
      colorMap["DB0122"] = getColor(124, 68, 48);
      colorMap["DB0123"] = getColor(131, 119, 39);
      colorMap["DB0124"] = getColor(159, 159, 95);
      colorMap["DB0125"] = getColor(0, 89, 47);
      colorMap["DB0126"] = getColor(124, 50, 30);
      colorMap["DB0127"] = getColor(79, 79, 47);
      colorMap["DB0128"] = getColor(75, 19, 63);
      colorMap["DB0129"] = getColor(124, 50, 30);
      colorMap["DB0131"] = getColor(79, 89, 41);
      colorMap["DB0132"] = getColor(84, 84, 84);
      colorMap["DB0133"] = getColor(99, 99, 27);
      colorMap["DB0134"] = getColor(75, 75, 75);
      colorMap["DB0135"] = getColor(96, 41, 123);
      colorMap["DB0141"] = getColor(255, 255, 255);
      colorMap["DB0144"] = getColor(181, 111, 39);
      colorMap["DB0145"] = getColor(226, 230, 118);
      colorMap["DB0146"] = getColor(170, 115, 120);
      colorMap["DB0147"] = getColor(200, 207, 27);
      colorMap["DB0148"] = getColor(0, 91, 35);
      colorMap["DB0149"] = getColor(5, 130, 155);
      colorMap["DB0150"] = getColor(105, 65, 53);
      colorMap["DB0151"] = getColor(255, 130, 5);
      colorMap["DB0152"] = getColor(0, 255, 0);
      colorMap["DB0157"] = getColor(255, 255, 235);
      colorMap["DB0158"] = getColor(167, 140, 150);
      colorMap["DB0159"] = getColor(255, 30, 55);
      colorMap["DB0160"] = getColor(255, 255, 0);
      colorMap["DB0161"] = getColor(255, 102, 0);
      colorMap["DB0162"] = getColor(179, 59, 75);
      colorMap["DB0163"] = getColor(35, 142, 35);
      colorMap["DB0164"] = getColor(109, 219, 219);
      colorMap["DB0165"] = getColor(5, 5, 255);
      colorMap["DB0166"] = getColor(51, 171, 143);
      colorMap["DB0167"] = getColor(105, 155, 255);
      colorMap["DB0168"] = getColor(125, 121, 115);
      colorMap["DB0169"] = getColor(205, 255, 5);
      colorMap["DB0170"] = getColor(140, 78, 25);
      colorMap["DB0171"] = getColor(231, 235, 123);
      colorMap["DB0172"] = getColor(159, 51, 67);
      colorMap["DB0173"] = getColor(165, 129, 151);
      colorMap["DB0174"] = getColor(230, 255, 105);
      colorMap["DB0175"] = getColor(5, 155, 105);
      colorMap["DB0176"] = getColor(96, 193, 248);
      colorMap["DB0177"] = getColor(5, 155, 155);
      colorMap["DB0178"] = getColor(0, 0, 255);
      colorMap["DB0179"] = getColor(130, 130, 120);
      colorMap["DB0180"] = getColor(75, 55, 55);
      colorMap["DB0181"] = getColor(181, 111, 39);
      colorMap["DB0182"] = getColor(0, 67, 23);
      colorMap["DB0183"] = getColor(55, 0, 153);
      colorMap["DB0184"] = getColor(93, 47, 7);
      colorMap["DB0191"] = getColor(235, 183, 167);
      colorMap["DB0200"] = getColor(255, 255, 253);
      colorMap["DB0201"] = getColor(255, 255, 253);
      colorMap["DB0202"] = getColor(255, 255, 253);
      colorMap["DB0203"] = getColor(255, 255, 245);
      colorMap["DB0204"] = getColor(243, 215, 175);
      colorMap["DB0205"] = getColor(223, 187, 143);
      colorMap["DB0206"] = getColor(255, 171, 155);
      colorMap["DB0207"] = getColor(255, 171, 155);
      colorMap["DB0208"] = getColor(227, 183, 115);
      colorMap["DB0209"] = getColor(231, 231, 235);
      colorMap["DB0210"] = getColor(183, 143, 155);
      colorMap["DB0211"] = getColor(255, 255, 245);
      colorMap["DB0214"] = getColor(167, 19, 43);
      colorMap["DB0215"] = getColor(109, 219, 219);
      colorMap["DB0216"] = getColor(51, 0, 204);
      colorMap["DB0217"] = getColor(100, 177, 175);
      colorMap["DB0218"] = getColor(109, 215, 215);
      colorMap["DB0220"] = getColor(255, 255, 254);
      colorMap["DB0221"] = getColor(255, 255, 245);
      colorMap["DB0230"] = getColor(243, 195, 91);
      colorMap["DB0231"] = getColor(255, 255, 253);
      colorMap["DB0232"] = getColor(255, 251, 139);
      colorMap["DB0233"] = getColor(255, 205, 105);
      colorMap["DB0234"] = getColor(255, 223, 215);
      colorMap["DB0235"] = getColor(227, 123, 107);
      colorMap["DB0236"] = getColor(219, 112, 147);
      colorMap["DB0237"] = getColor(147, 219, 112);
      colorMap["DB0238"] = getColor(23, 147, 119);
      colorMap["DB0239"] = getColor(195, 243, 230);
      colorMap["DB0240"] = getColor(130, 130, 255);
      colorMap["DB0241"] = getColor(215, 203, 211);
      colorMap["DB0242"] = getColor(135, 135, 145);
      colorMap["DB0243"] = getColor(51, 0, 255);
      colorMap["DB0244"] = getColor(255, 180, 205);
      colorMap["DB0245"] = getColor(255, 155, 205);
      colorMap["DB0246"] = getColor(255, 110, 199);
      colorMap["DB0247"] = getColor(240, 75, 180);
      colorMap["DB0248"] = getColor(216, 191, 216);
      colorMap["DB0249"] = getColor(145, 103, 171);
      colorMap["DB0250"] = getColor(147, 112, 219);
      colorMap["DB0251"] = getColor(120, 120, 120);
      colorMap["DB0252"] = getColor(215, 215, 205);
      colorMap["DB0253"] = getColor(163, 99, 139);
      colorMap["DB0254"] = getColor(83, 87, 67);
      colorMap["DB0256"] = getColor(200, 169, 149);
      colorMap["DB0257"] = getColor(159, 183, 207);
      colorMap["DB0261"] = getColor(250, 247, 230);
      colorMap["DB0262"] = getColor(147, 194, 0);
      colorMap["DB0263"] = getColor(135, 151, 63);
      colorMap["DB0264"] = getColor(61, 112, 107);
      colorMap["DB0265"] = getColor(91, 53, 66);
      colorMap["DB0266"] = getColor(96, 129, 255);
      colorMap["DB0267"] = getColor(62, 79, 123);
      colorMap["DB0268"] = getColor(79, 77, 80);
      colorMap["DB0271"] = getColor(191, 195, 175);
      colorMap["DB0272"] = getColor(200, 145, 60);
      colorMap["DB0273"] = getColor(56, 69, 31);
      colorMap["DB0274"] = getColor(105, 155, 55);
      colorMap["DB0275"] = getColor(15, 59, 47);
      colorMap["DB0276"] = getColor(15, 59, 47);
      colorMap["DB0277"] = getColor(51, 0, 204);
      colorMap["DB0278"] = getColor(0, 0, 156);
      colorMap["DB0279"] = getColor(95, 0, 95);
      colorMap["DB0280"] = getColor(127, 43, 59);
      colorMap["DB0281"] = getColor(142, 35, 107);
      colorMap["DB0282"] = getColor(159, 51, 67);
      colorMap["DB0283"] = getColor(135, 7, 31);
      colorMap["DB0284"] = getColor(84, 57, 133);
      colorMap["DB0285"] = getColor(0, 51, 255);
      colorMap["DB0286"] = getColor(15, 91, 103);
      colorMap["DB0287"] = getColor(83, 47, 7);
      colorMap["DB0295"] = getColor(255, 0, 0);
      colorMap["DB0296"] = getColor(111, 27, 43);
      colorMap["DB0297"] = getColor(76, 30, 40);
      colorMap["DB0301"] = getColor(59, 59, 67);
      colorMap["DB0306"] = getColor(59, 59, 67);
      colorMap["DB0307"] = getColor(81, 81, 81);
      colorMap["DB0310"] = getColor(5, 5, 5);
      colorMap["DB0311"] = getColor(29, 45, 45);
      colorMap["DB0312"] = getColor(89, 63, 47);
      colorMap["DB0321"] = getColor(125, 125, 99);
      colorMap["DB0322"] = getColor(99, 75, 51);
      colorMap["DB0323"] = getColor(80, 53, 37);
      colorMap["DB0324"] = getColor(25, 81, 61);
      colorMap["DB0325"] = getColor(15, 91, 103);
      colorMap["DB0327"] = getColor(0, 79, 37);
      colorMap["DB0331"] = getColor(205, 155, 80);
      colorMap["DB0332"] = getColor(158, 153, 149);
      colorMap["DB0334"] = getColor(190, 145, 60);
      colorMap["DB0335"] = getColor(203, 187, 171);
      colorMap["DB0336"] = getColor(151, 155, 135);
      colorMap["DB0340"] = getColor(166, 103, 88);
      colorMap["DB0351"] = getColor(255, 255, 253);
      colorMap["DB0352"] = getColor(255, 255, 245);
      colorMap["DB0353"] = getColor(255, 247, 231);
      colorMap["DB0354"] = getColor(255, 235, 227);
      colorMap["DB0355"] = getColor(183, 147, 167);
      colorMap["DB0356"] = getColor(200, 175, 200);
      colorMap["DB0357"] = getColor(241, 241, 235);
      colorMap["DB0361"] = getColor(51, 51, 204);
      colorMap["DB0362"] = getColor(179, 59, 75);
      colorMap["DB0371"] = getColor(155, 123, 55);
      colorMap["DB0372"] = getColor(123, 111, 63);
      colorMap["DB0373"] = getColor(73, 89, 51);
      colorMap["DB0374"] = getColor(130, 155, 130);
      colorMap["DB0375"] = getColor(135, 177, 175);
      colorMap["DB0376"] = getColor(105, 130, 155);
      colorMap["DB0377"] = getColor(35, 35, 142);
      colorMap["DB0378"] = getColor(155, 30, 80);
      colorMap["DB0379"] = getColor(143, 111, 115);
      colorMap["DB0380"] = getColor(130, 111, 63);
      colorMap["DB0381"] = getColor(101, 112, 140);
      colorMap["DB0382"] = getColor(247, 226, 163);
      colorMap["DB0383"] = getColor(165, 184, 165);
      colorMap["DB0384"] = getColor(70, 62, 59);
      colorMap["DB0385"] = getColor(107, 176, 145);
      colorMap["DB0386"] = getColor(84, 85, 94);
      colorMap["DB0387"] = getColor(52, 61, 70);
      colorMap["DB0388"] = getColor(207, 190, 102);
      colorMap["DB0389"] = getColor(206, 153, 75);
      colorMap["DB0390"] = getColor(113, 111, 28);
      colorMap["DB0391"] = getColor(138, 139, 95);
      colorMap["DB0410"] = getColor(247, 179, 15);
      colorMap["DB0411"] = getColor(255, 211, 107);
      colorMap["DB0412"] = getColor(255, 235, 67);
      colorMap["DB0413"] = getColor(180, 205, 155);
      colorMap["DB0414"] = getColor(167, 215, 165);
      colorMap["DB0415"] = getColor(103, 159, 131);
      colorMap["DB0416"] = getColor(87, 143, 143);
      colorMap["DB0417"] = getColor(216, 205, 191);
      colorMap["DB0418"] = getColor(255, 215, 223);
      colorMap["DB0419"] = getColor(193, 159, 180);
      colorMap["DB0420"] = getColor(251, 99, 135);
      colorMap["DB0421"] = getColor(235, 99, 7);
      colorMap["DB0422"] = getColor(255, 0, 255);
      colorMap["DB0423"] = getColor(175, 43, 39);
      colorMap["DB0424"] = getColor(247, 223, 111);
      colorMap["DB0425"] = getColor(255, 0, 255);
      colorMap["DB0426"] = getColor(51, 255, 153);
      colorMap["DB0427"] = getColor(30, 255, 230);
      colorMap["DB0428"] = getColor(203, 55, 79);
      colorMap["DB0429"] = getColor(195, 159, 195);
      colorMap["DB0430"] = getColor(107, 55, 116);
      colorMap["DB0431"] = getColor(153, 0, 153);
      colorMap["DB0432"] = getColor(35, 157, 142);
      colorMap["DB0433"] = getColor(203, 172, 117);
      colorMap["DB0434"] = getColor(165, 122, 87);
      colorMap["DB0435"] = getColor(193, 131, 134);
      colorMap["DB0436"] = getColor(135, 136, 120);
      colorMap["DB0451"] = getColor(23, 71, 71);
      colorMap["DB0452"] = getColor(79, 79, 47);
      colorMap["DB0453"] = getColor(65, 65, 65);
      colorMap["DB0454"] = getColor(119, 63, 95);
      colorMap["DB0455"] = getColor(75, 19, 63);
      colorMap["DB0456"] = getColor(111, 99, 27);
      colorMap["DB0457"] = getColor(63, 79, 43);
      colorMap["DB0458"] = getColor(35, 91, 71);
      colorMap["DB0459"] = getColor(23, 71, 71);
      colorMap["DB0460"] = getColor(111, 47, 0);
      colorMap["DB0461"] = getColor(115, 43, 15);
      colorMap["DB0462"] = getColor(119, 63, 95);
      colorMap["DB0463"] = getColor(120, 25, 90);
      colorMap["DB0464"] = getColor(85, 23, 101);
      colorMap["DB0465"] = getColor(15, 91, 103);
      colorMap["DB0501"] = getColor(210, 155, 80);
      colorMap["DB0502"] = getColor(163, 107, 31);
      colorMap["DB0505"] = getColor(210, 155, 80);
      colorMap["DB0506"] = getColor(123, 83, 27);
      colorMap["DB0507"] = getColor(139, 83, 7);
      colorMap["DB0508"] = getColor(155, 130, 55);
      colorMap["DB0509"] = getColor(101, 87, 126);
      colorMap["DB0510"] = getColor(47, 91, 127);
      colorMap["DB0511"] = getColor(75, 115, 151);
      colorMap["DB0512"] = getColor(99, 143, 171);
      colorMap["DB0513"] = getColor(165, 165, 145);
      colorMap["DB0514"] = getColor(47, 87, 123);
      colorMap["DB0601"] = getColor(166, 42, 42);
      colorMap["DB0602"] = getColor(165, 15, 40);
      colorMap["DB0603"] = getColor(165, 15, 40);
      colorMap["DB0604"] = getColor(111, 99, 27);
      colorMap["DB0605"] = getColor(0, 91, 35);
      colorMap["DB0606"] = getColor(39, 55, 31);
      colorMap["DB0607"] = getColor(5, 155, 130);
      colorMap["DB0608"] = getColor(35, 107, 142);
      colorMap["DB0609"] = getColor(73, 27, 113);
      colorMap["DB0610"] = getColor(85, 40, 125);
      colorMap["DB0611"] = getColor(87, 19, 71);
      colorMap["DB0612"] = getColor(89, 51, 19);
      colorMap["DB0613"] = getColor(25, 25, 25);
      colorMap["DB0621"] = getColor(255, 215, 105);
      colorMap["DB0622"] = getColor(209, 146, 117);
      colorMap["DB0623"] = getColor(255, 243, 187);
      colorMap["DB0624"] = getColor(255, 223, 215);
      colorMap["DB0625"] = getColor(227, 127, 155);
      colorMap["DB0626"] = getColor(151, 195, 171);
      colorMap["DB0627"] = getColor(123, 227, 179);
      colorMap["DB0628"] = getColor(159, 255, 255);
      colorMap["DB0629"] = getColor(211, 179, 225);
      colorMap["DB0630"] = getColor(185, 185, 165);
      colorMap["DB0631"] = getColor(120, 120, 120);
      colorMap["DB0635"] = getColor(255, 255, 255);
      colorMap["DB0651"] = getColor(255, 180, 5);
      colorMap["DB0652"] = getColor(135, 135, 115);
      colorMap["DB0653"] = getColor(191, 59, 15);
      colorMap["DB0654"] = getColor(155, 30, 80);
      colorMap["DB0655"] = getColor(5, 200, 105);
      colorMap["DB0656"] = getColor(5, 155, 80);
      colorMap["DB0657"] = getColor(117, 103, 40);
      colorMap["DB0658"] = getColor(0, 255, 204);
      colorMap["DB0659"] = getColor(5, 205, 255);
      colorMap["DB0660"] = getColor(131, 91, 139);
      colorMap["DB0661"] = getColor(90, 55, 139);
      colorMap["DB0662"] = getColor(79, 47, 79);
      colorMap["DB0663"] = getColor(47, 79, 47);
      colorMap["DB0670"] = getColor(255, 255, 255);
      colorMap["DB0671"] = getColor(135, 115, 79);
      colorMap["DB0672"] = getColor(255, 255, 245);
      colorMap["DB0673"] = getColor(255, 255, 245);
      colorMap["DB0674"] = getColor(235, 199, 158);
      colorMap["DB0675"] = getColor(255, 239, 239);
      colorMap["DB0676"] = getColor(231, 235, 223);
      colorMap["DB0677"] = getColor(225, 235, 235);
      colorMap["DB0678"] = getColor(223, 195, 227);
      colorMap["DB0679"] = getColor(211, 211, 199);
      colorMap["DB0681"] = getColor(255, 145, 0);
      colorMap["DB0682"] = getColor(255, 102, 0);
      colorMap["DB0683"] = getColor(183, 31, 51);
      colorMap["DB0684"] = getColor(199, 87, 119);
      colorMap["DB0685"] = getColor(243, 71, 139);
      colorMap["DB0686"] = getColor(255, 235, 67);
      colorMap["DB0687"] = getColor(255, 235, 67);
      colorMap["DB0688"] = getColor(0, 255, 127);
      colorMap["DB0689"] = getColor(119, 135, 95);
      colorMap["DB0690"] = getColor(15, 99, 43);
      colorMap["DB0691"] = getColor(130, 205, 155);
      colorMap["DB0692"] = getColor(105, 230, 255);
      colorMap["DB0693"] = getColor(5, 80, 255);
      colorMap["DB0694"] = getColor(141, 94, 168);
      colorMap["DB0695"] = getColor(150, 85, 145);
      colorMap["DB0696"] = getColor(51, 0, 204);
      colorMap["DB0697"] = getColor(100, 100, 100);
      colorMap["DB0702"] = getColor(255, 205, 100);
      colorMap["DB0703"] = getColor(255, 130, 30);
      colorMap["DB0704"] = getColor(255, 5, 80);
      colorMap["DB0705"] = getColor(0, 255, 0);
      colorMap["DB0706"] = getColor(115, 191, 195);
      colorMap["DB0707"] = getColor(51, 0, 204);
      colorMap["DB0708"] = getColor(130, 130, 120);
      colorMap["DB0709"] = getColor(150, 70, 35);
      colorMap["DB0710"] = getColor(231, 235, 123);
      colorMap["DB0711"] = getColor(160, 125, 127);
      colorMap["DB0712"] = getColor(230, 255, 130);
      colorMap["DB0713"] = getColor(0, 91, 35);
      colorMap["DB0714"] = getColor(5, 155, 180);
      colorMap["DB0715"] = getColor(99, 65, 53);
      colorMap["DB0721"] = getColor(255, 255, 0);
      colorMap["DB0722"] = getColor(255, 102, 0);
      colorMap["DB0723"] = getColor(255, 0, 0);
      colorMap["DB0724"] = getColor(80, 155, 55);
      colorMap["DB0725"] = getColor(109, 219, 219);
      colorMap["DB0726"] = getColor(51, 0, 204);
      colorMap["DB0727"] = getColor(255, 30, 55);
      colorMap["DB0728"] = getColor(167, 140, 150);
      colorMap["DB0729"] = getColor(51, 171, 143);
      colorMap["DB0730"] = getColor(105, 155, 255);
      colorMap["DB0731"] = getColor(125, 121, 115);
      colorMap["DB0732"] = getColor(255, 255, 235);
      colorMap["DB0733"] = getColor(205, 255, 5);
      colorMap["DB0734"] = getColor(51, 0, 0);
      colorMap["DB0741"] = getColor(255, 255, 254);
      colorMap["DB0742"] = getColor(205, 165, 80);
      colorMap["DB0743"] = getColor(255, 255, 80);
      colorMap["DB0744"] = getColor(239, 119, 11);
      colorMap["DB0745"] = getColor(184, 49, 51);
      colorMap["DB0746"] = getColor(30, 155, 80);
      colorMap["DB0747"] = getColor(135, 195, 207);
      colorMap["DB0748"] = getColor(51, 0, 204);
      colorMap["DB0749"] = getColor(125, 121, 115);
      colorMap["DB0751"] = getColor(255, 255, 0);
      colorMap["DB0752"] = getColor(255, 102, 0);
      colorMap["DB0753"] = getColor(167, 19, 43);
      colorMap["DB0754"] = getColor(0, 125, 0);
      colorMap["DB0755"] = getColor(109, 219, 219);
      colorMap["DB0756"] = getColor(51, 0, 204);
      colorMap["DB0757"] = getColor(255, 30, 55);
      colorMap["DB0758"] = getColor(167, 140, 150);
      colorMap["DB0759"] = getColor(51, 171, 143);
      colorMap["DB0760"] = getColor(105, 155, 255);
      colorMap["DB0761"] = getColor(125, 121, 115);
      colorMap["DB0762"] = getColor(255, 255, 235);
      colorMap["DB0763"] = getColor(205, 255, 5);
      colorMap["DB0764"] = getColor(155, 80, 30);
      colorMap["DB0765"] = getColor(160, 132, 139);
      colorMap["DB0766"] = getColor(200, 225, 67);
      colorMap["DB0767"] = getColor(0, 107, 47);
      colorMap["DB0768"] = getColor(15, 145, 159);
      colorMap["DB0769"] = getColor(92, 64, 51);
      colorMap["DB0771"] = getColor(194, 179, 123);
      colorMap["DB0772"] = getColor(133, 94, 66);
      colorMap["DB0773"] = getColor(175, 75, 83);
      colorMap["DB0774"] = getColor(167, 19, 43);
      colorMap["DB0775"] = getColor(179, 59, 75);
      colorMap["DB0776"] = getColor(11, 127, 59);
      colorMap["DB0777"] = getColor(167, 71, 0);
      colorMap["DB0778"] = getColor(187, 51, 119);
      colorMap["DB0779"] = getColor(223, 103, 99);
      colorMap["DB0780"] = getColor(239, 103, 151);
      colorMap["DB0781"] = getColor(230, 155, 5);
      colorMap["DB0782"] = getColor(123, 55, 103);
      colorMap["DB0783"] = getColor(110, 46, 140);
      colorMap["DB0784"] = getColor(102, 0, 102);
      colorMap["DB0785"] = getColor(90, 45, 139);
      colorMap["DB0786"] = getColor(0, 235, 204);
      colorMap["DB0787"] = getColor(0, 155, 190);
      colorMap["DB0788"] = getColor(51, 107, 107);
      colorMap["DB0791"] = getColor(255, 0, 0);
      colorMap["DB0792"] = getColor(82, 127, 118);
      colorMap["DB0793"] = getColor(0, 255, 204);
      colorMap["DB0794"] = getColor(112, 46, 15);
      colorMap["DB0795"] = getColor(223, 59, 0);
      colorMap["DB0796"] = getColor(167, 19, 43);
      colorMap["DB0797"] = getColor(39, 91, 59);
      colorMap["DB0798"] = getColor(15, 145, 159);
      colorMap["DB0799"] = getColor(112, 72, 120);
      colorMap["DB0800"] = getColor(204, 102, 153);
      colorMap["DB0820"] = getColor(255, 239, 239);
      colorMap["DB0821"] = getColor(255, 227, 207);
      colorMap["DB0822"] = getColor(187, 167, 139);
      colorMap["DB0823"] = getColor(251, 247, 191);
      colorMap["DB0824"] = getColor(251, 215, 203);
      colorMap["DB0825"] = getColor(233, 184, 166);
      colorMap["DB0826"] = getColor(200, 169, 149);
      colorMap["DB0827"] = getColor(195, 175, 143);
      colorMap["DB0828"] = getColor(207, 255, 211);
      colorMap["DB0829"] = getColor(175, 207, 191);
      colorMap["DB0830"] = getColor(229, 240, 245);
      colorMap["DB0831"] = getColor(211, 223, 235);
      colorMap["DB0832"] = getColor(230, 232, 250);
      colorMap["DB0833"] = getColor(255, 239, 239);
      colorMap["DB0851"] = getColor(255, 255, 254);
      colorMap["DB0852"] = getColor(205, 165, 80);
      colorMap["DB0853"] = getColor(119, 65, 43);
      colorMap["DB0854"] = getColor(255, 255, 80);
      colorMap["DB0855"] = getColor(247, 139, 19);
      colorMap["DB0856"] = getColor(195, 39, 67);
      colorMap["DB0857"] = getColor(160, 132, 139);
      colorMap["DB0858"] = getColor(30, 147, 71);
      colorMap["DB0859"] = getColor(10, 112, 47);
      colorMap["DB0860"] = getColor(223, 255, 100);
      colorMap["DB0861"] = getColor(173, 234, 234);
      colorMap["DB0862"] = getColor(55, 155, 180);
      colorMap["DB0863"] = getColor(135, 135, 115);
      colorMap["DB0864"] = getColor(77, 77, 255);
      colorMap["DB0865"] = getColor(95, 57, 55);
      colorMap["DB0867"] = getColor(113, 71, 93);
      colorMap["DB0868"] = getColor(230, 200, 192);
      colorMap["DB0869"] = getColor(98, 89, 106);
      colorMap["DB0870"] = getColor(54, 55, 85);
      colorMap["DB0871"] = getColor(7, 31, 59);
      colorMap["DB0872"] = getColor(255, 102, 0);
      colorMap["DB0873"] = getColor(255, 30, 55);
      colorMap["DB0874"] = getColor(179, 59, 75);
      colorMap["DB0875"] = getColor(167, 140, 150);
      colorMap["DB0876"] = getColor(205, 255, 5);
      colorMap["DB0877"] = getColor(31, 147, 71);
      colorMap["DB0878"] = getColor(51, 171, 143);
      colorMap["DB0879"] = getColor(112, 219, 219);
      colorMap["DB0880"] = getColor(50, 50, 205);
      colorMap["DB0881"] = getColor(105, 155, 255);
      colorMap["DB0882"] = getColor(135, 135, 115);
      colorMap["DB0883"] = getColor(255, 255, 235);
      colorMap["DB0884"] = getColor(88, 35, 76);
      colorMap["DB0901"] = getColor(195, 135, 67);
      colorMap["DB0902"] = getColor(250, 125, 199);
      colorMap["DB0903"] = getColor(215, 227, 151);
      colorMap["DB0904"] = getColor(23, 147, 119);
      colorMap["DB0905"] = getColor(19, 191, 243);
      colorMap["DB0906"] = getColor(125, 75, 154);
      colorMap["DB0907"] = getColor(211, 187, 135);
      colorMap["DB0908"] = getColor(155, 143, 59);
      colorMap["DB0909"] = getColor(180, 155, 80);
      colorMap["DB0910"] = getColor(211, 215, 115);
      colorMap["DB0911"] = getColor(207, 181, 59);
      colorMap["DB0912"] = getColor(134, 78, 75);
      colorMap["DB0913"] = getColor(187, 103, 83);
      colorMap["DB0914"] = getColor(231, 59, 127);
      colorMap["DB0915"] = getColor(155, 55, 5);
      colorMap["DB0916"] = getColor(50, 205, 50);
      colorMap["DB0917"] = getColor(95, 145, 81);
      colorMap["DB0918"] = getColor(35, 142, 104);
      colorMap["DB0919"] = getColor(40, 130, 100);
      colorMap["DB0920"] = getColor(0, 145, 199);
      colorMap["DB0921"] = getColor(11, 83, 95);
      colorMap["DB0922"] = getColor(163, 123, 167);
      colorMap["DB0923"] = getColor(87, 26, 107);
      colorMap["DB0924"] = getColor(147, 63, 79);
      colorMap["DB0925"] = getColor(65, 65, 65);
      colorMap["DB0981"] = getColor(187, 135, 55);
      colorMap["DB0982"] = getColor(187, 103, 83);
      colorMap["DB0983"] = getColor(211, 215, 115);
      colorMap["DB0984"] = getColor(35, 165, 100);
      colorMap["DB0985"] = getColor( 47, 115, 139);
      colorMap["DB0986"] = getColor(123, 123, 175);
      colorMap["DB1301"] = getColor(255, 216, 0);
      colorMap["DB1302"] = getColor(187, 95, 19);
      colorMap["DB1304"] = getColor(102, 255, 204);
      colorMap["DB1308"] = getColor(247, 139, 175);
      colorMap["DB1310"] = getColor(240, 20, 235);
      colorMap["DB1312"] = getColor(147, 0, 79);
      colorMap["DB1315"] = getColor(159, 55, 185);
      colorMap["DB1318"] = getColor(0, 255, 255);
      colorMap["DB1319"] = getColor(31, 31, 35);
      colorMap["DB1333"] = getColor(235, 99, 7);
      colorMap["DB1335"] = getColor(231, 121, 175);
      colorMap["DB1338"] = getColor(255, 28, 174);
      colorMap["DB1340"] = getColor(204, 0, 153);
      colorMap["DB1341"] = getColor(203, 55, 79);
      colorMap["DB1342"] = getColor(167, 23, 95);
      colorMap["DB1343"] = getColor(159, 95, 185);
      colorMap["DB1345"] = getColor(140, 25, 153);
      colorMap["DB1347"] = getColor(112, 55, 145);
      colorMap["DB1363"] = getColor(247, 151, 111);
      colorMap["DB1371"] = getColor(211, 87, 147);
      colorMap["DB1376"] = getColor(155, 30, 105);
      colorMap["DB1379"] = getColor(159, 75, 185);
   }

   EBPColorMap::~EBPColorMap() {
   }

   EBPColorMap* EBPColorMap::getSingleton() {
      return singleton;
   }

   const Color& EBPColorMap::getColor(std::string beadName) {
      std::map<std::string, Color*>::iterator it;
      it = colorMap.find(beadName);
      if(it != colorMap.end()) {
         return *(colorMap[beadName]);
      }

      return Color::BLACK;
   }


   Color* EBPColorMap::getColor(int r, int g, int b) {
      return new Color(r / 255.0f, g / 255.0f, b / 255.0f);
   }


   const std::string EBPColorMap::getNearestColorName(const Color& color) {
      std::map<std::string, Color*>::iterator it;
      float curDist = 1000000;
      std::string colorName;
      for(it = colorMap.begin(); it != colorMap.end(); it++) {
         Color* c = it->second;
         float dist = fabs(color.getRed() - c->getRed()) + 
                      fabs(color.getGreen() - c->getGreen()) +
                      fabs(color.getBlue() - c->getBlue());
         if(dist < curDist) {
            colorName = it->first;
            curDist = dist;
         }
      }

      return colorName;
   }

   EBPColorMap* EBPColorMap::singleton = NULL;
}

