/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_data_dir_h
#define _miriango_data_dir_h

#include <string>
#include "miriangoconfig.h"

namespace Miriango {

   /*! Used to find the valid data directory for Miriango to use.
    * Start searching at ../data, then ./data and finally, at
    * the installed data dir (MIRIANGO_DATADIR) */
   class DataDir {
      public:
         static void init();
         static void finish();

         /*! \return the data directory to use, with trail / */
         static std::string getDirectory();

         /*! \return the locale directory to use */
         static std::string getLocaleDirectory();

      private:
         static bool tryDir(std::string dir);
         static std::string dataDir;
         static std::string localeDir;
   };

}

#endif
