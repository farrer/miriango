/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mrgfile.h"
#include "../common/controller.h"
#include "../common/pattern.h"
#include <iostream>
#include <fstream>

namespace Miriango {

   MRGFile::MRGFile() {
   }

   MRGFile::~MRGFile() {
   }

   bool MRGFile::load(const std::string& fileName, Gtk::Notebook* notebook,
         bool fullLoad) {
      bool res = XMLParser::load(fileName);
      if(!res) {
         return false;
      }
 
      bool loadedPattern = false;

      std::list<Node*>::iterator it;
      for(it = rootNodes.begin(); it != rootNodes.end(); ++it) {
         Node* root = *it;
         if(root->name == "pattern") {
            int width = 0;
            int height = 0;
            /* Let's get pattern attributes */
            for(std::list<Attribute*>::iterator att = root->attributes.begin();
                att != root->attributes.end(); ++att) {
                Attribute* attribute = *att;
                if(attribute->name == "height") {
                   height = attribute->getIntValue();
                } else if(attribute->name == "width" ) {
                   width = attribute->getIntValue();
                }
            }

            /* Create pattern */
            if(width <= 0 || height <= 0) {
               std::cerr << "ERROR: invalid size (" << width << ", "
                     << height << ")" << std::endl;
               return false;
            }
            Pattern* pattern = new Pattern(width, height, notebook);
            if(fullLoad) {
               pattern->setFilename(fileName);
            }
            loadedPattern = true;

            /* parse its children beads */
            std::list<Node*>::iterator ch;
            for(ch = root->children.begin(); ch != root->children.end(); ++ch) {
               Node* child = *ch;
               if(child->name == "bead") {
                  int x = -1;
                  int y = -1;
                  float red=0.0f, green=0.0f, blue=0.0f;
                  for(std::list<Attribute*>::iterator att = 
                        child->attributes.begin();
                        att != child->attributes.end(); ++att) {
                     Attribute* attribute = *att;
                     if(attribute->name == "x") {
                        x = attribute->getIntValue();
                     } else if(attribute->name == "y") {
                        y = attribute->getIntValue();
                     } else if(attribute->name == "red") {
                        red = attribute->getFloatValue();
                     } else if(attribute->name == "green") {
                        green = attribute->getFloatValue();
                     } else if(attribute->name == "blue") {
                        blue = attribute->getFloatValue();
                     }
                  }

                  Bead* bead = pattern->getBead(x, y);
                  if(bead == NULL) {
                     std::cerr << "ERROR: invalid bead at (" << x << ", "
                           << y << ")" << std::endl;
                     return false;
                  }
                  bead->use(false);
                  if(fullLoad) {
                     bead->setColor(Color(red, green, blue), false);
                  }
               }
            }
            pattern->getUsedColors().updateDisplay();
            pattern->doneAction();
         }
      }

      if(!loadedPattern) {
         std::cerr << "WARNING: No pattern was loaded from file!" << std::endl;
         return false;
      }

      return true;
   }


   bool MRGFile::save(const std::string& filename, Pattern* pattern) {
      std::ofstream file;

      file.open(filename.c_str(), std::ios::out | std::ios::binary);
      if(!file) {
         std::cerr << "ERROR: Couldn't open file '" << filename 
               << "' to save!" << std::endl;
         return false;
      }

      file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
           << std::endl;

      file << "<pattern height=\"" << pattern->getHeight() << "\" width=\""
           << pattern->getWidth() << "\">" << std::endl;

      for(int x = 0; x < pattern->getWidth(); x++) {
         for(int y = 0; y < pattern->getHeight(); y++) {
            Bead* bead = pattern->getBead(x, y);
            if(bead->isUsed()) {
               file << "<bead x=\"" << x << "\" y=\"" << y << "\" red=\""
                    << bead->getColor().getRed() << "\" green=\""
                    << bead->getColor().getGreen() << "\" blue=\""
                    << bead->getColor().getBlue() << "\"/>" << std::endl;
            }
         }
      }

      file << "</pattern>" << std::endl;
      file.close();

      /* Finally, define filename and done! */
      pattern->setFilename(filename);
      return true;
   }

}

