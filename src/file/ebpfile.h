/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_ebp_file_h
#define _miriango_ebp_file_h

#include "xmlparser.h"
#include "../common/pattern.h"

namespace Miriango {

   /*! Easy Bead Pattern File parser. Used on Miriango to import
    * files created on EBP. */
   class EBPFile : public XMLParser {
      public:
         EBPFile();
         ~EBPFile();
         /*! Load a Easy Bead Pattern file to Miriango */
         bool load(const std::string& fileName, Gtk::Notebook* notebook);
 
         /*! Save a miriango pattern as a Easy Bead Pattern file.
          * \note Only works for loom patterns. */
         bool save(const std::string& fileName, Pattern* pattern);
      private:
         int getPatternType(std::string name);
         void convertCoordinateByType(int& x, int& y, int type);
   };

}

#endif

