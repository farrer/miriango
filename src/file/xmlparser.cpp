/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "xmlparser.h"
#include <expat.h>
#include <iostream>
#include <sstream>
#include <fstream>

namespace Miriango {

   /* EXPAT parser functions */
   static void XMLCALL startElement(void *userData, 
         const XML_Char *name, const XML_Char **atts) {
      XMLParser* parser = (XMLParser*) userData;
      XMLParser::Node* parent = parser->_getCurrentElement();
      XMLParser::Node* node = new XMLParser::Node(parent);
      node->name = name;

      /*! Add as children to its parent */
      if(parent == NULL) {
         parser->_addRootNode(node);
      } else {
         parent->children.push_back(node);
      }
      parser->_setCurrentElement(node);

      /*! Create its attributes */
      for(int att = 0; atts[att]; att+=2) {
         XMLParser::Attribute* attribute = new XMLParser::Attribute(atts[att], 
               atts[att+1]);
         node->attributes.push_back(attribute);
      }
   }

   static void XMLCALL endElement(void *userData, const XML_Char *name) {
      XMLParser* parser = (XMLParser*) userData;
      XMLParser::Node* curNode = parser->_getCurrentElement();
      if(curNode != NULL) {
         /* Done with child, let's go back to its parent */
         parser->_setCurrentElement(curNode->parent);
      }
   }
   
   /* end of EXPAT parser functions */

   XMLParser::Attribute::Attribute(std::string name, std::string value) {
      this->name = name;
      this->value = value;
   }
   
   int XMLParser::Attribute::getIntValue() {
      int iValue = 0;
      std::istringstream out(value);
      out >> iValue;

      return iValue;
   }

   float XMLParser::Attribute::getFloatValue() {
      float fValue = 0;
      std::istringstream out(value);
      out >> fValue;

      return fValue;
   }
   
   XMLParser::Node::Node(Node* parent) {
      this->parent = parent;
   }

   XMLParser::Node::~Node() {
      /* Clear attributes list */
      while(attributes.size() > 0) {
         Attribute* att = attributes.back();
         attributes.pop_back();
         delete att;
      }
      /* Clear children list */
      while(children.size() > 0) {
         Node* child = children.back();
         children.pop_back();
         delete child;
      }
   }
         
   XMLParser::XMLParser() {
      curElement = NULL;
   }

   XMLParser::~XMLParser() {
      while(rootNodes.size() > 0) {
         Node* node = rootNodes.back();
         rootNodes.pop_back();
         delete node;
      }
   }

   bool XMLParser::load(const std::string& fileName) {
      std::ifstream fileStd;
      std::string strBuffer;
      
      /*! Define expat parameters */
      XML_Parser parser = XML_ParserCreate(NULL);
      XML_SetUserData(parser, this);
      XML_SetElementHandler(parser, startElement, endElement);

      /* Try to open and parse the file */
      fileStd.open(fileName.c_str(), std::ios::in | std::ios::binary);
      if(!fileStd) {
         /* Couldn't open! */
         XML_ParserFree(parser);
         std::cerr << "ERROR: Couldn't open file '" << 
            fileName << "'" << std::endl;
         return false;
      }
      for(int line = 1; !fileStd.eof(); line++) {
         getline(fileStd, strBuffer);
         if(XML_Parse(parser, strBuffer.c_str(), strBuffer.length(), 
                  fileStd.eof()) == XML_STATUS_ERROR) {
            /* Error! */
            std::cerr << "ERROR parsing file: " 
               << XML_ErrorString(XML_GetErrorCode(parser)) << " at line: " 
               << XML_GetCurrentLineNumber(parser) << std::endl;
            XML_ParserFree(parser);
            fileStd.close();
            return false;
         }
      }
      fileStd.close();

      XML_ParserFree(parser);
      return true;
   }

   XMLParser::Node* XMLParser::_getCurrentElement() {
      return curElement;
   }

   void XMLParser::_setCurrentElement(Node* element) {
      this->curElement = element;
   }

   void XMLParser::_addRootNode(Node* element) {
      rootNodes.push_back(element);
   }

}

