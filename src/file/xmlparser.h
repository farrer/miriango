/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_xml_parser_h
#define _miriango_xml_parser_h

#include <gtkmm.h>
#include <list>
#include <string>

namespace Miriango {

   /*! Wrapper parser for expat */
   class XMLParser {
      public:
         /*! A XML Attribute */
         class Attribute {
            public:
               /*! Constructor, with name and value definition */
               Attribute(std::string name, std::string value);
               /*! \return current value as integer */
               int getIntValue();
               /*! \return current value as float */
               float getFloatValue();
               /*! Attribute's name */
               std::string name;
               /*! Attribute's value as string */
               std::string value;
         };
         /*! XML Node, with its attributes and children */
         class Node {
            public:
               /*! Constructor, defining its parent */
               Node(Node* parent);
               /*! Destructor */
               ~Node();

               /*! Node's name */
               std::string name;
               /*! Node's attributes */
               std::list<Attribute*> attributes;
               /*! Nodes's children */
               std::list<Node*> children;
               /*! Node's parent, if any or NULL if a root node. */
               Node* parent;
         };
        
         /*! Constructor */
         XMLParser();
         /*! Destructor */
         virtual ~XMLParser();

         /*! Load and parse a file. All loaded structures in rootNodes list.
          * \return true if load was successfull. */
         bool load(const std::string& fileName);

         Node* _getCurrentElement();
         void _setCurrentElement(Node* element);
         void _addRootNode(Node* element);

      protected:
         /*! All root nodes of the document */
         std::list<Node*> rootNodes;

      private:
         /*! Current element. only used while parsing */
         Node* curElement;
   };

}

#endif

