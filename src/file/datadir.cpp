/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "datadir.h"
#include <fstream>
#include <iostream>

namespace Miriango {

   void DataDir::init() {
      if(tryDir("../data/")) {
         dataDir = "../data/";
         localeDir = dataDir + "locale";
      } else if(tryDir("./data/")) {
         dataDir = "./data/";
         localeDir = dataDir + "locale";
      } else if(tryDir(MIRIANGO_DATADIR + std::string("/"))) {
         dataDir = MIRIANGO_DATADIR + std::string("/");
         localeDir = MIRIANGO_LOCALEDIR;
      } else {
         std::cerr << "ERROR: Couldn't locate data directory!" << std::endl;
      }
   }
   
   void DataDir::finish() {
   }

   std::string DataDir::getDirectory() {
      return dataDir;
   }

   std::string DataDir::getLocaleDirectory() {
      return localeDir;
   }
   
   bool DataDir::tryDir(std::string dir) {
      std::string filename = dir + "icons/bead_color_copy.png";
      std::ifstream file;
      file.open(filename.c_str(), std::ios::in | std::ios::binary);
      if(file) {
         file.close();
         return true;
      }

      return false;
   }

   std::string DataDir::dataDir = "";
   std::string DataDir::localeDir = "";
}

