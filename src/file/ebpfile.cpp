/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ebpfile.h"
#include "../common/controller.h"
#include "../common/pattern.h"
#include "../ebp/ebpcolormap.h"
#include <iostream>
#include <fstream>

namespace Miriango {

   EBPFile::EBPFile() {
   }

   EBPFile::~EBPFile() {
   }

   int EBPFile::getPatternType(std::string name) {
      if(name == "Loom") {
         return Pattern::FILL_TYPE_LOOM;
      } else if(name == "Peyote") {
         return Pattern::FILL_TYPE_PEYOTE;
      } else if(name == "Two-Drop") {
         return Pattern::FILL_TYPE_TWO_DROP;
      } else if(name == "Brick") {
         return Pattern::FILL_TYPE_BRICK;
      }

      return -1;
   }

   void EBPFile::convertCoordinateByType(int& x, int& y, int type) {
      if(type == Pattern::FILL_TYPE_LOOM) {
         /* Loom: all * 2 (only regular beads) */
         x *= 2;
         y *= 2;
      } else if(type == Pattern::FILL_TYPE_PEYOTE) {
         /* Peyote: even cols: all * 2. odd cols: y * 2 + 1 */
         if(x % 2 == 0) {
            x *= 2;
            y *= 2;
         } else {
            x *= 2;
            y = y * 2 + 1;
         }
      } else if(type == Pattern::FILL_TYPE_TWO_DROP) {
         /*Two drop: DIV 4: 0 and 1: all * 2; 2 and 3: y * 2 + 1 */
         if((x % 4 == 0) || (x % 4 == 1)) {
            x *= 2;
            y *= 2;
         } else {
            x *= 2;
            y = y * 2 + 1;
         }
      } else if(type == Pattern::FILL_TYPE_BRICK) {
         /* Brick: even rows: all * 2. odd row: y * 2 + 1 */
         if(y % 2 == 0) {
            x *= 2;
            y *= 2;
         } else {
            x = x * 2 + 1;
            y *= 2;
         }
      }
   }

   bool EBPFile::load(const std::string& fileName, Gtk::Notebook* notebook) {
      bool res = XMLParser::load(fileName);
      if(!res) {
         return false;
      }
 
      bool loadedPattern = false;

      std::list<Node*>::iterator it;
      for(it = rootNodes.begin(); it != rootNodes.end(); ++it) {
         Node* root = *it;
         if(root->name == "pattern") {
            int width = 0;
            int height = 0;
            int type = -1;
            /* Let's get pattern attributes */
            for(std::list<Attribute*>::iterator att = root->attributes.begin();
                att != root->attributes.end(); ++att) {
                Attribute* attribute = *att;
                if(attribute->name == "rows") {
                   height = attribute->getIntValue();
                } else if(attribute->name == "cols" ) {
                   width = attribute->getIntValue();
                } else if(attribute->name == "stitch") {
                   type = getPatternType(attribute->value);
                }
            }

            /* Recalculate width and height based on pattern type */
            if(type < 0) {
               std::cerr << "ERROR: Unknown pattern type!" << std::endl;
               return false;
            } else if(type == Pattern::FILL_TYPE_LOOM) {
               width = width * 2 - 1;
               height = height * 2 - 1;
            } else if(type == Pattern::FILL_TYPE_PEYOTE) {
               width = width * 2 - 1;
               height = height * 2 + 1;
            } else if(type == Pattern::FILL_TYPE_TWO_DROP) {
               width = width * 2 - 1;
               height = height * 2 + 1;
            } else if(type == Pattern::FILL_TYPE_BRICK) {
               width = width * 2 + 1;
               height = height * 2 - 1;
            }

            /* Create pattern */
            if(width <= 0 || height <= 0) {
               std::cerr << "ERROR: invalid size (" << width << ", "
                     << height << ")" << std::endl;
               return false;
            }
            Pattern* pattern = new Pattern(width, height, notebook);
            pattern->setName(Glib::path_get_basename(fileName));
            loadedPattern = true;

            /* parse its children beads */
            std::list<Node*>::iterator ch;
            for(ch = root->children.begin(); ch != root->children.end(); ++ch) {
               Node* child = *ch;
               if(child->name == "bead") {
                  int x = -1;
                  int y = -1;
                  std::string colorId = "";
                  for(std::list<Attribute*>::iterator att = 
                        child->attributes.begin();
                        att != child->attributes.end(); ++att) {
                     Attribute* attribute = *att;
                     if(attribute->name == "col") {
                        x = attribute->getIntValue();
                     } else if(attribute->name == "row") {
                        y = attribute->getIntValue();
                     } else if(attribute->name == "id") {
                        colorId = attribute->value;
                     }
                  }
                  /* Convert EBP coordinate to Miriango coordinates */
                  convertCoordinateByType(x, y, type);

                  Bead* bead = pattern->getBead(x, y);
                  if(bead == NULL) {
                     std::cerr << "ERROR: invalid bead at (" << x << ", "
                           << y << ")" << std::endl;
                     return false;
                  }
                  bead->use(false);
                  bead->setColor(
                        EBPColorMap::getSingleton()->getColor(colorId), false);
               }
            }
            pattern->getUsedColors().updateDisplay();
            pattern->doneAction();
         }
      }

      if(!loadedPattern) {
         std::cerr << "WARNING: No pattern was loaded from file!" << std::endl;
         return false;
      }

      return true;
   }

   bool EBPFile::save(const std::string& fileName, Pattern* pattern) {
      std::ofstream file;
      EBPColorMap* colorMap = EBPColorMap::getSingleton();

      file.open(fileName.c_str(), std::ios::out | std::ios::binary);
      if(!file) {
         std::cerr << "ERROR: Couldn't open file '" << fileName
            << "' to save!" << std::endl;
         return false;
      }

      file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
         << std::endl;

      file << "<pattern stitch=\"Loom\" rows=\"" 
         << ((pattern->getHeight() / 2) + 1)
         << "\" cols=\"" << ((pattern->getWidth() / 2) + 1) 
         << "\">" << std::endl;
      for(int x = 0; x < pattern->getWidth(); x += 2) {
         for(int y = 0; y < pattern->getHeight(); y += 2) {
            Bead* bead = pattern->getBead(x, y);
            if(bead->isUsed()) {
               file << "<bead id=\"" 
                  << colorMap->getNearestColorName(bead->getColor())
                  << "\" col=\"" << (x / 2) 
                  << "\" row=\"" << (y / 2) 
                  << "\"/>" << std::endl;
            }
         }
      }


      file << "</pattern>" << std::endl;;
      file.close();
      return true;
   }

}

