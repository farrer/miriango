/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_mrg_file_h
#define _miriango_mrg_file_h

#include "xmlparser.h"
#include "../common/pattern.h"

namespace Miriango {

   /*! Parser for .mrg files. This is the internal format used by Miriango
    * to load and save patterns. */
   class MRGFile : public XMLParser {
      public:
         MRGFile();
         ~MRGFile();

         /*! Load .mrg file to a new pattern on Miriango.
          * \param fileName file to load.
          * \param notebook Gtk::Notebook to put the loaded in.
          * \param fullLoad true to load pattern and colors, false to
          *                 only load pattern */
         bool load(const std::string& fileName, Gtk::Notebook* notebook, 
               bool fullLoad=true); 
         /*! Save Pattern to file */
         bool save(const std::string& filename, Pattern* pattern);
   };

}

#endif

