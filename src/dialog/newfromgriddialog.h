/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_new_from_grid_dialog_h
#define _miriango_new_from_grid_dialog_h

#include <gtkmm.h>
#include "../common/pattern.h"

namespace Miriango {

   class NewFromGridDialog : public Gtk::Dialog {
      public:
         NewFromGridDialog();
         virtual ~NewFromGridDialog();

         const int getWidth() const;
         const int getHeight() const;
         const bool shouldFillWithBeads() const;
         const Pattern::FillType getFillType() const;

      protected:
         void onFillToggled();
      
      private:
         Glib::RefPtr<Gtk::Adjustment> widthAdjustment;
         Gtk::Label labelWidth;
         Gtk::SpinButton widthSpin;
         Glib::RefPtr<Gtk::Adjustment> heightAdjustment;
         Gtk::Label labelHeight;
         Gtk::SpinButton heightSpin;
         Gtk::CheckButton fillWithBeads;
         Gtk::ComboBoxText fillType;
   };

}

#endif

