/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_window_h
#define _miriango_window_h

#include <gtkmm.h>
#include "../common/usedcolorsdisplay.h"

namespace Miriango {

   class App;

   class MainWindow : public Gtk::ApplicationWindow {
      public:

         MainWindow(App* app);

         virtual ~MainWindow();

         Gtk::Notebook* getNotebook() { return &notebook; };

      protected:
         void onZoomValueChanged();
         void onSwitchPage(Gtk::Widget* page, guint page_number);
         void onPageRemoved(Gtk::Widget* page, guint page_number);
         
         void onButtonPatternEditClicked();
         void onButtonPatternCutClicked();
         void onButtonPatternCopyClicked();
         void onButtonPatternPasteClicked();

         void onButtonBeadColorSetClicked();
         void onButtonBeadColorLineClicked();
         void onButtonBeadColorFillClicked();
         void onButtonBeadColorPickClicked();
         void onButtonBeadColorCopyClicked();
         void onButtonBeadColorPasteClicked();

      private:
         /*! The tabs for each opened pattern */
         Gtk::Notebook notebook;

         Glib::RefPtr<Gio::ActionGroup> actionGroup;

         Gtk::Toolbar toolBar;
         Gtk::ToolButton buttonNew;
         Gtk::ToolButton buttonSave;
         Gtk::ToolButton buttonSaveAs;
         Gtk::ToolButton buttonExport;
         Gtk::ToolButton buttonLoad;
         Gtk::ToolButton buttonUndo;
         Gtk::ToolButton buttonRedo;

         Gtk::Box toolsArea;
         Gtk::Image activeToolImage;
         Gtk::ToolPalette toolSelector;
         Gtk::ToolItemGroup patternGroup;
         Gtk::Image imagePatternEdit;
         Gtk::ToolButton buttonPatternEdit;
         Gtk::Image imagePatternCut;
         Gtk::ToolButton buttonPatternCut;
         Gtk::Image imagePatternCopy;
         Gtk::ToolButton buttonPatternCopy;
         Gtk::Image imagePatternPaste;
         Gtk::ToolButton buttonPatternPaste;
         Gtk::ToolItemGroup beadGroup;
         Gtk::Image imageBeadColorSet;
         Gtk::ToolButton buttonBeadColorSet;
         Gtk::Image imageBeadColorPick;
         Gtk::ToolButton buttonBeadColorPick;
         Gtk::Image imageBeadColorFill;
         Gtk::ToolButton buttonBeadColorFill;
         Gtk::Image imageBeadColorLine;
         Gtk::ToolButton buttonBeadColorLine;
         Gtk::Image imageBeadColorCopy;
         Gtk::ToolButton buttonBeadColorCopy;
         Gtk::Image imageBeadColorPaste;
         Gtk::ToolButton buttonBeadColorPaste;
         Gtk::Box colorArea;
         Gtk::Grid grid;
         Gtk::ColorButton leftColorButton;
         Gtk::ColorButton rightColorButton;
         Glib::RefPtr<Gtk::Adjustment> zoomAdjustment;
         Gtk::Scale zoomScale;
         Gtk::ScrolledWindow usedColorsScroller;
         UsedColorsDisplay* usedColorsDisplay;

   };


}

#endif
