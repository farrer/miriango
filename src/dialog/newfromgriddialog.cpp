/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "newfromgriddialog.h"
#include <glibmm/i18n.h>

namespace Miriango {

   NewFromGridDialog::NewFromGridDialog()
                     :widthAdjustment(Gtk::Adjustment::create(38, 1, 2000)),
                      labelWidth(gettext("Width")),
                      widthSpin(widthAdjustment),
                      heightAdjustment(Gtk::Adjustment::create(68, 1, 2000)),
                      labelHeight(gettext("Height")),
                      heightSpin(heightAdjustment),
                      fillWithBeads(gettext("Fill with beads")) {

      Gtk::VBox* vbox = Gtk::make_managed<Gtk::VBox>();
      get_content_area()->add(*vbox);
      Gtk::HBox* hbox = Gtk::make_managed<Gtk::HBox>();
      vbox->add(*hbox);
      hbox->add(labelWidth);
      hbox->add(widthSpin);
      hbox->add(labelHeight);
      hbox->add(heightSpin);

      fillWithBeads.signal_toggled().connect(sigc::mem_fun(*this,
                    &NewFromGridDialog::onFillToggled));
      vbox->add(fillWithBeads);

      fillType.set_sensitive(false);
      fillType.append("LOOM", gettext("Loom"));
      fillType.append("PEYOTE", gettext("Peyote"));
      fillType.append("TWO_DROP", gettext("Two-drop"));
      fillType.append("BRICK", gettext("Brick"));
      fillType.set_active(0);
      vbox->add(fillType);

      add_button(gettext("Create"), Gtk::RESPONSE_OK);
      add_button(gettext("Cancel"), Gtk::RESPONSE_CANCEL);

      show_all_children();
   }

   NewFromGridDialog::~NewFromGridDialog() {
   }

   void NewFromGridDialog::onFillToggled() {
      if(fillWithBeads.get_active()) {
         fillType.set_sensitive(true);
      } else {
         fillType.set_sensitive(false);
      }
   }

   const int NewFromGridDialog::getWidth() const { 
      return widthSpin.get_value(); 
   }
   
   const int NewFromGridDialog::getHeight() const { 
      return heightSpin.get_value(); 
   }
   
   const bool NewFromGridDialog::shouldFillWithBeads() const { 
      return fillWithBeads.get_active(); 
   }
   
   const Pattern::FillType NewFromGridDialog::getFillType() const {
      Glib::ustring selId = fillType.get_active_id();
      if(selId == "LOOM") {
         return Pattern::FILL_TYPE_LOOM;
      }
      else if(selId == "PEYOTE") {
         return Pattern::FILL_TYPE_PEYOTE;
      }
      else if(selId == "TWO_DROP") {
         return Pattern::FILL_TYPE_TWO_DROP;
      }
      else if(selId == "BRICK") {
         return Pattern::FILL_TYPE_BRICK;
      }
      
      return Pattern::FILL_TYPE_LOOM;
   }

}

