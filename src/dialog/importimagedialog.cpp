/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "importimagedialog.h"
#include <glibmm/i18n.h>
#include <sstream>
#include <fstream>

namespace Miriango {

   ImportImageDialog::ImportImageDialog(int width, int height)
                     :labelWidth(gettext("Width")),
                      labelHeight(gettext("Height")) {

      /* Create adjustments */
      int initialX = (width < 40) ? width : 40;
      int initialY = (int)(((float)initialX / width) * height);
      widthAdjustment = Gtk::Adjustment::create(initialX, 1, width);
      heightAdjustment = Gtk::Adjustment::create(initialY, 1, height);

      /* Set adjustments */
      widthSpin.set_adjustment(widthAdjustment);
      heightSpin.set_adjustment(heightAdjustment);

      Gtk::VBox* vbox = Gtk::make_managed<Gtk::VBox>();
      vbox->add(labelImageDimension);
      labelImageDimension.set_text(gettext("Image size: ") + 
            getDimensionText(width, height));
      get_content_area()->add(*vbox);
      Gtk::HBox* hbox = Gtk::make_managed<Gtk::HBox>();
      vbox->add(*hbox);
      hbox->add(labelWidth);
      hbox->add(widthSpin);
      hbox->add(labelHeight);
      hbox->add(heightSpin);

      add_button(gettext("Create"), Gtk::RESPONSE_OK);
      add_button(gettext("Cancel"), Gtk::RESPONSE_CANCEL);

      show_all_children();
   }

   ImportImageDialog::~ImportImageDialog() {
   }

   const int ImportImageDialog::getWidth() const { 
      return widthSpin.get_value(); 
   }
   
   const int ImportImageDialog::getHeight() const { 
      return heightSpin.get_value(); 
   }
   
   Glib::ustring ImportImageDialog::getDimensionText(int width, int height) {
      std::stringstream out;
      out << width;
      out << " x ";
      out << height;
      return out.str();
   }
   
}

