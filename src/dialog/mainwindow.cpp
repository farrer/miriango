/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "miriangoconfig.h"
#include "mainwindow.h"
#include "../file/datadir.h"
#include "../common/controller.h"
#include "../common/pattern.h"
#include "../app.h"
#include <glibmm/i18n.h>

namespace Miriango {

   MainWindow::MainWindow(App* app)
              :toolsArea(Gtk::ORIENTATION_VERTICAL),
               colorArea(Gtk::ORIENTATION_VERTICAL),
               zoomAdjustment(Gtk::Adjustment::create(
                        Pattern::DEFAULT_ZOOM_LEVEL, Pattern::MIN_ZOOM_LEVEL, 
                        Pattern::MAX_ZOOM_LEVEL, 0.1, 0.5)),
               zoomScale(zoomAdjustment) {

      Controller::setColorButtons(&leftColorButton, &rightColorButton);

      /* Widgets */
      maximize();

      /* Tool Bar with quick access actions */
      buttonNew.set_icon_name("document-new");
      buttonNew.set_tooltip_text(gettext("New from grid"));
      buttonNew.signal_clicked().connect(sigc::mem_fun(*app,
                     &App::onFileNewFromGrid));
      toolBar.add(buttonNew);
      buttonLoad.set_icon_name("document-open");
      buttonLoad.set_tooltip_text(gettext("Load"));
      buttonLoad.signal_clicked().connect(sigc::mem_fun(*app,
               &App::onFileLoad));
      toolBar.add(buttonLoad);
      buttonSave.set_icon_name("document-save");
      buttonSave.set_tooltip_text(gettext("Save"));
      buttonSave.signal_clicked().connect(sigc::mem_fun(*app,
            &App::onFileSave));
      toolBar.add(buttonSave);
      buttonSaveAs.set_icon_name("document-save-as");
      buttonSaveAs.set_tooltip_text(gettext("Save as"));
      buttonSaveAs.signal_clicked().connect(sigc::mem_fun(*app,
            &App::onFileSaveAs));
      toolBar.add(buttonSaveAs);
      buttonExport.set_icon_name("drive-harddisk");
      buttonExport.set_tooltip_text(gettext("Export"));
      buttonExport.signal_clicked().connect(sigc::mem_fun(*app,
            &App::onFileExport));
      toolBar.add(buttonExport);
      buttonUndo.set_icon_name("edit-undo");
      buttonUndo.set_tooltip_text(gettext("Undo"));
      buttonUndo.signal_clicked().connect(sigc::mem_fun(*app,
            &App::onUndo));
      toolBar.add(buttonUndo);
      buttonRedo.set_icon_name("edit-redo");
      buttonRedo.set_tooltip_text(gettext("Redo"));
      buttonRedo.signal_clicked().connect(sigc::mem_fun(*app,
            &App::onRedo));
      toolBar.add(buttonRedo);


      /* Tools Area */
      toolsArea.set_size_request(120, 500);
      toolSelector.set_size_request(120, 180);
      toolsArea.add(toolSelector);
      
      /* Pattern tools */
      patternGroup.set_label(gettext("Pattern"));

      /* Edit */
      imagePatternEdit.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/pattern_edit.png"));
      buttonPatternEdit.set_icon_widget(imagePatternEdit);
      buttonPatternEdit.set_tooltip_markup(
            gettext("Add/remove beads from pattern\n\n"\
               "Mouse <i>click</i> on an empty area to add a bead.\n\n"\
               "Mouse <i>click</i> on a bead to remove it."));
      buttonPatternEdit.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonPatternEditClicked));
      patternGroup.insert(buttonPatternEdit);
      /* Cut */
      imagePatternCut.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/pattern_cut.png"));
      buttonPatternCut.set_icon_widget(imagePatternCut);
      buttonPatternCut.set_tooltip_markup(gettext("Cut beads from pattern\n\n"\
               "<b>Area select</b>\n"
               "Mouse <i>press and drag</i> to select beads in an area "\
               "and <i>release</i> to cut.\n\n"\
               "<b>Manual select</b>\n"
               "press <i>SHIFT</i> key to enter manual mode, "\
               "<i>mouse click</i> to select or deselect a bead, then "
               "release <i>SHIFT</i> key to cut.\n\n"\
               "Press <i>ESC</i> key to cancel current selection."));
      buttonPatternCut.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonPatternCutClicked));
      patternGroup.insert(buttonPatternCut);
      /* Copy */
      Glib::ustring copyText = gettext("<b>Area select</b>\n"
               "Mouse <i>press and drag</i> to select beads in an area "\
               "and <i>release</i> to copy.\n\n"\
               "<b>Manual select</b>\n"
               "press <i>SHIFT</i> key to enter manual mode, "\
               "<i>mouse click</i> to select or deselect a bead, then "
               "release <i>SHIFT</i> key to copy.\n\n"\
               "Press <i>ESC</i> key to cancel current selection.");
      imagePatternCopy.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/pattern_copy.png"));
      buttonPatternCopy.set_icon_widget(imagePatternCopy);
      buttonPatternCopy.set_tooltip_markup(
            gettext("Copy beads from pattern\n\n") + copyText);
      buttonPatternCopy.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonPatternCopyClicked));
      patternGroup.insert(buttonPatternCopy);
      /* Paste */
      imagePatternPaste.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/pattern_paste.png"));
      Glib::ustring pasteText = gettext("Mouse <i>click</i> to paste, "\
            "<i>left</i> and <i>right</i> arrow keys to rotate.");
      buttonPatternPaste.set_tooltip_markup(
            gettext("Paste beads to pattern\n\n") + pasteText);
      buttonPatternPaste.set_icon_widget(imagePatternPaste);
      buttonPatternPaste.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonPatternPasteClicked));
      patternGroup.insert(buttonPatternPaste);

      toolSelector.add(patternGroup);

      /* Bead Tools */
      beadGroup.set_label(gettext("Bead"));
      
      /* Color set */ 
      imageBeadColorSet.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/bead_color_set.png"));
      buttonBeadColorSet.set_icon_widget(imageBeadColorSet); 
      buttonBeadColorSet.set_tooltip_markup(gettext("Set bead color\n\n"\
               "Mouse <i>click</i> on a bead to change its color."));
      buttonBeadColorSet.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonBeadColorSetClicked));
      beadGroup.insert(buttonBeadColorSet);
      /* Color pick */
      imageBeadColorPick.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/bead_color_pick.png"));
      buttonBeadColorPick.set_icon_widget(imageBeadColorPick); 
      buttonBeadColorPick.set_tooltip_markup(gettext("Pick bead color\n\n"\
               "Mouse <i>click</i> on a bead to pick its color."));
      buttonBeadColorPick.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonBeadColorPickClicked));
      beadGroup.insert(buttonBeadColorPick);
      /* Fill */
      imageBeadColorFill.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/bead_color_fill.png"));
      buttonBeadColorFill.set_icon_widget(imageBeadColorFill);
      buttonBeadColorFill.set_tooltip_markup(gettext("Fill color to beads\n\n"\
               "Mouse <i>click</i> on a bead to change its color and "\
               "from all his neighbors that have the same color."));
      buttonBeadColorFill.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonBeadColorFillClicked));
      beadGroup.insert(buttonBeadColorFill);
      /* Line */
      imageBeadColorLine.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/bead_color_line.png"));
      buttonBeadColorLine.set_icon_widget(imageBeadColorLine);
      buttonBeadColorLine.set_tooltip_markup(gettext("Draw a line\n\n"\
               "Mouse <i>press and drag</i> on beads to draw a line "\
               "with active color after mouse <i>release</i>\n\n"\
               "Press <i>ESC</i> key to cancel."));
      buttonBeadColorLine.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonBeadColorLineClicked));
      beadGroup.insert(buttonBeadColorLine);
      /* Copy */
      imageBeadColorCopy.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/bead_color_copy.png"));
      buttonBeadColorCopy.set_icon_widget(imageBeadColorCopy);
      buttonBeadColorCopy.set_tooltip_markup(
            gettext("Copy current color from beads\n\n") + copyText); 
      buttonBeadColorCopy.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonBeadColorCopyClicked));
      beadGroup.insert(buttonBeadColorCopy);
      /* Paste */
      imageBeadColorPaste.set(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/bead_color_paste.png"));
      buttonBeadColorPaste.set_tooltip_markup(
            gettext("Paste colors to beads\n\n") + pasteText);
      buttonBeadColorPaste.set_icon_widget(imageBeadColorPaste); 
      buttonBeadColorPaste.signal_clicked().connect(sigc::mem_fun(*this,
               &MainWindow::onButtonBeadColorPasteClicked));
      beadGroup.insert(buttonBeadColorPaste);

      toolSelector.add(beadGroup);
      toolSelector.show();

      /* Current tool */
      Gtk::Frame* frame = Gtk::make_managed<Gtk::Frame>();
      frame->set_label(gettext("Current"));
      frame->set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
      activeToolImage.set_padding(0, 2);
      frame->add(activeToolImage);
      toolsArea.add(*frame);

      /* Color Area */
      colorArea.set_size_request(20, 500);
      colorArea.set_halign(Gtk::ALIGN_END);

      Gtk::VBox* vbox = Gtk::make_managed<Gtk::VBox>();
      vbox->set_halign(Gtk::ALIGN_END);
      
      Gtk::HBox* hbox = Gtk::make_managed<Gtk::HBox>();
      hbox->set_spacing(10);
      hbox->set_vexpand(false);
      leftColorButton.set_vexpand(false);
      rightColorButton.set_vexpand(false);
      hbox->pack_start(leftColorButton, 0, 0);
      hbox->pack_start(rightColorButton, 0, 0);
      hbox->set_valign(Gtk::ALIGN_BASELINE);

      /* Define initial colors */
      Gdk::RGBA color;
      color.set_rgba(1.0f, 1.0f, 1.0f, 1.0f);
      leftColorButton.set_rgba(color);
      leftColorButton.set_tooltip_text(gettext("Left mouse button color"));
      color.set_rgba(0.0f, 0.0f, 0.0f, 1.0f);
      rightColorButton.set_rgba(color);
      rightColorButton.set_tooltip_text(gettext("Right mouse button color"));
      
      vbox->pack_start(*hbox, Gtk::PACK_SHRINK);

      /* zoom */
      zoomScale.set_draw_value(false);
      zoomAdjustment->signal_value_changed().connect(sigc::mem_fun(*this,
          &MainWindow::onZoomValueChanged));
      zoomScale.set_tooltip_text(
            gettext("Slide to change current pattern zoom level"));
      vbox->pack_start(zoomScale, Gtk::PACK_SHRINK);

      /* used colors */
      Gtk::HBox* usedColorsBox = Gtk::make_managed<Gtk::HBox>();
      usedColorsDisplay = new UsedColorsDisplay(usedColorsBox);
      usedColorsDisplay->set_tooltip_markup(gettext("Current used colors\n\n"\
               "Mouse <i>click</i> on one to select it."));
      vbox->pack_start(*usedColorsBox, Gtk::PACK_EXPAND_WIDGET);
      Controller::setUsedColorsDisplay(usedColorsDisplay);

      colorArea.add(*vbox);

      /* Edit Area */
      notebook.signal_switch_page().connect(sigc::mem_fun(*this,
               &MainWindow::onSwitchPage));
      notebook.signal_page_removed().connect(sigc::mem_fun(*this,
               &MainWindow::onPageRemoved));
      notebook.set_scrollable(true);
      Glib::RefPtr<Gdk::Screen> screen = Gdk::Screen::get_default();
      notebook.set_size_request(screen->get_width() - 240, 
            screen->get_height() - 100);

      /* Define our main window grid */
      add(grid);
      grid.attach(toolBar, 0, 0, 3, 1); 
      grid.attach(toolsArea, 0, 1, 1, 1);
      grid.attach(notebook, 1, 1, 1, 1);
      grid.attach(colorArea, 2, 1, 1, 1);
      
      /* Show them all */
      grid.show_all();

      set_show_menubar(true);
   }

   MainWindow::~MainWindow() {
      delete usedColorsDisplay;
   }

   void MainWindow::onZoomValueChanged() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         pattern->setZoom(zoomScale.get_value());
         pattern->setAllDirty();
         pattern->notifyDirty();
      }
   }
   
   void MainWindow::onSwitchPage(Gtk::Widget* page, guint page_number) {
      Controller::setCurrentPattern(page);
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         pattern->getUsedColors().updateDisplay();
         zoomScale.set_value(pattern->getZoom());
      }
   }
   
   void MainWindow::onPageRemoved(Gtk::Widget* page, guint page_number) {
      Pattern* pattern = Controller::getPattern(page);
      if(pattern != NULL) {
         Controller::removePattern(pattern);
      }
   }

   void MainWindow::onButtonPatternEditClicked() {
      Controller::changeTool(Controller::TOOL_PATTERN_EDIT);
      activeToolImage.set(imagePatternEdit.get_pixbuf());
   }

   void MainWindow::onButtonPatternCutClicked() {
      Controller::changeTool(Controller::TOOL_CUT_PATTERN);
      activeToolImage.set(imagePatternCut.get_pixbuf());
   }

   void MainWindow::onButtonPatternCopyClicked() {
      Controller::changeTool(Controller::TOOL_COPY_PATTERN);
      activeToolImage.set(imagePatternCopy.get_pixbuf());
   }

   void MainWindow::onButtonPatternPasteClicked() {
      Controller::changeTool(Controller::TOOL_PASTE_PATTERN);
      activeToolImage.set(imagePatternPaste.get_pixbuf());
   }

   void MainWindow::onButtonBeadColorSetClicked() {
      Controller::changeTool(Controller::TOOL_BEAD_COLOR_CHANGE);
      activeToolImage.set(imageBeadColorSet.get_pixbuf());
   }

   void MainWindow::onButtonBeadColorLineClicked() {
      Controller::changeTool(Controller::TOOL_BEAD_COLOR_LINE);
      activeToolImage.set(imageBeadColorLine.get_pixbuf());
   }

   void MainWindow::onButtonBeadColorFillClicked() {
      Controller::changeTool(Controller::TOOL_BEAD_COLOR_FILL);
      activeToolImage.set(imageBeadColorFill.get_pixbuf());
   }

   void MainWindow::onButtonBeadColorPickClicked() {
      Controller::changeTool(Controller::TOOL_BEAD_COLOR_PICK);
      activeToolImage.set(imageBeadColorPick.get_pixbuf());
   }

   void MainWindow::onButtonBeadColorCopyClicked() {
      Controller::changeTool(Controller::TOOL_COPY_COLOR);
      activeToolImage.set(imageBeadColorCopy.get_pixbuf());
   }

   void MainWindow::onButtonBeadColorPasteClicked() {
      Controller::changeTool(Controller::TOOL_PASTE_COLOR);
      activeToolImage.set(imageBeadColorPaste.get_pixbuf());
   }

}

