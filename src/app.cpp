/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "miriangoconfig.h"
#include "app.h"
#include "common/controller.h"
#include "editor/imageimport.h"
#include "editor/effects.h"
#include "file/datadir.h"
#include "file/ebpfile.h"
#include "file/mrgfile.h"
#include <iostream>
#include <glibmm/i18n.h>

namespace Miriango {

   Glib::RefPtr<App> App::create() {
      return Glib::RefPtr<App>(new App());
   }

   App::App() 
       :Gtk::Application("org.dnteam.miriango") {
      Glib::set_application_name("Miriango");
      mainWindow = new MainWindow(this);
      Controller::init();
   }

   App::~App() {
      Controller::finish();
      delete mainWindow;
   }
   
   void App::on_startup() {
      Gtk::Application::on_startup();

      add_action("newfromgrid", sigc::mem_fun(*this,
               &App::onFileNewFromGrid));
      add_action("newfrompattern", sigc::mem_fun(*this, 
               &App::onFileNewFromPattern));
      add_action("newfromimage", sigc::mem_fun(*this, 
               &App::onFileNewFromImage));
      add_action("load", sigc::mem_fun(*this, &App::onFileLoad));
      add_action("save", sigc::mem_fun(*this, &App::onFileSave));
      add_action("saveas", sigc::mem_fun(*this, &App::onFileSaveAs));
      add_action("export", sigc::mem_fun(*this, &App::onFileExport));
      add_action("quit", sigc::mem_fun(*this, &App::onFileQuit));

      add_action("redo", sigc::mem_fun(*this, &App::onRedo));
      add_action("undo", sigc::mem_fun(*this, &App::onUndo));

      add_action("togglegrid", sigc::mem_fun(*this, &App::onViewToggleGrid));
      
      add_action("invertcolors", sigc::mem_fun(*this, 
               &App::onEffectsInvertColors));
      add_action("swapredblue", sigc::mem_fun(*this, 
               &App::onEffectsSwapRedBlue));
      add_action("swapredgreen", sigc::mem_fun(*this, 
               &App::onEffectsSwapRedGreen));
      add_action("swapgreenblue", sigc::mem_fun(*this, 
               &App::onEffectsSwapGreenBlue));
      
      add_action("about", sigc::mem_fun(*this, &App::onInfoAbout));

      Glib::RefPtr<Gio::Menu> menuBar = Gio::Menu::create();

      Glib::RefPtr<Gio::Menu> menuFile = createFileMenu();
      menuBar->append_submenu(gettext("File"), menuFile);
      Glib::RefPtr<Gio::Menu> menuEdit = createEditMenu();
      menuBar->append_submenu(gettext("Edit"), menuEdit);
      Glib::RefPtr<Gio::Menu> menuView = createViewMenu();
      menuBar->append_submenu(gettext("View"), menuView);
      Glib::RefPtr<Gio::Menu> menuEffects = createEffectsMenu();
      menuBar->append_submenu(gettext("Effects"), menuEffects);
      Glib::RefPtr<Gio::Menu> menuInfo = createInfoMenu();
      menuBar->append_submenu(gettext("Info"), menuInfo);

      set_menubar(menuBar);
   }

   void App::on_activate() {
      add_window(*mainWindow);
      mainWindow->show();
   }

   void App::onFileNewFromGrid() {
      newFromGrid.set_transient_for(*mainWindow);
      int result = newFromGrid.run();
      if(result == Gtk::RESPONSE_OK) {
         int width = newFromGrid.getWidth();
         int height = newFromGrid.getHeight();
         if(newFromGrid.shouldFillWithBeads()) {
            /* Adjust needed size according to the fill type */
            Pattern::FillType fillType = newFromGrid.getFillType();
            if(fillType == Pattern::FILL_TYPE_LOOM) {
               width = width * 2 - 1;
               height = height * 2 - 1;
            } else if((fillType == Pattern::FILL_TYPE_PEYOTE) ||
                      (fillType == Pattern::FILL_TYPE_TWO_DROP)) {
               width = width * 2 - 1;
               height = height * 2 + 1;
            } else if(fillType == Pattern::FILL_TYPE_BRICK) {
               width = width * 2 + 1;
               height = height * 2 - 1;
            }
         } else {
            /* Usually we alloc double values, as we allow half bead
             * positions. */
            width = width * 2 - 1;
            height = height * 2 - 1;
         }
         Pattern* pattern = new Pattern(width, height, 
               mainWindow->getNotebook());
         if(newFromGrid.shouldFillWithBeads()) {
            pattern->populate(newFromGrid.getFillType());
         }
         /* Tell pattern action to keep initial pattern state for undo's */
         pattern->doneAction();
         newFromGrid.close();
      } else {
         newFromGrid.close();
      }
   }

   void App::onFileNewFromPattern() {
      Gtk::FileChooserDialog dialog(gettext("Select file to open"),
            Gtk::FILE_CHOOSER_ACTION_OPEN);
      dialog.set_transient_for(*mainWindow);
      dialog.add_button(gettext("Cancel"), Gtk::RESPONSE_CANCEL);
      dialog.add_button(gettext("Open"), Gtk::RESPONSE_OK);

      Glib::RefPtr<Gtk::FileFilter> mirFilter = Gtk::FileFilter::create();
      mirFilter->set_name(gettext("Miriango Files"));
      mirFilter->add_pattern("*.mrg");
      dialog.add_filter(mirFilter);

      int result = dialog.run();
      if(result == Gtk::RESPONSE_OK) {
         std::string filename = dialog.get_filename();
         
         MRGFile mrgFile;
         mrgFile.load(filename, mainWindow->getNotebook(), false);
      }
   }

   void App::onFileNewFromImage() {
      Gtk::FileChooserDialog dialog(gettext("Select file to open"),
            Gtk::FILE_CHOOSER_ACTION_OPEN);
      dialog.set_transient_for(*mainWindow);
      dialog.add_button(gettext("Cancel"), Gtk::RESPONSE_CANCEL);
      dialog.add_button(gettext("Open"), Gtk::RESPONSE_OK);

      Glib::RefPtr<Gtk::FileFilter> mirFilter = Gtk::FileFilter::create();
      mirFilter->set_name(gettext("PNG Image Files"));
      mirFilter->add_pattern("*.png");
      dialog.add_filter(mirFilter);

      int result = dialog.run();
      if(result == Gtk::RESPONSE_OK) {
         ImageImport imgImport;
         imgImport.loadAndImport(dialog.get_filename(), 
               mainWindow->getNotebook(), mainWindow);
      }
   }

   void App::onFileLoad() {
      Gtk::FileChooserDialog dialog(gettext("Select file to open"),
            Gtk::FILE_CHOOSER_ACTION_OPEN);
      dialog.set_transient_for(*mainWindow);
      dialog.add_button(gettext("Cancel"), Gtk::RESPONSE_CANCEL);
      dialog.add_button(gettext("Open"), Gtk::RESPONSE_OK);

      Glib::RefPtr<Gtk::FileFilter> filter = Gtk::FileFilter::create();
      filter->set_name(gettext("All supported files"));
      filter->add_pattern("*.mrg");
      filter->add_pattern("*.ptnx");
      dialog.add_filter(filter);

      Glib::RefPtr<Gtk::FileFilter> mirFilter = Gtk::FileFilter::create();
      mirFilter->set_name(gettext("Miriango Files"));
      mirFilter->add_pattern("*.mrg");
      dialog.add_filter(mirFilter);

      Glib::RefPtr<Gtk::FileFilter> ebpFilter = Gtk::FileFilter::create();
      ebpFilter->set_name(gettext("Easy Bead Pattern Files"));
      ebpFilter->add_pattern("*.ptnx");
      dialog.add_filter(ebpFilter);

      int result = dialog.run();
      if(result == Gtk::RESPONSE_OK) {
         std::string filename = dialog.get_filename();
         if(Glib::str_has_suffix(filename, "mrg")) {
            /* Load miriango file */
            MRGFile mrgFile;
            mrgFile.load(filename, mainWindow->getNotebook());
         } else if(Glib::str_has_suffix(filename, "ptnx")) {
            /* Load easy bead pattern file */
            EBPFile ebpFile;
            ebpFile.load(filename, mainWindow->getNotebook());
         } else {
            /* Show error! */
            std::cerr << "ERROR: Unknown file format to load: " 
               << filename << std::endl;
         }
      }
   }

   void App::onFileSaveAs() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         Gtk::FileChooserDialog dialog(gettext("Select or set file to save"),
               Gtk::FILE_CHOOSER_ACTION_SAVE);
         dialog.set_transient_for(*mainWindow);
         dialog.add_button(gettext("Cancel"), Gtk::RESPONSE_CANCEL);
         dialog.add_button(gettext("Save"), Gtk::RESPONSE_OK);

         Glib::RefPtr<Gtk::FileFilter> mirFilter = Gtk::FileFilter::create();
         mirFilter->set_name(gettext("Miriango Files"));
         mirFilter->add_pattern("*.mrg");
         dialog.add_filter(mirFilter);

         int result = dialog.run();
         if(result == Gtk::RESPONSE_OK) {
            std::string filename = dialog.get_filename();
            /* Add extension, if not found. */
            if(filename.find(".mrg") == std::string::npos) {
               filename += ".mrg";
            }
            MRGFile mrgFile;
            mrgFile.save(filename, pattern);
         }
      }
   }

   void App::onFileSave() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern) {
         if(pattern->definedFile()) {
            MRGFile mrgFile;
            mrgFile.save(pattern->getFilename(), pattern);
         } else {
            onFileSaveAs();
         }
      }
   }

   void App::onFileExport() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         /* Get file to export */
         Gtk::FileChooserDialog dialog(gettext("Select or set file to export"),
               Gtk::FILE_CHOOSER_ACTION_SAVE);
         dialog.set_transient_for(*mainWindow);
         dialog.add_button(gettext("Cancel"), Gtk::RESPONSE_CANCEL);
         dialog.add_button(gettext("Export"), Gtk::RESPONSE_OK);

         Glib::RefPtr<Gtk::FileFilter> filter = Gtk::FileFilter::create();
         filter->set_name(gettext("PNG Image Files"));
         filter->add_pattern("*.png");
         dialog.add_filter(filter);

         Glib::RefPtr<Gtk::FileFilter> ebpFilter = Gtk::FileFilter::create();
         ebpFilter->set_name(gettext("Easy Bead Pattern Files"));
         ebpFilter->add_pattern("*.ptnx");
         dialog.add_filter(ebpFilter);

         int result = dialog.run();
         if(result == Gtk::RESPONSE_OK) {
            std::string filename = dialog.get_filename();

            if(dialog.get_filter() == ebpFilter) {
               /* Add extension, if not found. */
               if(filename.find(".ptnx") == std::string::npos) {
                  filename += ".ptnx";
               }
               EBPFile ebpFile;
               ebpFile.save(filename, pattern);
            } else {
               /* Add extension, if not found. */
               if(filename.find(".png") == std::string::npos) {
                  filename += ".png";
               }

               /* Draw the pattern to image */
               int width = (pattern->getWidth() + 1) * 
                  (int) ceilf(Bead::getHalfWidth(pattern->getZoom()));
               int height = (pattern->getHeight() + 1) *
                  (int) ceilf(Bead::getHalfHeight(pattern->getZoom()));
               Cairo::RefPtr<Cairo::ImageSurface> surface =
                  Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, 
                        width, height);

               Cairo::RefPtr<Cairo::Context> cr = Cairo::Context::create(
                     surface);
               pattern->draw(cr);

               /* And, finally, export it */
               surface->write_to_png(filename);
            }
         }
      }
   }

   void App::onFileQuit() {
      quit();
   }

   void App::onRedo() {
      Pattern* pattern = Controller::getCurrentPattern();
      if((pattern != NULL) && (pattern->getUndoRedo().hasRedo())) {
         pattern->getUndoRedo().redo();
      }
   }

   void App::onUndo() {
      Pattern* pattern = Controller::getCurrentPattern();
      if((pattern != NULL) && (pattern->getUndoRedo().hasUndo())) {
         pattern->getUndoRedo().undo();
      }
   }

   void App::onViewToggleGrid() {
      Controller::setShowGrid(!Controller::getShowGrid());
   }

   void App::onEffectsInvertColors() {
      Effects::getSingleton()->invertColors();
   }
   
   void App::onEffectsSwapRedBlue() {
      Effects::getSingleton()->swapRedBlueChannels();
   }

   void App::onEffectsSwapRedGreen() {
      Effects::getSingleton()->swapRedGreenChannels();
   }
   
   void App::onEffectsSwapGreenBlue() {
      Effects::getSingleton()->swapGreenBlueChannels();
   }

   void App::onInfoAbout() {
      Gtk::AboutDialog aboutDialog;
      aboutDialog.set_transient_for(*mainWindow);
     
      aboutDialog.set_logo(Gdk::Pixbuf::create_from_file(
               DataDir::getDirectory() + "icons/pattern_edit.png"));
      aboutDialog.set_program_name("Miriango");
      aboutDialog.set_version(MIRIANGO_VERSION);
      aboutDialog.set_copyright("DNTeam");
      aboutDialog.set_comments(gettext("A bead pattern editor."));
      aboutDialog.set_license("GNU General Public License 3.0 or latter");

      aboutDialog.set_website("https://miriango.dnteam.org");
      aboutDialog.set_website_label("https://miriango.dnteam.org");
      
      aboutDialog.run();
   }

   Glib::RefPtr<Gio::Menu> App::createFileMenu() {
      Glib::RefPtr<Gio::Menu> menuFile = Gio::Menu::create();
      
      Glib::RefPtr<Gio::Menu> sectionFileNew = Gio::Menu::create();

      Glib::RefPtr<Gio::Menu> menuNew = Gio::Menu::create();
      Glib::RefPtr<Gio::MenuItem> itemNFG = Gio::MenuItem::create(
            gettext("New from grid"), "app.newfromgrid");
      itemNFG->set_attribute_value("accel", 
            Glib::Variant<Glib::ustring>::create("<Primary>n"));
      menuNew->append_item(itemNFG);
      
      Glib::RefPtr<Gio::MenuItem> itemNFP = Gio::MenuItem::create(
            gettext("New from pattern"), "app.newfrompattern");
      menuNew->append_item(itemNFP);

      Glib::RefPtr<Gio::MenuItem> itemNFI = Gio::MenuItem::create(
            gettext("New from image"), "app.newfromimage");
      menuNew->append_item(itemNFI);

      sectionFileNew->append_submenu(gettext("New"), menuNew);
      menuFile->append_section(sectionFileNew);
      
      Glib::RefPtr<Gio::Menu> sectionFileActions = Gio::Menu::create();

      Glib::RefPtr<Gio::MenuItem> itemLoad = Gio::MenuItem::create(
            gettext("Load"), "app.load");
      itemLoad->set_attribute_value("accel", 
         Glib::Variant<Glib::ustring>::create("<Primary>l"));
      sectionFileActions->append_item(itemLoad);

      Glib::RefPtr<Gio::MenuItem> itemSave = Gio::MenuItem::create(
            gettext("Save"), 
         "app.save");
      itemSave->set_attribute_value("accel", 
         Glib::Variant<Glib::ustring>::create("<Primary>s"));
      sectionFileActions->append_item(itemSave);

      Glib::RefPtr<Gio::MenuItem> itemSaveAs = Gio::MenuItem::create(
            gettext("Save as"), "app.saveas");
      sectionFileActions->append_item(itemSaveAs);
      menuFile->append_section(sectionFileActions);

      Glib::RefPtr<Gio::Menu> sectionFileExport = Gio::Menu::create();
      Glib::RefPtr<Gio::MenuItem> itemExport = Gio::MenuItem::create(
            gettext("Export"), "app.export");
      sectionFileExport->append_item(itemExport);

      menuFile->append_section(sectionFileExport);

      Glib::RefPtr<Gio::Menu> sectionAppActions = Gio::Menu::create();
      Glib::RefPtr<Gio::MenuItem> itemQuit = Gio::MenuItem::create(
            gettext("Quit"), 
         "app.quit");
      sectionAppActions->append_item(itemQuit);

      menuFile->append_section(sectionAppActions);

      return menuFile;
   }

   Glib::RefPtr<Gio::Menu> App::createEditMenu() {
      Glib::RefPtr<Gio::Menu> menuEdit = Gio::Menu::create();
      
      Glib::RefPtr<Gio::MenuItem> undoItem = Gio::MenuItem::create(
            gettext("Undo"), "app.undo");
      undoItem->set_attribute_value("accel", 
            Glib::Variant<Glib::ustring>::create("<Primary>z"));
      menuEdit->append_item(undoItem);

      Glib::RefPtr<Gio::MenuItem> redoItem = Gio::MenuItem::create(
            gettext("Redo"), "app.redo");
      redoItem->set_attribute_value("accel", 
            Glib::Variant<Glib::ustring>::create("<Primary>y"));
      menuEdit->append_item(redoItem);

      return menuEdit;
   }

   Glib::RefPtr<Gio::Menu> App::createViewMenu() {
      Glib::RefPtr<Gio::Menu> menuView = Gio::Menu::create();
      
      Glib::RefPtr<Gio::MenuItem> toggleGridItem = Gio::MenuItem::create(
            gettext("Toggle grid"), "app.togglegrid");
      toggleGridItem->set_attribute_value("accel", 
            Glib::Variant<Glib::ustring>::create("<Primary>g"));
      menuView->append_item(toggleGridItem);

      return menuView;
   }

   Glib::RefPtr<Gio::Menu> App::createEffectsMenu() {
      Glib::RefPtr<Gio::Menu> menuEffects = Gio::Menu::create();
      
      Glib::RefPtr<Gio::MenuItem> invertColorsItem = Gio::MenuItem::create(
            gettext("Invert colors"), "app.invertcolors");
      menuEffects->append_item(invertColorsItem);

      Glib::RefPtr<Gio::MenuItem> swapRedBlueItem = Gio::MenuItem::create(
            gettext("Swap red <-> blue channels"), "app.swapredblue");
      menuEffects->append_item(swapRedBlueItem);

      Glib::RefPtr<Gio::MenuItem> swapRedGreenItem = Gio::MenuItem::create(
            gettext("Swap red <-> green channels"), "app.swapredgreen");
      menuEffects->append_item(swapRedGreenItem);

      Glib::RefPtr<Gio::MenuItem> swapGreenBlueItem = Gio::MenuItem::create(
            gettext("Swap green <-> blue channels"), "app.swapgreenblue");
      menuEffects->append_item(swapGreenBlueItem);

      return menuEffects;
   }

   Glib::RefPtr<Gio::Menu> App::createInfoMenu() {
      Glib::RefPtr<Gio::Menu> menuInfo = Gio::Menu::create();
      
      Glib::RefPtr<Gio::MenuItem> aboutItem = Gio::MenuItem::create(
            gettext("About"), "app.about");
      menuInfo->append_item(aboutItem);

      return menuInfo;
   }

}

int main(int argc, char *argv[]) {
   Miriango::DataDir::init();

   setlocale(LC_CTYPE, "");
   setlocale(LC_MESSAGES, "");
   bindtextdomain("miriango", Miriango::DataDir::getLocaleDirectory().c_str());
   textdomain("miriango");

   Glib::RefPtr<Miriango::App> app = Miriango::App::create();
   const int status = app->run(argc, argv);

   Miriango::DataDir::finish();

   return status;
}

