/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_app_h
#define _miriango_app_h

#include <gtkmm.h>
#include "dialog/mainwindow.h"
#include "dialog/newfromgriddialog.h"

namespace Miriango {


   class App: public Gtk::Application
   {
      public:
         static Glib::RefPtr<App> create();
         virtual ~App();

         void onFileNewFromGrid();
         void onFileNewFromPattern();
         void onFileNewFromImage();
         void onFileLoad();
         void onFileSaveAs();
         void onFileSave();
         void onFileExport();

         void onFileQuit();

         void onUndo();
         void onRedo();

         void onViewToggleGrid();

         void onEffectsInvertColors();
         void onEffectsSwapRedBlue();
         void onEffectsSwapRedGreen();
         void onEffectsSwapGreenBlue();

         void onInfoAbout();

      protected:
         App();
         void on_startup() override;
         void on_activate() override;

      private:
         Glib::RefPtr<Gio::Menu> createFileMenu();
         Glib::RefPtr<Gio::Menu> createEditMenu();
         Glib::RefPtr<Gio::Menu> createViewMenu();
         Glib::RefPtr<Gio::Menu> createEffectsMenu();
         Glib::RefPtr<Gio::Menu> createInfoMenu();

         MainWindow* mainWindow;
         NewFromGridDialog newFromGrid;
   };


}

#endif

