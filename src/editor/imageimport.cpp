/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "imageimport.h"
#include "../common/color.h"
#include "../common/pattern.h"
#include "../dialog/importimagedialog.h"
#include <iostream>
#include <math.h>

namespace Miriango {
   
   ImageImport::ImageImport() {
   }
   
   ImageImport::~ImageImport() {
   }

   void ImageImport::loadAndImport(const std::string& filename, 
         Gtk::Notebook* notebook, Gtk::Window* mainWindow) {
      /* load image to pixbuf */
      Glib::RefPtr<Gdk::Pixbuf> pixbuf =
         Gdk::Pixbuf::create_from_file(filename);

      if(pixbuf->get_colorspace() != Gdk::COLORSPACE_RGB) {
         std::cerr << "ERROR: Image color space not supported : " 
                   << pixbuf->get_colorspace() << std::endl;
         return;
      }

      /* retrieve parameters */
      int imgWidth = pixbuf->get_width();
      int imgHeight = pixbuf->get_height();

      if((imgWidth <= 0) || (imgHeight <= 0)) {
         std::cerr << "ERROR: Unknown image dimensions!" << std::endl;
         return;
      }

      /* Get desired pattern size */
      ImportImageDialog dialog(imgWidth, imgHeight);
      dialog.set_transient_for(*mainWindow);
      int result = dialog.run();
      if(result == Gtk::RESPONSE_OK) {

         /* Calculate total pixels per bead */
         int width = dialog.getWidth();
         int height = dialog.getHeight();
         int incX = (int)floor(((float)imgWidth) / width);
         int incY = (int)floor(((float)imgHeight) / height);

         /* Finally, import image */
         int channels = pixbuf->get_n_channels();
         int rowstride = pixbuf->get_rowstride();
         guchar* pixels = pixbuf->get_pixels();
         
         Pattern* pattern = new Pattern(width * 2 - 1, height * 2 - 1, 
               notebook);

         for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
               Bead* bead = pattern->getBead(x * 2, y * 2);
               Color curColor;
               bool definedColor = false;
               /* get interpolated pixels related to the bead */
               for(int iX = 0; iX < incX; iX++) {
                  for(int iY = 0; iY < incY; iY++) {
                     int pX = x * incX + iX;
                     int pY = y * incY + iY;
                     if((pX < imgWidth) && (pY < imgHeight)) {
                        guchar* p = pixels + pY * rowstride + pX * channels;
                        Color color(((float)p[0]) / 255.0f, 
                              ((float)p[1]) / 255.0f, 
                              ((float)p[2]) / 255.0f);
                        if(!definedColor) {
                           curColor = color;
                           definedColor = true;
                        } else {
                           curColor.interpolate(color);
                        }
                     }
                  }
               }
               if(definedColor) {
                  bead->use(false);
                  bead->setColor(curColor, false);
               }
            }
         }

         pattern->doneAction();
         pattern->getUsedColors().updateDisplay();
      }
   }
}

