/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_effects_h_
#define _miriango_effects_h_

#include "miriangoconfig.h"
#include "../common/beadgrid.h"

namespace Miriango {

   class Effects {
      public:
         static void init();
         static void finish();
         static Effects* getSingleton();
         
         ~Effects();

         /*! Invert all colors from each pattern bead.
          * Inversion is newValue = 1 - oldValue; */
         void invertColors();
         /*! Swap read and blue channels for each pattern bead */
         void swapRedBlueChannels();
         /*! Swap read and green channels for each pattern bead */
         void swapRedGreenChannels();
         /*! Swap green and blue channels for each pattern bead */
         void swapGreenBlueChannels();

      private:
         Effects();
         static Effects* singleton;
   };


}

#endif

