/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "effects.h"
#include "../common/controller.h"

namespace Miriango {

   void Effects::init() {
      if(singleton == NULL) {
         singleton = new Effects();
      }
   }

   void Effects::finish() {
      if(singleton != NULL) {
         delete singleton;
         singleton = NULL;
      }
   }
   
   Effects* Effects::getSingleton() {
      return singleton;
   }
         
   Effects::Effects() {
   }

   Effects::~Effects() {
   }

   void Effects::invertColors() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         for(int x = 0; x < pattern->getWidth(); x++) {
            for(int y = 0; y < pattern->getHeight(); y++) {
               Bead* bead = pattern->getBead(x, y);
               if(bead->isUsed()) {
                  Color color = bead->getColor();
                  color.set(1.0f - color.getRed(), 1.0f - color.getGreen(),
                        1.0f - color.getBlue());
                  bead->setColor(color, false);
                  pattern->setDirtyArea(bead, pattern->getZoom());
               }
            }
         }
         pattern->getUsedColors().updateDisplay();
         pattern->doneAction();
      }
   }

   void Effects::swapRedBlueChannels() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         for(int x = 0; x < pattern->getWidth(); x++) {
            for(int y = 0; y < pattern->getHeight(); y++) {
               Bead* bead = pattern->getBead(x, y);
               if(bead->isUsed()) {
                  Color color = bead->getColor();
                  color.set(color.getBlue(), color.getGreen(), color.getRed());
                  bead->setColor(color, false);
                  pattern->setDirtyArea(bead, pattern->getZoom());
               }
            }
         }
         pattern->getUsedColors().updateDisplay();
         pattern->doneAction();
      }
   }

   void Effects::swapRedGreenChannels() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         for(int x = 0; x < pattern->getWidth(); x++) {
            for(int y = 0; y < pattern->getHeight(); y++) {
               Bead* bead = pattern->getBead(x, y);
               if(bead->isUsed()) {
                  Color color = bead->getColor();
                  color.set(color.getGreen(), color.getRed(), color.getBlue());
                  bead->setColor(color, false);
                  pattern->setDirtyArea(bead, pattern->getZoom());
               }
            }
         }
         pattern->getUsedColors().updateDisplay();
         pattern->doneAction();
      }
   }
   
   void Effects::swapGreenBlueChannels() {
      Pattern* pattern = Controller::getCurrentPattern();
      if(pattern != NULL) {
         for(int x = 0; x < pattern->getWidth(); x++) {
            for(int y = 0; y < pattern->getHeight(); y++) {
               Bead* bead = pattern->getBead(x, y);
               if(bead->isUsed()) {
                  Color color = bead->getColor();
                  color.set(color.getRed(), color.getBlue(), color.getGreen());
                  bead->setColor(color, false);
                  pattern->setDirtyArea(bead, pattern->getZoom());
               }
            }
         }
         pattern->getUsedColors().updateDisplay();
         pattern->doneAction();
      }
   }

   Effects* Effects::singleton = NULL;

}
