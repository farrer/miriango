/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "undoredocontroller.h"
#include "../common/bead.h"
#include "../common/pattern.h"

namespace Miriango {

   UndoRedoController::State::State(int width, int height) 
                             :beads(width, height, NULL) {
      this->used = false;
      this->width = width;
      this->height = height;
      this->previous = NULL;
      this->next = NULL;

      for(int x = 0; x < width; x++) {
         for(int y = 0; y < height; y++) {
            this->beads(x, y)->setCoordinate(x, y);
         }
      }
   }

   UndoRedoController::State::~State() {
   }

   UndoRedoController::UndoRedoController(Pattern* owner) { 
      this->owner = owner;
      this->states = NULL;
      /* Create states list */
      for(int i = 0; i < UNDO_REDO_MAX_STATES; i++) {
         addNewState(new State(owner->getWidth(), owner->getHeight()));
      }
      /* Set curState to the last state on the list */
      this->curState = this->states->previous;
      this->lastCurState = this->states->previous;
   }

   UndoRedoController::~UndoRedoController() {
   }
   
   void UndoRedoController::addNewState(State* state) {
      if(this->states == NULL) {
         /* Insert as first and only element */
         this->states = state;
         state->next = state;
         state->previous = state;
      } else {
         /* Insert as last list element */
         state->next = this->states;
         state->previous = this->states->previous;
         this->states->previous->next = state;
         this->states->previous = state;
      }
   }

   void UndoRedoController::saveCurrentState() {
      unuseUpToLastCurState();
      
      curState = curState->next;
      lastCurState = curState;

      for(int x = 0; x < owner->getWidth(); x++) {
         for(int y = 0; y < owner->getHeight(); y++) {
            (*curState->beads(x, y)) = (*owner->getBead(x, y));
         }
      }
      curState->used = true;
   }

   void UndoRedoController::restoreState(State* state) {
      for(int x = 0; x < owner->getWidth(); x++) {
         for(int y = 0; y < owner->getHeight(); y++) {
            (*owner->getBead(x, y)) = (*state->beads(x, y));
         }
      }
   }


   void UndoRedoController::unuseUpToLastCurState() {
      /* Calculate spectrum to mark as unused */
      State* first = curState->next;
      State* until = lastCurState->next;

      /* unuse it */
      State* cur = first;
      while(cur != until) {
         cur->used = false;
         cur = cur->next;
      }
   }

   bool UndoRedoController::hasRedo() {
      State* state = curState->next;

      if(state != lastCurState->next) {
         return state->used;
      }

      return false;
   }

   bool UndoRedoController::hasUndo() {
      State* state = curState->previous;
      if(state != lastCurState) {
         return state->used;
      }
      return false;
   }

   void UndoRedoController::undo() {
      if(hasUndo()) {
         curState = curState->previous;
         restoreState(curState);

         owner->clearAll();
         owner->setAllDirty();
         owner->notifyDirty();
         owner->getUsedColors().updateDisplay();
      }
   }

   void UndoRedoController::redo() {
      if(hasRedo()) {
         curState = curState->next;
         restoreState(curState);

         owner->clearAll();
         owner->setAllDirty();
         owner->notifyDirty();
         owner->getUsedColors().updateDisplay();
      }
   }

}

