/* 
  Miriango: a bead pattern editor
  Copyright (C) Farrer <farrer@dnteam.org>
 
  This file is part of Miriango.
 
  Miriango is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Miriango is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Miriango.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _miriango_undo_redo_controller_h_
#define _miriango_undo_redo_controller_h_

#include "miriangoconfig.h"
#include "../common/beadgrid.h"

namespace Miriango {

#define UNDO_REDO_MAX_STATES   20

   /*! A Controller for Undo-Redo states of a Pattern */
   class UndoRedoController {
      
      public:
         /*! Constructor 
          * \param owner Pattern owner of this */
         UndoRedoController(Pattern* owner);
         /*! Destructor */
         ~UndoRedoController();

         /*! Save current pattern to the current state */
         void saveCurrentState();

         /*! Check if have some states to redo */
         bool hasRedo();
         /*! Check if have some states to undo */
         bool hasUndo();

         /*! Undo to previous state */
         void undo();
         /*! Redo to next state */
         void redo();

      private:

         /*! A single Pattern state. */
         class State {
            public:
               State(int width, int height);
               ~State();

               bool used; /**< If the state is used or not */
               BeadGrid beads; /**< Pattern Bead grid at the state */
               int width;  /**< Pattern grid width */
               int height; /**< Pattern grid height */
               State* next;
               State* previous;
         };

         /*! Add a new state to the circular double linked list of states */
         void addNewState(State* state);

         /*! Restore the state to the pattern */
         void restoreState(State* state);

         /*! Mark subsequent states as unused. This is used to make undo and
          * redo actions sane. */
         void unuseUpToLastCurState();

         Pattern* owner;  /**< Pattern owner of the controller */
         State* states; /**< States to keep, as a double linked circular list */
         State* curState;    /**< Pointer to current state */
         State* lastCurState; /**< Last current state, used for redos */

   };

}

#endif

