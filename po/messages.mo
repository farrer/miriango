��    *      l  ;   �      �  3  �  }   �     [     o     t     {     �     �  <   �  I  �  �   D     �     �     �     �  z   �     w     �     �  O   �     �     �     	     	     %	     *	     :	     S	     k	  @   s	     �	     �	     �	     �	     �	     �	     �	     
  A   0
  *   r
     �
    �
  �  �  �   �     +     E     Q     Z  -   y     �  Q   �  �  �  �   �     �     �     �     �  �   �  0   �     �     �  �   �     m     r     �     �     �     �     �  )   �     '  u   /     �     �  /   �     �     �     �  !        6  d   V  4   �     �              $   
             %       )                       *              (              	                                          #   !                          "            &                 '          <b>Area select</b>
Mouse <i>press and drag</i> to select beads in an area and <i>release</i> to copy.

<b>Manual select</b>
press <i>SHIFT</i> key to enter manual mode, <i>mouse click</i> to select or deselect a bead, then release <i>SHIFT</i> key to copy.

Press <i>ESC</i> key to cancel current selection. Add/remove beads from pattern

Mouse <i>click</i> on an empty area to add a bead.

Mouse <i>click</i> on a bead to remove it. All supported files Bead Cancel Copy beads from pattern

 Copy current color from beads

 Current Current used colors

Mouse <i>click</i> on one to select it. Cut beads from pattern

<b>Area select</b>
Mouse <i>press and drag</i> to select beads in an area and <i>release</i> to cut.

<b>Manual select</b>
press <i>SHIFT</i> key to enter manual mode, <i>mouse click</i> to select or deselect a bead, then release <i>SHIFT</i> key to cut.

Press <i>ESC</i> key to cancel current selection. Draw a line

Mouse <i>press and drag</i> on beads to draw a line with active color after mouse <i>release</i>

Press <i>ESC</i> key to cancel. Easy Bead Pattern Files Edit Export File Fill color to beads

Mouse <i>click</i> on a bead to change its color and from all his neighbors that have the same color. Left mouse button color Load Miriango Files Mouse <i>click</i> to paste, <i>left</i> and <i>right</i> arrow keys to rotate. New New from grid New from image New from pattern Open PNG Image Files Paste beads to pattern

 Paste colors to beads

 Pattern Pick bead color

Mouse <i>click</i> on a bead to pick its color. Quit Redo Right mouse button color Save Save as Select file to open Select or set file to export Select or set file to save Set bead color

Mouse <i>click</i> on a bead to change its color. Slide to change current pattern zoom level Undo Project-Id-Version: Miriango 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-05-13 20:00-0300
Last-Translator: Farrer <farrer@dnteam.org>
Language-Team: pt_BR <LL@li.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 <b>Seleção de área</b>
<i>Pressione e arraste</i> com o botão do mouse para selecionar miçangas em uma área e <i>solte</i> o mesmo botão para copiar.

<b>Seleção manual</b>
Aperte a tecla <i>SHIFT</i> para entrar no modo de seleção manual, <i>clique</i> com o mouse para selecionar or deselecionar miçangas, e solte a tecla <i>SHIFT</i> quando finalizada a seleção para copiá-la.

Pressione a tecla <i>ESC</i> para cancelar a seleção atual. Adicionar/remover miçangas do padrão

<i>Clique</i> do mouse em um local vazio para adicionar uma miçanga.

<i>Clique</i> do mouse em uma miçanga para removê-la. Todos formatos suportados Coloração Cancelar Copiar miçangas do padrão.

 Copiar as cores atuais de algumas miçangas

 Atual Cores atualmente utilizadas

<i>Clique</i> com o mouse em uma para selecioná-la. Recortar miçangas do padrão

<b>Seleção de área</b>
<i>Pressione e arraste</i> com o botão do mouse para selecionar miçangas em uma área e <i>solte</i> o mesmo botão para recortar.

<b>Seleção manual</b>
Aperte a tecla <i>SHIFT</i> para entrar no modo de seleção manual, <i>clique</i> com o mouse para selecionar or deselecionar miçangas, e solte a tecla <i>SHIFT</i> quando finalizada a seleção para recortá-la.

Pressione a tecla <i>ESC</i> para cancelar a seleção atual. Desenhar uma linha

<i>Pressione e arreste</i> o mouse entre duas miçangas para desenhar uma linha reta entre elas.

Pressione a tecla <i>ESC</i> para cancelar um desenho em curso. Arquivos do Easy Bead Patterns Editar Exportar Arquivo Preencher a cor de miçangas iguais

<i>Clique</i> com o mouse em uma miçanga para mudar a sua coloração e a de todas suas vizinhas que possuíam a mesma coloração que ela. Coloração ativa com o botão esquerdo do mouse Carregar Arquivos Miriango <i>Clique</i> com o botão do mouse para colar. Pressione a tecla de <i>seta à esquerda</i> e <i>seta à direita</i> para rotacionar antes de colar. Novo Novo a partir de um grid Novo a partir de uma imagem Novo a partir de um padrão Abrir Arquivos de Imagem PNG Colar miçangas no padrão

 Colar a coloração copiada a miçangas

 Padrão Pegar a color atual de uma miçanga

<i>Clique</i> com o mouse em uma miçanga para selecionar sua coloração atual. Sair Refazer Coloração ativa com o botão direito do mouse Salvar Salvar como Selecione o arquivo para abrir Selecione o arquivo para exportar Selecione o arquivo para salvar Definir a cor de uma miçanga

<i>Clique</i> com o mouse em uma miçanga para mudar sua coloração. Deslize para mudar o nível de zoom do padrão atual Desfazer 