set(COMMON_HEADERS
   src/common/bead.h
   src/common/beadgrid.h
   src/common/color.h
   src/common/controller.h
   src/common/drawingarea.h
   src/common/mousedrawingarea.h
   src/common/mouseevent.h
   src/common/pattern.h
   src/common/usedcolors.h
   src/common/usedcolorsdisplay.h
)
set(COMMON_SOURCES
   src/common/bead.cpp
   src/common/beadgrid.cpp
   src/common/color.cpp
   src/common/controller.cpp
   src/common/drawingarea.cpp
   src/common/mousedrawingarea.cpp
   src/common/mouseevent.cpp
   src/common/pattern.cpp
   src/common/usedcolors.cpp
   src/common/usedcolorsdisplay.cpp
)

set(DIALOG_HEADERS
   src/dialog/importimagedialog.h
   src/dialog/mainwindow.h
   src/dialog/newfromgriddialog.h
)
set(DIALOG_HEADERS
   src/dialog/importimagedialog.cpp
   src/dialog/mainwindow.cpp
   src/dialog/newfromgriddialog.cpp
)

set(EBP_HEADERS
   src/ebp/ebpcolormap.h
)
set(EBP_SOURCES
   src/ebp/ebpcolormap.cpp
)

set(EDITOR_HEADERS
   src/editor/effects.h
   src/editor/imageimport.h
   src/editor/undoredocontroller.h
)
set(EDITOR_SOURCES
   src/editor/effects.cpp
   src/editor/imageimport.cpp
   src/editor/undoredocontroller.cpp
)

set(FILE_HEADERS
   src/file/datadir.h
   src/file/xmlparser.h
   src/file/ebpfile.h
   src/file/mrgfile.h
)
set(FILE_SOURCES
   src/file/datadir.cpp
   src/file/xmlparser.cpp
   src/file/ebpfile.cpp
   src/file/mrgfile.cpp
)

set(TOOL_HEADERS
   src/tool/tool.h
   src/tool/beadaddremovetool.h
   src/tool/beadcolortool.h
   src/tool/beadcolorchangetool.h
   src/tool/beadcolorfilltool.h
   src/tool/beadcolorlinetool.h
   src/tool/beadcolorpicktool.h
   src/tool/beadcopytool.h
   src/tool/beadcopycolortool.h
   src/tool/beadcopypatterntool.h
   src/tool/beadcutpatterntool.h
   src/tool/beadpastetool.h
   src/tool/beadpastecolortool.h
   src/tool/beadpastepatterntool.h
)
set(TOOL_SOURCES
   src/tool/tool.cpp
   src/tool/beadaddremovetool.cpp
   src/tool/beadcolortool.cpp
   src/tool/beadcolorchangetool.cpp
   src/tool/beadcolorfilltool.cpp
   src/tool/beadcolorlinetool.cpp
   src/tool/beadcolorpicktool.cpp
   src/tool/beadcopytool.cpp
   src/tool/beadcopycolortool.cpp
   src/tool/beadcopypatterntool.cpp
   src/tool/beadcutpatterntool.cpp
   src/tool/beadpastetool.cpp
   src/tool/beadpastecolortool.cpp
   src/tool/beadpastepatterntool.cpp
)


set(MIRIANGO_LIB_HEADERS
   ${COMMON_HEADERS}
   ${DIALOG_HEADERS}
   ${EDITOR_HEADERS}
   ${FILE_HEADERS}
   ${EBP_HEADERS}
   ${TOOL_HEADERS}
)

set(MIRIANGO_LIB_SOURCES
   ${COMMON_SOURCES}
   ${DIALOG_SOURCES}
   ${EDITOR_SOURCES}
   ${FILE_SOURCES}
   ${EBP_SOURCES}
   ${TOOL_SOURCES}
)

set(MIRIANGO_SOURCES
   src/app.cpp
   ${WIN_SOURCES}
)
set(MIRIANGO_HEADERS
   src/app.h
)

